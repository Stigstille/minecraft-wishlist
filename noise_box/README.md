Noise Box
=========

Adds a noise box item that can be toggled ![noise_box_on] open and
![noise_box_off] close; if carried while open, it tags the player with
`wishlist.hidden`.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for player UI
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Holding and using the noise box toggles it between its open and close state.

If a player has it open in their inventory, they are tagged `wishlist.hidden`.

The hidden tag interacts with a number of other datapacks.

Overhead
--------

Checks every player every second to see if they have an open noise box in their
inventory

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Apotropaic Device: Open a noise box

[noise_box_off]: resources/assets/wishlist/textures/item/noise_box_off.png ""
[noise_box_on]: resources/assets/wishlist/textures/item/noise_box_on.png ""
