Chatterbox
==========

Provide more ways to interact with mobs of all types.

Mobs are given a special interaction area (roughly their head in the case of
bipeds, and the middle of their back for the rest), and clicking there allows
to do other things that just hit/feed/trade with the mobs.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

By default, all non-rideable mobs get a volatile chatterbox with no dialogue
when a player is close enough to interact with them, and it is removed when no
player is close enough.  The chatterbox can be made as permanent as the mob by
removing the `wishlist.chatterbox.volatile` tag from the root entity.

If the entity despawns on any way while wearing the chatterbox, the chatterbox
will remain in place until pruned; the prune happens every second around active
players and randomly in loaded chunks.

When right-clicked, the player will read a line that says:

```
<NPC name> chatter
```

The chatter can be changed by the employer of the NPC by using an unsigned
book on them. The entire text of the first page will be set as the NPC chatter.
Note that the page must contain valid JSON text. For simple sentences you only
have to enclose the text in quotes.

Changing the chatter line for any mob will make the chatterbox persistent, even
if players around it have the `classic_combat` flag active (see below).

When left-clicked with an empty hand the NPC will emit a generic sound that
depends on their type and show three floating hearts above their head.

When left-clicked with something on the main hand the NPC will be hurt as if it
had been hit. Calculation of the right amount of damage for this is an ongoing
project.

If a player does not want to deal with chatterboxes during combat, the creation
of volatile chatterboxes around them can be disabled by running:

```
/trigger wishlist.chatterbox.classic_combat set 1
```

And enabled again with:

```
/trigger wishlist.chatterbox.classic_combat set 0
```

Overhead
--------

Provides two advancements for players attacking or interacting with an
interaction entity tagged `wishlist.chatterbox`

* checks in a small area in front of the player's eyes to find the interaction
  entity that was interacted with.
    * checks every player in an 8-block radius around the interaction entity to
      whisper a message to them.

Every second a prune of lost chatterbox entities is performed in a small radius
around each player and in a random loaded chunk.

Known Issues
------------

The chatterbox on villagers does not cover the top side of their head because
that's not how riding points on entities work. Players might hit a villager
from above when trying to interact with them.

Setting a chatterbox on biped mobs (illagers, piglins, zombies…) prevents
players from raising their shields when fighting them; instead they will try to
chat with the mob.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Voiceless no More: find an entity with a chatterbox
    * Stop and Listen: right-click on a chatterbox to read their chatter
        * Seasoned Diplomat: chat with all sentient NPCs
    * Headpats!: left-click on a chatterbox to pet the mob
        * Friend of all Living Things: pet every mob in the game.

