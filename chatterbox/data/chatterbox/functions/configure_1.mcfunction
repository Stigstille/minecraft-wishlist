# configure_1.mcfunction

execute on vehicle run function wishlist:recall
execute store result score employer wishlist.vars run data get storage wishlist:args memories.employer
execute if score employer wishlist.vars = playerId wishlist.vars run function chatterbox:configure_2
execute unless score employer wishlist.vars = playerId wishlist.vars on vehicle run title @a[tag=wishlist.match] actionbar { "translate":"%s is not your employee", "with":[{"selector":"@s"}] }
data remove entity @s interaction


