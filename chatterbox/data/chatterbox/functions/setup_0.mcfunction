# setup_0.mcfunction

data modify storage wishlist:args entity_data set value \
        { response:1b, width:0.61f, height:0.3f }

execute if entity @s[type=#wishlist:biped] \
        run data modify storage wishlist:args entity_data set value \
        { response:1b, width:0.61f, height:-0.61f }

execute if entity @s[type=minecraft:iron_golem] \
        run data modify storage wishlist:args entity_data set value \
        { response:1b, width:1.41f, height:-0.61f }

execute if entity @s[type=minecraft:snow_golem] \
        run data modify storage wishlist:args entity_data set value \
        { response:1b, width:0.71f, height:-0.61f }

execute if entity @s[type=minecraft:witch] \
        run data modify storage wishlist:args entity_data set value \
        { response:1b, width:0.61f, height:-0.9f }

execute if entity @s[type=minecraft:wither_skeleton] \
        run data modify storage wishlist:args entity_data set value \
        { response:1b, width:0.71f, height:-0.61f }

tag @s add wishlist.chatterbox
tag @s add wishlist.mrbo61a8
execute summon minecraft:interaction run function chatterbox:setup_1
tag @s remove wishlist.mrbo61a8


