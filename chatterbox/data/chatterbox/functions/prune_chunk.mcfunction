# prune_chunk.mcfunction

# This function is tagged at #random_ticks:tick

execute as @e[type=#wishlist:mobs,tag=wishlist.chatterbox.volatile,dx=16,dy=256,dz=16] at @s unless entity @a[distance=..16] run function chatterbox:setdown

execute as @e[type=minecraft:interaction,tag=wishlist.chatterbox,dx=16,dy=256,dz=16] at @s unless entity @a[distance=..16] run function chatterbox:prune
