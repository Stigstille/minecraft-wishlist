# tick_1s.mcfunction

scoreboard players enable @a wishlist.chatterbox.classic_combat

execute at @a \
        as @e[type=minecraft:interaction,tag=wishlist.chatterbox,distance=..8] \
        at @s \
        run function chatterbox:prune

execute at @a \
        as @e[type=minecraft:interaction,tag=wishlist.chatterbox,distance=..8] \
        at @s \
        run function chatterbox:update_name

execute as @a \
        unless score @s wishlist.chatterbox.classic_combat matches 1.. \
        at @s \
        as @e[type=#wishlist:non_rideable,tag=!wishlist.chatterbox,distance=..8] \
        at @s \
        run function chatterbox:setup_volatile

execute as @a \
        if score @s wishlist.chatterbox.classic_combat matches 1.. \
        at @s \
        as @e[type=#wishlist:non_rideable,tag=wishlist.chatterbox.volatile,distance=..8] \
        run function chatterbox:setdown

execute as @a \
        at @s \
        if entity @e[tag=wishlist.chatterbox,distance=..8] \
        run advancement grant @s only wishlist:meet_chatterbox

schedule function chatterbox:tick_1s 1s



