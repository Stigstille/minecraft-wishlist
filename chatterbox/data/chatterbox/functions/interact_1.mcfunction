# interact_1.mcfunction

execute on vehicle run function wishlist:recall
execute unless data storage wishlist:args memories.chatter run data modify storage wishlist:args memories.chatter set value '{"text":"..."}'
execute on vehicle run title @a[tag=wishlist.match] actionbar { "translate":"<%s> %s", "with":[ {"selector":"@s"}, {"nbt":"memories.chatter","storage":"wishlist:args","interpret":true} ] }

data remove entity @s interaction

