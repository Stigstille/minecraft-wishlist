# interact.mcfunction

advancement revoke @s only chatterbox:interact

execute if entity @s[nbt={SelectedItem:{id:"minecraft:writable_book"}}] \
        run return run function chatterbox:configure

tag @s add wishlist.match

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=minecraft:interaction,tag=wishlist.chatterbox,distance=..4] \
        if data entity @s interaction \
        at @s \
        run function chatterbox:interact_1

data modify storage wishlist:args reason set value talk_with_npc
function better_gossip:do_good

advancement grant @s only wishlist:chat_with_mob
tag @s remove wishlist.match
