# configure.mcfunction

tag @s add wishlist.match
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
data modify storage wishlist:args chatter set from entity @s SelectedItem.tag.pages[0]

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=minecraft:interaction,tag=wishlist.chatterbox,distance=..4] \
        if data entity @s interaction \
        at @s \
        run function chatterbox:configure_1

tag @s remove wishlist.match
