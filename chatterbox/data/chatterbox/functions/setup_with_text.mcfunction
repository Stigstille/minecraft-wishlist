# setup_with_text.mcfunction

function chatterbox:setup

setblock 0 0 0 minecraft:oak_sign
data modify block 0 0 0 front_text.messages[0] set value '{"storage":"wishlist:args","nbt":"chatter"}'
function wishlist:recall
data modify storage wishlist:args memories.chatter set from block 0 0 0 front_text.messages[0]
function wishlist:memorize
setblock 0 0 0 minecraft:air

