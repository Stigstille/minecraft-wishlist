
data modify storage wishlist:args memories.chatter set from storage wishlist:args chatter
execute on vehicle run function wishlist:memorize
execute on vehicle run title @a[tag=wishlist.match] actionbar { "translate":"Updated chatter for %s", "with":[{"selector":"@s"}] }
tag @s remove wishlist.chatterbox.volatile
