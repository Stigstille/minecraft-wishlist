# pet.mcfunction

tag @s add wishlist.match

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=minecraft:interaction,tag=wishlist.chatterbox,distance=..4] \
        if data entity @s attack \
        at @s \
        run function chatterbox:pet_1

data modify storage wishlist:args reason set value headpat_npc
function better_gossip:do_good
advancement grant @s only wishlist:pet_mob
tag @s remove wishlist.match
advancement revoke @s only chatterbox:pet
