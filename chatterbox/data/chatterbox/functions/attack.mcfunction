# attack.mcfunction

tag @s add wishlist.match

data modify storage wishlist:args args set value { damage:1 }

execute if entity @s[nbt={SelectedItem:{id:"minecraft:wooden_sword"}}] \
        run data modify storage wishlist:args args.damage set value 4
execute if entity @s[nbt={SelectedItem:{id:"minecraft:gold_sword"}}] \
        run data modify storage wishlist:args args.damage set value 4
execute if entity @s[nbt={SelectedItem:{id:"minecraft:stone_sword"}}] \
        run data modify storage wishlist:args args.damage set value 5
execute if entity @s[nbt={SelectedItem:{id:"minecraft:iron_sword"}}] \
        run data modify storage wishlist:args args.damage set value 6
execute if entity @s[nbt={SelectedItem:{id:"minecraft:diamond_sword"}}] \
        run data modify storage wishlist:args args.damage set value 7
execute if entity @s[nbt={SelectedItem:{id:"minecraft:netherite_sword"}}] \
        run data modify storage wishlist:args args.damage set value 8

execute if entity @s[nbt={SelectedItem:{id:"minecraft:wooden_shovel"}}] \
        run data modify storage wishlist:args args.damage set value 2.5
execute if entity @s[nbt={SelectedItem:{id:"minecraft:gold_shovel"}}] \
        run data modify storage wishlist:args args.damage set value 2.5
execute if entity @s[nbt={SelectedItem:{id:"minecraft:stone_shovel"}}] \
        run data modify storage wishlist:args args.damage set value 3.5
execute if entity @s[nbt={SelectedItem:{id:"minecraft:iron_shovel"}}] \
        run data modify storage wishlist:args args.damage set value 4.5
execute if entity @s[nbt={SelectedItem:{id:"minecraft:diamond_shovel"}}] \
        run data modify storage wishlist:args args.damage set value 5.5
execute if entity @s[nbt={SelectedItem:{id:"minecraft:netherite_shovel"}}] \
        run data modify storage wishlist:args args.damage set value 6.5

execute if entity @s[nbt={SelectedItem:{id:"minecraft:wooden_pickaxe"}}] \
        run data modify storage wishlist:args args.damage set value 2
execute if entity @s[nbt={SelectedItem:{id:"minecraft:gold_pickaxe"}}] \
        run data modify storage wishlist:args args.damage set value 2
execute if entity @s[nbt={SelectedItem:{id:"minecraft:stone_pickaxe"}}] \
        run data modify storage wishlist:args args.damage set value 3
execute if entity @s[nbt={SelectedItem:{id:"minecraft:iron_pickaxe"}}] \
        run data modify storage wishlist:args args.damage set value 4
execute if entity @s[nbt={SelectedItem:{id:"minecraft:diamond_pickaxe"}}] \
        run data modify storage wishlist:args args.damage set value 5
execute if entity @s[nbt={SelectedItem:{id:"minecraft:netherite_pickaxe"}}] \
        run data modify storage wishlist:args args.damage set value 6

execute if entity @s[nbt={SelectedItem:{id:"minecraft:wooden_axe"}}] \
        run data modify storage wishlist:args args.damage set value 7
execute if entity @s[nbt={SelectedItem:{id:"minecraft:gold_axe"}}] \
        run data modify storage wishlist:args args.damage set value 7
execute if entity @s[nbt={SelectedItem:{id:"minecraft:stone_axe"}}] \
        run data modify storage wishlist:args args.damage set value 9
execute if entity @s[nbt={SelectedItem:{id:"minecraft:iron_axe"}}] \
        run data modify storage wishlist:args args.damage set value 9
execute if entity @s[nbt={SelectedItem:{id:"minecraft:diamond_axe"}}] \
        run data modify storage wishlist:args args.damage set value 9
execute if entity @s[nbt={SelectedItem:{id:"minecraft:netherite_axe"}}] \
        run data modify storage wishlist:args args.damage set value 10

execute if entity @s[nbt={SelectedItem:{id:"minecraft:trident"}}] \
        run data modify storage wishlist:args args.damage set value 9

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=minecraft:interaction,tag=wishlist.chatterbox,distance=..4] \
        if data entity @s attack on vehicle \
        run function chatterbox:attack_1 with storage wishlist:args args

tag @s remove wishlist.match
advancement revoke @s only chatterbox:attack

