
data remove storage wishlist:args name
execute on vehicle \
        if data entity @s CustomName \
        run data modify storage wishlist:args name set from entity @s CustomName
execute if data storage wishlist:args name \
        run data modify entity @s CustomName set from storage wishlist:args name
