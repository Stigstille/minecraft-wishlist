# pet_1.mcfunction

particle minecraft:heart ~ ~ ~ 0.2 0.2 0.2 1 3 normal

execute if entity @s[type=axolotl] run playsound minecraft:entity.axolotl.ambient neutral @a
execute if entity @s[type=bat] run playsound minecraft:entity.bat.ambient neutral @a
execute if entity @s[type=cat] run playsound minecraft:entity.cat.ambient neutral @a
execute if entity @s[type=chicken] run playsound minecraft:entity.chicken.ambient neutral @a
execute if entity @s[type=cod] run playsound minecraft:entity.cod.ambient neutral @a
execute if entity @s[type=cow] run playsound minecraft:entity.cow.ambient neutral @a
execute if entity @s[type=donkey] run playsound minecraft:entity.donkey.ambient neutral @a
execute if entity @s[type=fox] run playsound minecraft:entity.fox.ambient neutral @a
execute if entity @s[type=glow_squid] run playsound minecraft:entity.glow_squid.ambient neutral @a
execute if entity @s[type=horse] run playsound minecraft:entity.horse.ambient neutral @a
execute if entity @s[type=mooshroom] run playsound minecraft:entity.mooshroom.ambient neutral @a
execute if entity @s[type=mule] run playsound minecraft:entity.mule.ambient neutral @a
execute if entity @s[type=ocelot] run playsound minecraft:entity.ocelot.ambient neutral @a
execute if entity @s[type=parrot] run playsound minecraft:entity.parrot.ambient neutral @a
execute if entity @s[type=pig] run playsound minecraft:entity.pig.ambient neutral @a
execute if entity @s[type=pufferfish] run playsound minecraft:entity.pufferfish.ambient neutral @a
execute if entity @s[type=rabbit] run playsound minecraft:entity.rabbit.ambient neutral @a
execute if entity @s[type=salmon] run playsound minecraft:entity.salmon.ambient neutral @a
execute if entity @s[type=sheep] run playsound minecraft:entity.sheep.ambient neutral @a
execute if entity @s[type=skeleton_horse] run playsound minecraft:entity.skeleton_horse.ambient neutral @a
execute if entity @s[type=snow_golem] run playsound minecraft:entity.snow_golem.ambient neutral @a
execute if entity @s[type=squid] run playsound minecraft:entity.squid.ambient neutral @a
execute if entity @s[type=strider] run playsound minecraft:entity.strider.ambient neutral @a
execute if entity @s[type=tropical_fish] run playsound minecraft:entity.tropical_fish.ambient neutral @a
execute if entity @s[type=turtle] run playsound minecraft:entity.turtle.ambient neutral @a
execute if entity @s[type=villager] run playsound minecraft:entity.villager.ambient neutral @a
execute if entity @s[type=wandering_trader] run playsound minecraft:entity.wandering_trader.ambient neutral @a

execute if entity @s[type=bee] run playsound minecraft:entity.bee.ambient neutral @a
execute if entity @s[type=cave_spider] run playsound minecraft:entity.cave_spider.ambient neutral @a
execute if entity @s[type=dolphin] run playsound minecraft:entity.dolphin.ambient neutral @a
execute if entity @s[type=enderman] run playsound minecraft:entity.enderman.ambient neutral @a
execute if entity @s[type=goat] run playsound minecraft:entity.goat.ambient neutral @a
execute if entity @s[type=iron_golem] run playsound minecraft:entity.iron_golem.ambient neutral @a
execute if entity @s[type=llama] run playsound minecraft:entity.llama.ambient neutral @a
execute if entity @s[type=panda] run playsound minecraft:entity.panda.ambient neutral @a
execute if entity @s[type=piglin] run playsound minecraft:entity.piglin.ambient neutral @a
execute if entity @s[type=polar_bear] run playsound minecraft:entity.polar_bear.ambient neutral @a
execute if entity @s[type=spider] run playsound minecraft:entity.spider.ambient neutral @a
execute if entity @s[type=trader_llama] run playsound minecraft:entity.trader_llama.ambient neutral @a
execute if entity @s[type=wolf] run playsound minecraft:entity.wolf.ambient neutral @a
execute if entity @s[type=zombified_piglin] run playsound minecraft:entity.zombified_piglin.ambient neutral @a

execute if entity @s[type=blaze] run playsound minecraft:entity.blaze.ambient neutral @a
execute if entity @s[type=creeper] run playsound minecraft:entity.creeper.ambient neutral @a
execute if entity @s[type=drowned] run playsound minecraft:entity.drowned.ambient neutral @a
execute if entity @s[type=elder_guardian] run playsound minecraft:entity.elder_guardian.ambient neutral @a
execute if entity @s[type=endermite] run playsound minecraft:entity.endermite.ambient neutral @a
execute if entity @s[type=evoker] run playsound minecraft:entity.evoker.ambient neutral @a
execute if entity @s[type=ghast] run playsound minecraft:entity.ghast.ambient neutral @a
execute if entity @s[type=guardian] run playsound minecraft:entity.guardian.ambient neutral @a
execute if entity @s[type=hoglin] run playsound minecraft:entity.hoglin.ambient neutral @a
execute if entity @s[type=husk] run playsound minecraft:entity.husk.ambient neutral @a
execute if entity @s[type=magma_cube] run playsound minecraft:entity.magma_cube.ambient neutral @a
execute if entity @s[type=phantom] run playsound minecraft:entity.phantom.ambient neutral @a
execute if entity @s[type=piglin_brute] run playsound minecraft:entity.piglin_brute.ambient neutral @a
execute if entity @s[type=pillager] run playsound minecraft:entity.pillager.ambient neutral @a
execute if entity @s[type=ravager] run playsound minecraft:entity.ravager.ambient neutral @a
execute if entity @s[type=shulker] run playsound minecraft:entity.shulker.ambient neutral @a
execute if entity @s[type=silverfish] run playsound minecraft:entity.silverfish.ambient neutral @a
execute if entity @s[type=skeleton] run playsound minecraft:entity.skeleton.ambient neutral @a
execute if entity @s[type=slime] run playsound minecraft:entity.slime.ambient neutral @a
execute if entity @s[type=stray] run playsound minecraft:entity.stray.ambient neutral @a
execute if entity @s[type=vex] run playsound minecraft:entity.vex.ambient neutral @a
execute if entity @s[type=vindicator] run playsound minecraft:entity.vindicator.ambient neutral @a
execute if entity @s[type=witch] run playsound minecraft:entity.witch.ambient neutral @a
execute if entity @s[type=wither_skeleton] run playsound minecraft:entity.wither_skeleton.ambient neutral @a
execute if entity @s[type=zoglin] run playsound minecraft:entity.zoglin.ambient neutral @a
execute if entity @s[type=zombie] run playsound minecraft:entity.zombie.ambient neutral @a
execute if entity @s[type=zombie_villager] run playsound minecraft:entity.zombie_villager.ambient neutral @a

data remove entity @s attack

