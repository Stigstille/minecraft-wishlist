# summon_npc.mcfunction - Summon an NPC

data modify storage wishlist:args stash set from storage wishlist:args tag.entityTag
data modify storage wishlist:args stash.Tags append value wishlist.summon
function stash:pop

execute as @e[tag=wishlist.summon,distance=0,limit=1] \
        run function wishlist:recall
execute store result storage wishlist:args memories.employer int 1 \
        run scoreboard players get @s wishlist.playerId
execute as @e[tag=wishlist.summon,distance=0,limit=1] \
        run function wishlist:memorize
tag @e remove wishlist.summon
