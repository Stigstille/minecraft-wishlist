# player_skip.mcfunction

execute as @a[distance=..2,tag=!wishlist.hidden] \
        run function magic_tools:impl/record_skip
data modify storage wishlist:args uuid set from storage wishlist:args tag.uuid
function wishlist:find_player_by_uuid
execute as @a[tag=wishlist.match] \
        at @s \
        positioned ~-16 ~-8 ~-16 \
        if entity @e[type=marker,tag=wishlist.dungeon_room,dx=16,dy=16,dz=16] \
        run tag @s remove wishlist.match
tp @a[distance=..2,tag=!wishlist.hidden] @a[tag=wishlist.match,tag=!wishlist.hidden,gamemode=!spectator,limit=1]
tag @a remove wishlist.match
