# impl/start_spectating.mcfunction

function magic_tools:impl/record_skip
tag @s add wishlist.spectator
gamemode spectator @s
spectate @a[tag=wishlist.match,limit=1] @s
scoreboard players operation @s wishlist.spectating = @a[tag=wishlist.match,limit=1] wishlist.playerId
tellraw @a[tag=wishlist.match,limit=1] {"translate":"wishlist.magic_tools.start_spectating","with":[{"selector":"@s"}],"color":"dark_gray"}
