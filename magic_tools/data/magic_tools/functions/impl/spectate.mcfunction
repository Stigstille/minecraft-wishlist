# impl/spectate.mcfunction

# Spectee is the player whose wishlist.playerId matches wishlist.spectating
scoreboard players operation spectee wishlist.vars = @s wishlist.spectating
execute as @a \
        if score @s wishlist.playerId = spectee wishlist.vars \
        run tag @s add wishlist.spectee

# Stop spectating if:
# - the spectator crouches,
# - the spectee can't be found
execute unless entity @a[tag=wishlist.spectee,tag=!wishlist.hidden,gamemode=!spectator] \
        run scoreboard players set @s mt_crouch 1
execute if score @s mt_crouch matches 1.. \
        run function magic_tools:impl/stop_spectating

# Otherwise ensure that the spectator spectates the spectee instead of getting
# out in any of the ways the game UI allows.
execute unless score @s mt_crouch matches 1.. \
        run spectate @a[tag=wishlist.spectee,limit=1] @s

tag @a remove wishlist.spectee
