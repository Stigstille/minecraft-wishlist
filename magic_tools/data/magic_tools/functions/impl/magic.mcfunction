# magic.mcfunction - break magic egg, and run appropriate magic

particle minecraft:flash ~ ~ ~

execute store result score magic mt_ \
        run data get storage wishlist:args tag.magic2

execute if score magic mt_ matches 0 run function magic_tools:aura_sight/cast
execute if score magic mt_ matches 1 run function magic_tools:health_sight/cast
execute if score magic mt_ matches 2 run function magic_tools:job_sight/cast
execute if score magic mt_ matches 3 run function magic_tools:home_sight/cast
execute if score magic mt_ matches 4 run function magic_tools:pocket_sight/cast
execute if score magic mt_ matches 5 run function magic_tools:rep_sight/cast
execute if score magic mt_ matches 6 \
        run function magic_tools:lodestone_skip/cast
execute if score magic mt_ matches 7 \
        run function magic_tools:villager_steal/cast
execute if score magic mt_ matches 12 \
        run function magic_tools:summon_enderfolk/cast
execute if score magic mt_ matches 13 \
        run function magic_tools:summon_evoker/cast
execute if score magic mt_ matches 17 \
        run function magic_tools:summon_illusioner/cast
execute if score magic mt_ matches 22 \
        run function magic_tools:summon_piglin/cast
execute if score magic mt_ matches 23 \
        run function magic_tools:summon_piglin_brute/cast
execute if score magic mt_ matches 24 \
        run function magic_tools:summon_pillager/cast
execute if score magic mt_ matches 27 \
        run function magic_tools:summon_villager/cast
execute if score magic mt_ matches 28 \
        run function magic_tools:summon_vindicator/cast
execute if score magic mt_ matches 30 \
        run function magic_tools:summon_witch/cast
# 31 used to be villager child; it's now done with an extra tag in summon villager
# 32 used to be piglin child; it's now done with an extra tag in summon piglin
# 33 is mimic magic; it works by seasoning
# 34 is the book of bù; it works by seasoning
execute if score magic mt_ matches 35 run function magic_tools:skip_back/cast
execute if score magic mt_ matches 36 run function magic_tools:player_skip/cast
# 37 is dismiss summon; it works by seasoning
execute if score magic mt_ matches 38 run function magic_tools:bubblevator/cast
# 39 is mimic frame; it works by seasoning
execute if score magic mt_ matches 40 run function magic_tools:map_skip/cast
execute if score magic mt_ matches 41 \
        run function magic_tools:player_sight/cast
execute if score magic mt_ matches 42 run function magic_tools:hide_player/cast
execute if score magic mt_ matches 43 run function magic_tools:home_skip/cast
execute if score magic mt_ matches 44 run function magic_tools:bait_npc/cast
# 45 is true sight; it works automatically by carrying the item
execute if score magic mt_ matches 46 \
        run function magic_tools:dismiss_bait/cast
execute if score magic mt_ matches 47 \
        run function magic_tools:summon_gateway/cast
execute if score magic mt_ matches 48 \
        run function magic_tools:summon_player/cast
execute if score magic mt_ matches 49 \
        run function magic_tools:summon_npc/cast
