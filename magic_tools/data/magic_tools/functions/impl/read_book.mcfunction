# read_book.mcfunction - trigger spell in book

data modify storage wishlist:args tag set value {magic2:-1}
execute if score @s mt_book matches 0 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[0]
execute if score @s mt_book matches 1 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[1]
execute if score @s mt_book matches 2 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[2]
execute if score @s mt_book matches 3 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[3]
execute if score @s mt_book matches 4 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[4]
execute if score @s mt_book matches 5 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[5]
execute if score @s mt_book matches 6 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[6]
execute if score @s mt_book matches 7 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[7]
execute if score @s mt_book matches 8 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[8]
execute if score @s mt_book matches 9 \
        run data modify storage wishlist:args tag set from entity @s SelectedItem.tag.tags[9]
function magic_tools:impl/magic
advancement grant @s only wishlist:use_magic_book
