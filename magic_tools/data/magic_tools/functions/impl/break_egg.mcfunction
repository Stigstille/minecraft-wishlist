# break_egg.mcfunction - break magic egg, and run appropriate magic

playsound minecraft:entity.zombie.destroy_egg player @a ~ ~ ~
data modify storage wishlist:args uuid set from entity @s Owner
function wishlist:find_player_by_uuid
data modify storage wishlist:args tag set from entity @s Item.tag
execute as @a[tag=wishlist.match] at @s run function magic_tools:impl/magic
advancement grant @a[tag=wishlist.match] only wishlist:use_magic_egg
tag @a remove wishlist.match
kill @s
