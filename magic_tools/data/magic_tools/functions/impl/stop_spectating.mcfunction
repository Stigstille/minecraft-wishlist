# stop_spectating.mcfunction

tellraw @a[tag=wishlist.spectee] {"translate":"wishlist.magic_tools.stop_spectating","with":[{"selector":"@s"}],"color":"dark_gray"}

gamemode survival @s
tag @s remove wishlist.spectator
scoreboard players reset @s wishlist.spectating

data modify storage wishlist:args tag set value {pos:[0.0d,0.0d,0.0d],dim:0}
execute store result storage wishlist:args tag.pos[0] double 0.0625 \
        run scoreboard players get @s mt_back_x
execute store result storage wishlist:args tag.pos[1] double 0.0625 \
        run scoreboard players get @s mt_back_y
execute store result storage wishlist:args tag.pos[2] double 0.0625 \
        run scoreboard players get @s mt_back_z
execute store result storage wishlist:args tag.dim double 1 \
        run scoreboard players get @s mt_back_d
tag @s add mt_teleport_subject
function magic_tools:impl/teleport
tag @s remove mt_teleport_subject

function magic_tools:impl/record_skip
