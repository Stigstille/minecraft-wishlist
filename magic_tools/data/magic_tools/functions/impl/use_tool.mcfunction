# use_tool.mcfunction - run appropriate magic

data modify storage wishlist:args tag set from entity @s SelectedItem.tag
function magic_tools:impl/magic
execute if entity @s[nbt={SelectedItem:{tag:{CustomModelData:271829}}}] \
        run advancement grant @s only wishlist:use_noise_box
execute if entity @s[nbt={SelectedItem:{tag:{CustomModelData:271830}}}] \
        run advancement grant @s only wishlist:use_magic_wand
