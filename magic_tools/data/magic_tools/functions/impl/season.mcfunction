# season_egg.mcfunction - Update data for magic egg items

execute store result score magic mt_ run data get entity @s Item.tag.magic2

execute if score magic mt_ matches 6 \
        run function magic_tools:lodestone_skip/season
execute if score magic mt_ matches 33 \
        run function magic_tools:mimic_magic/season
execute if score magic mt_ matches 34 run function magic_tools:book/season
execute if score magic mt_ matches 36 \
        run function magic_tools:player_skip/season
execute if score magic mt_ matches 37 \
        run function magic_tools:dismiss_summon/season
execute if score magic mt_ matches 39 \
        run function magic_tools:mimic_frame/season
execute if score magic mt_ matches 41 \
        run function magic_tools:player_sight/season
execute if score magic mt_ matches 47 \
        run function magic_tools:summon_gateway/season
execute if score magic mt_ matches 48 \
        run function magic_tools:summon_player/season
