# load.mcfunction - initialize magic_tools

execute in minecraft:overworld run forceload add 0 0
execute in minecraft:the_nether run forceload add 0 0
execute in minecraft:the_end run forceload add 0 0
execute in wishlist:sketch run forceload add 0 0
execute in wishlist:ocean run forceload add 0 0
execute in wishlist:sky run forceload add 0 0

scoreboard objectives add mt_ dummy
scoreboard objectives add mt_book trigger
scoreboard objectives add mt_spyglass minecraft.used:minecraft.spyglass
scoreboard objectives add mt_back_x dummy
scoreboard objectives add mt_back_y dummy
scoreboard objectives add mt_back_z dummy
scoreboard objectives add mt_back_d dummy
scoreboard objectives add mt_death deathCount
scoreboard objectives add mt_crouch minecraft.custom:minecraft.sneak_time
scoreboard objectives add wishlist.spectating dummy
scoreboard objectives add wishlist.mt.used_carrot_on_a_stick minecraft.used:minecraft.carrot_on_a_stick

team add wishlist.npc_poi
team modify wishlist.npc_poi collisionRule never

team add wishlist.red
team modify wishlist.red color red
team add wishlist.green
team modify wishlist.green color green
team add wishlist.blue
team modify wishlist.blue color blue

data modify storage wishlist:args all_chatter append value \
        "Colored eggs everywhere, nowadays; I like how they look"
data modify storage wishlist:args all_chatter append value \
        "Do you have a Magic Wand? those things are pretty useful"
data modify storage wishlist:args all_chatter append value \
        "Ever used a Lens? I always get the colors mixed up"
data modify storage wishlist:args all_chatter append value \
        "The Noise Box is an awesome tool, but it can prevent your own tools from working"
data modify storage wishlist:args all_chatter append value \
        "A Magic Book can hold up to ten eggs, ever wondered why ten?"

function magic_tools:impl/tick_1t
function magic_tools:impl/tick_1s
