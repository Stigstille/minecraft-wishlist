# tick_1t.mcfunction - update magic egg items and thrown magic eggs

schedule function magic_tools:impl/tick_1t 1t

# Magic eggs will only work around a player
execute at @a run function magic_tools:impl/tick_eggs

# Books are triggered when read
execute as @a[gamemode=!spectator,scores={mt_book=0..}] \
        at @s \
        run function magic_tools:impl/read_book
scoreboard players enable @a mt_book
scoreboard players set @a mt_book -1

# Spyglasses are triggered when used
execute as @a[scores={mt_spyglass=1..}] \
        at @s \
        if data entity @s SelectedItem.tag.magic2 \
        run function magic_tools:impl/use_tool
scoreboard players set @a mt_spyglass 0

# Carrots on a stick are triggered when used
execute as @a[scores={wishlist.mt.used_carrot_on_a_stick=1..}] \
        at @s \
        if data entity @s SelectedItem.tag.magic2 \
        run function magic_tools:impl/use_tool
scoreboard players reset * wishlist.mt.used_carrot_on_a_stick

# If a player dies, store the position for back skip eggs
execute as @a[scores={mt_death=1..}] run function magic_tools:impl/record_skip
scoreboard players reset * mt_death

# Spectators are bound to their players until dismounting
execute as @a[tag=wishlist.spectator] run function magic_tools:impl/spectate
scoreboard players reset * mt_crouch

