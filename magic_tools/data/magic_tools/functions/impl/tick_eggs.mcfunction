# impl/tick_eggs.mcfunction

# Magic items that are in item form get "seasoned"
execute as @e[type=item,distance=..4] \
        at @s \
        if data entity @s Item.tag.magic2 \
        at @s \
        run function magic_tools:impl/season

# Eggs are triggered when thrown
execute as @e[type=egg,distance=..4] \
        at @s \
        if data entity @s Item.tag.magic2 \
        run function magic_tools:impl/break_egg

