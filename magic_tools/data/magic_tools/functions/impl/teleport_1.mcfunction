# impl/teleport_1.mcfunction - move target, teleport subjects, kill target

data modify entity @s Pos set from storage wishlist:args tag.pos
execute at @s \
        unless block ~ ~ ~ minecraft:air \
        run tp @s ~ ~1 ~
tp @a[tag=mt_teleport_subject] @s
kill @s
