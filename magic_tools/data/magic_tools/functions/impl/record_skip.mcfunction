# impl/record_skip.mcfunction - store position and dimension before skip

execute store result score @s mt_back_x run data get entity @s Pos[0] 16
execute store result score @s mt_back_y run data get entity @s Pos[1] 16
execute store result score @s mt_back_z run data get entity @s Pos[2] 16
execute if entity @s[nbt={Dimension:"minecraft:overworld"}] \
        run scoreboard players set @s mt_back_d 0
execute if entity @s[nbt={Dimension:"minecraft:the_nether"}] \
        run scoreboard players set @s mt_back_d 1
execute if entity @s[nbt={Dimension:"minecraft:the_end"}] \
        run scoreboard players set @s mt_back_d 2
execute if entity @s[nbt={Dimension:"wishlist:sketch"}] \
        run scoreboard players set @s mt_back_d 3
execute if entity @s[nbt={Dimension:"wishlist:ocean"}] \
        run scoreboard players set @s mt_back_d 4
execute if entity @s[nbt={Dimension:"wishlist:sky"}] \
        run scoreboard players set @s mt_back_d 5
execute at @s run particle minecraft:portal ~ ~0.5 ~ 0.35 0.45 0.35 0.5 20

# recording will fail if the player is inside a dungeon room

execute at @s \
        positioned ~-16 ~-8 ~-16 \
        if entity @e[type=marker,tag=wishlist.dungeon_room,dx=16,dy=16,dz=16] \
        run scoreboard players set @s mt_back_d -1
