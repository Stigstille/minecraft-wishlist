# impl/teleport.mcfunction - backup position, then teleport to target

execute as @a[tag=mt_teleport_subject] \
        run function magic_tools:impl/record_skip

execute store result score dim mt_ run data get storage wishlist:args tag.dim
execute if score dim mt_ matches 0 \
        in minecraft:overworld \
        run summon minecraft:marker 0 0 0 {Tags:["mt_teleport_target"]}
execute if score dim mt_ matches 1 \
        in minecraft:the_nether \
        run summon minecraft:marker 0 0 0 {Tags:["mt_teleport_target"]}
execute if score dim mt_ matches 2 \
        in minecraft:the_end \
        run summon minecraft:marker 0 0 0 {Tags:["mt_teleport_target"]}
execute if score dim mt_ matches 3 \
        in wishlist:sketch \
        run summon minecraft:marker 0 0 0 {Tags:["mt_teleport_target"]}
execute if score dim mt_ matches 4 \
        in wishlist:ocean \
        run summon minecraft:marker 0 0 0 {Tags:["mt_teleport_target"]}
execute if score dim mt_ matches 5 \
        in wishlist:sky \
        run summon minecraft:marker 0 0 0 {Tags:["mt_teleport_target"]}
execute as @e[tag=mt_teleport_target] run function magic_tools:impl/teleport_1

