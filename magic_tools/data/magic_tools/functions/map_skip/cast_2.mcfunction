
execute store result score x1 wishlist.vars run data get entity @s Pos[0] 512
execute store result score z1 wishlist.vars run data get entity @s Pos[2] 512
execute store result score x0 wishlist.vars \
        run data get entity @e[tag=wishlist.globe,sort=nearest,limit=1] Pos[0] 512
execute store result score z0 wishlist.vars \
        run data get entity @e[tag=wishlist.globe,sort=nearest,limit=1] Pos[2] 512
scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars
execute store result entity @s Pos[0] double 1 \
        run scoreboard players operation x1 wishlist.vars += dx wishlist.vars
execute store result entity @s Pos[2] double 1 \
        run scoreboard players operation z1 wishlist.vars += dz wishlist.vars
execute store success score success wishlist.vars \
        at @s run spreadplayers ~ ~ 1 1 false @a[tag=wishlist.map_skip_subject]
kill @s

advancement grant @a[tag=wishlist.map_skip_subject] only wishlist:use_map_skip
