
scoreboard players set go wishlist.vars 0
execute positioned ~-12 ~-12 ~-12 \
        as @e[tag=wishlist.globe,dx=24,dy=24,dz=24] \
        run function magic_tools:map_skip/cast_0
execute if score go wishlist.vars matches 1 \
        run function magic_tools:map_skip/cast_1
execute if score go wishlist.vars matches 0 \
        run return run title @p actionbar {"translate":"wishlist.magic_tools.missing_map_marker"}
execute unless score success wishlist.vars matches 1 \
        run return run title @p actionbar {"text":"You can't skip to water, lava, fire, or void"}



