
tag @a[distance=..2,tag=!wishlist.hidden] add wishlist.map_skip_subject
execute as @a[tag=wishlist.map_skip_subject] \
        run function magic_tools:impl/record_skip
summon minecraft:marker ~ ~ ~ {Tags:[wishlist.map_skip_marker]}
execute as @e[tag=wishlist.map_skip_marker] \
        run function magic_tools:map_skip/cast_2
tag @a remove wishlist.map_skip_subject
