# calculate the coordinates of the northwest corner of the map

scoreboard players set scale wishlist.vars 512

execute store result score dx wishlist.vars run data get entity @s Pos[0] 1
scoreboard players add dx wishlist.vars 64
scoreboard players operation dx wishlist.vars /= scale wishlist.vars
scoreboard players operation dx wishlist.vars *= scale wishlist.vars
scoreboard players add dx wishlist.vars 192

execute store result score dz wishlist.vars run data get entity @s Pos[2] 1
scoreboard players add dz wishlist.vars 64
scoreboard players operation dz wishlist.vars /= scale wishlist.vars
scoreboard players operation dz wishlist.vars *= scale wishlist.vars
scoreboard players add dz wishlist.vars 192

scoreboard players set go wishlist.vars 1
