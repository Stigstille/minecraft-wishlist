# summon_enderfolk.mcfunction - Summon an NPC

execute at @p run summon minecraft:enderman ~ ~ ~ {Tags:["wishlist.summon"]}
execute as @e[tag=wishlist.summon,distance=0,limit=1] \
        run function wishlist:recall
execute store result storage wishlist:args memories.employer int 1 \
        run scoreboard players get @p wishlist.playerId
execute as @e[tag=wishlist.summon,distance=0,limit=1] \
        run function wishlist:memorize
data modify entity @e[tag=wishlist.summon,limit=1] {} merge from storage wishlist:args tag.entityTag
tag @e remove wishlist.summon

