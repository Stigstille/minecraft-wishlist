
scoreboard players set go wishlist.vars 0
execute as @e[type=#wishlist:item_frames,distance=..0.7,nbt={Item:{id:"minecraft:filled_map"}},sort=nearest,limit=1] \
        run function magic_tools:mimic_frame/season_1
execute unless score go wishlist.vars matches 0 \
        run function magic_tools:mimic_frame/season_2
