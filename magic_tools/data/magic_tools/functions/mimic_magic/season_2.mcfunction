# mimic.mcfunction - copy spell from nearest magic item

data modify storage wishlist:args tag merge from entity @s Item.tag
data remove storage wishlist:args tag.CustomModelData
particle minecraft:instant_effect ~ ~ ~ 0.1 0.1 0.1 0.005 10
playsound minecraft:block.anvil.use player @a ~ ~ ~
