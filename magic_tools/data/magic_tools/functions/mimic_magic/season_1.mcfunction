# try_mimic.mcfunction - copy spell from nearest magic item

tag @s add mt_self
data modify storage wishlist:args tag set from entity @s Item.tag
execute as @e[type=minecraft:item,nbt=!{Item:{tag:{magic2:33}}},distance=..0.5,sort=nearest,limit=1] \
        if data entity @s Item.tag.magic2 \
        unless data entity @s Item.tag.noncopyable \
        run function magic_tools:mimic_magic/season_2
data modify entity @s Item.tag merge from storage wishlist:args tag
tag @s remove mt_self

