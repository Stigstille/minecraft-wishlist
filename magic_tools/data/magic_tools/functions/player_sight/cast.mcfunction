# player_skip.mcfunction

data modify storage wishlist:args uuid set from storage wishlist:args tag.uuid
function wishlist:find_player_by_uuid
execute if entity @a[tag=wishlist.match,tag=!wishlist.hidden,gamemode=!spectator] \
        as @a[tag=!wishlist.match,distance=..2] \
        run function magic_tools:impl/start_spectating
tag @a remove wishlist.match
