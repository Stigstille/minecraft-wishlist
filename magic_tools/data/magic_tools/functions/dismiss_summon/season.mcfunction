# dismiss_summon/season.mcfunction - dismiss summoned npc
# Entrypoint for summon-dismissing: 
# - store playerId of egg thrower
# - attempt to dismiss closest entity to the egg

tag @s add wishlist.dismiss_egg
execute on origin \
        run scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
execute as @e[type=!minecraft:player,tag=!wishlist.dismiss_egg,distance=..1,sort=nearest,limit=1] \
        run function magic_tools:dismiss_summon/season_1
tag @s remove wishlist.dismiss_egg

