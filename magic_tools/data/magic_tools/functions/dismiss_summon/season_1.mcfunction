# dismiss_summon/season_1.mcfunction
# dismiss @s but only if their employer is wishlist:args args.playerId

function wishlist:recall
scoreboard players set employer wishlist.vars -1
execute store result score employer wishlist.vars \
        run data get storage wishlist:args memories.employer
execute if score employer wishlist.vars = playerId wishlist.vars \
        run function magic_tools:dismiss_summon/season_2

