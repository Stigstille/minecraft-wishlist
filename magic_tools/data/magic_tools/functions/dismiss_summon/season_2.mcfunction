# dismiss_summon/season_2.mcfunction
# dismiss @s and construct the data for the new summon egg

function stash:stash
data modify storage wishlist:args tag set value { \
        CustomModelData:271828, \
        Enchantments:[{id:"minecraft:unbreaking",lvl:1}], \
        HideFlags:1b, \
        magic2:49 \
        }
data modify storage wishlist:args tag.entityTag set from storage wishlist:args stash
data remove storage wishlist:args tag.entityTag.UUID
data remove storage wishlist:args tag.entityTag.Pos

# Construct the name of the new summon egg
tag @s add wishlist.dismissed
setblock 0 0 0 oak_sign
data modify block 0 0 0 front_text.messages[0] set value \
        '[{"text":"Summon "},{"selector":"@e[tag=wishlist.dismissed]"}]'
data modify storage wishlist:args tag.display.Name set from block 0 0 0 front_text.messages[0]
setblock 0 0 0 air
data modify storage wishlist:args tag.display.Lore set value ['[{"text":"Summon dismissed NPC","italic":false,"color":"gray"}]']

# "Kill" the entity being dismissed
data merge entity @s {DeathTime:19,DeathLootTable:"minecraft:empty"}
kill @s

# create a new summon egg
summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:egg",Count:1b},Tags:[wishlist.summon_egg]}
data modify entity @e[tag=wishlist.summon_egg,limit=1] Item.tag set from storage wishlist:args tag
tag @e remove wishlist.summon_egg

# reduce stack count of egg item by one
execute as @e[tag=wishlist.dismiss_egg] \
        run function wishlist:decrement_item_entity_count
