
setblock ~ ~ ~ minecraft:water keep
setblock ~-1 ~ ~ minecraft:moving_piston keep
setblock ~1 ~ ~ minecraft:moving_piston keep
setblock ~ ~ ~-1 minecraft:moving_piston keep
setblock ~ ~ ~1 minecraft:moving_piston keep
execute positioned ~ ~1 ~ \
        if block ~ ~ ~ minecraft:air \
        run function magic_tools:bubblevator/cast

