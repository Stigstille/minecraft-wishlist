# dismiss_bait/cast_1.mcfunction

execute if entity @s[type=minecraft:marker] \
        store result score employer wishlist.vars \
        run data get entity @s data.employer
execute if entity @s[type=!minecraft:marker] \
        store result score employer wishlist.vars \
        run data get entity @s ArmorItems[3].tag.employer
execute if score employer wishlist.vars = playerId wishlist.vars \
        run function magic_tools:dismiss_bait/cast_2

