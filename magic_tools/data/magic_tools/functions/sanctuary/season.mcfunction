# sanctuary.mcfunction - Generate sanctuary area

execute at @s align xyz run tp @s ~0.5 ~ ~0.5
data modify entity @s Age set value 0
execute as @e[type=minecraft:piglin,distance=..64] \
        run data merge entity @s {IsImmuneToZombification:1b}
execute as @e[type=minecraft:piglin_brute,distance=..64] \
        run data merge entity @s {IsImmuneToZombification:1b}
execute as @e[type=minecraft:pillager,distance=..64] \
        run item replace entity @s weapon.mainhand with minecraft:air
execute as @e[type=minecraft:vindicator,distance=..64] \
        run item replace entity @s weapon.mainhand with minecraft:air
kill @e[type=minecraft:potion,distance=..64]
kill @e[type=#minecraft:arrows,distance=..64]
kill @e[type=minecraft:fireball,distance=..64]
kill @e[type=minecraft:small_fireball,distance=..64]
particle minecraft:electric_spark ~ ~ ~ 0.1 0.1 0.1 0.05 1
