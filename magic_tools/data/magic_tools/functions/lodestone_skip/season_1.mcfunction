# lodestone_skip/season.mcfunction - bind egg to lodestone

# This function has been updated to use the new "on origin" idiom for execute,
# and to use the success trick to compare dimension names.

execute at @s align xyz run tp @s ~0.5 ~ ~0.5

data modify storage wishlist:args tag set from entity @s Item.tag
data modify storage wishlist:args tag.pos set from entity @s Pos
execute on origin \
        run data modify storage wishlist:args tag.dimension \
        set from entity @s Dimension

data modify storage wishlist:args tmp set from storage wishlist:args tag.dimension
execute store success score go wishlist.vars \
        run data modify storage wishlist:args tmp set value "minecraft:overworld"
execute if score go wishlist.vars matches 0 \
        run data modify storage wishlist:args tag.dim set value 0

data modify storage wishlist:args tmp set from storage wishlist:args tag.dimension
execute store success score go wishlist.vars \
        run data modify storage wishlist:args tmp set value "minecraft:the_nether"
execute if score go wishlist.vars matches 0 \
        run data modify storage wishlist:args tag.dim set value 1

data modify storage wishlist:args tmp set from storage wishlist:args tag.dimension
execute store success score go wishlist.vars \
        run data modify storage wishlist:args tmp set value "minecraft:the_end"
execute if score go wishlist.vars matches 0 \
        run data modify storage wishlist:args tag.dim set value 2

data modify storage wishlist:args tmp set from storage wishlist:args tag.dimension
execute store success score go wishlist.vars \
        run data modify storage wishlist:args tmp set value "wishlist:sketch"
execute if score go wishlist.vars matches 0 \
        run data modify storage wishlist:args tag.dim set value 3

data modify storage wishlist:args tmp set from storage wishlist:args tag.dimension
execute store success score go wishlist.vars \
        run data modify storage wishlist:args tmp set value "wishlist:ocean"
execute if score go wishlist.vars matches 0 \
        run data modify storage wishlist:args tag.dim set value 4

data modify storage wishlist:args tmp set from storage wishlist:args tag.dimension
execute store success score go wishlist.vars \
        run data modify storage wishlist:args tmp set value "wishlist:sky"
execute if score go wishlist.vars matches 0 \
        run data modify storage wishlist:args tag.dim set value 5

data modify entity @s Item.tag set from storage wishlist:args tag

particle minecraft:electric_spark ~ ~ ~ 0.1 0.1 0.1 0.05 1

execute on origin run advancement grant @s only wishlist:tune_lodestone_skip
tag @a remove wishlist.match
