# lodestone_skip.mcfunction - teleport egg-breaker to encoded target

tag @a[distance=..2,tag=!wishlist.hidden] add mt_teleport_subject
execute if data storage wishlist:args tag.pos \
        run function magic_tools:impl/teleport
tag @a remove mt_teleport_subject
