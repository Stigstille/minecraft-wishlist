# pocket_sight.mcfunction - make villagers carrying stuff glow

execute as @e[distance=..32,type=minecraft:villager] \
        if data entity @s Inventory[0].Count \
        run effect give @s minecraft:glowing 10 1
