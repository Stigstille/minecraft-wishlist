# print_rep.mcfunction - print reputation scores for player

scoreboard players operation pos wishlist.vars = @s vg_positive
scoreboard players operation neg wishlist.vars = @s vg_negative

tellraw @s {"translate":"wishlist.magic_tools.villager_rep","with":[{"score":{"name":"pos","objective":"wishlist.vars"}},{"score":{"name":"neg","objective":"wishlist.vars"}}],"color":"dark_gray"}

scoreboard players operation pos wishlist.vars = @s ig_positive
scoreboard players operation neg wishlist.vars = @s ig_negative

tellraw @s {"translate":"wishlist.magic_tools.illager_rep","with":[{"score":{"name":"pos","objective":"wishlist.vars"}},{"score":{"name":"neg","objective":"wishlist.vars"}}],"color":"dark_gray"}

