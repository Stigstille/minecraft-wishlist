# rep_sight.mcfunction - print reputation scores for egg-breaker

execute as @a[distance=..2,sort=nearest,limit=1] \
        run function magic_tools:rep_sight/print_rep
