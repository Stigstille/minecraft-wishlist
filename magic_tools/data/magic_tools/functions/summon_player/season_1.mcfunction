# summon_player/season_1.mcfunction - bind egg to player

scoreboard players set go wishlist.vars 0
execute store success score go wishlist.vars \
        run data modify entity @s Item.tag.uuid set from entity @s Thrower
execute if score go wishlist.vars matches 1 \
        run particle minecraft:instant_effect ~ ~ ~ 0.1 0.1 0.1 0.005 10
execute if score go wishlist.vars matches 1 \
        run playsound minecraft:block.anvil.use player @a ~ ~ ~

