# summon_player/cast.mcfunction

data modify storage wishlist:args uuid set from storage wishlist:args tag.uuid
function wishlist:find_player_by_uuid
execute positioned ~-16 ~-8 ~-16 \
        if entity @e[type=marker,tag=wishlist.dungeon_room,dx=16,dy=16,dz=16] \
        run tag @s remove wishlist.match
execute as @a[tag=wishlist.match,tag=!wishlist.hidden,gamemode=!spectator] \
        run function magic_tools:impl/record_skip
tp @a[tag=wishlist.match,tag=!wishlist.hidden,gamemode=!spectator,limit=1] @s
tag @a remove wishlist.match
