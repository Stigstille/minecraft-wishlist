# backtrack.mcfunction - teleport to location

data modify storage wishlist:args tag set value {pos:[0.0d,0.0d,0.0d],dim:0}
execute store result storage wishlist:args tag.pos[0] double 0.0625 \
        run scoreboard players get @s mt_back_x
execute store result storage wishlist:args tag.pos[1] double 0.0625 \
        run scoreboard players get @s mt_back_y
execute store result storage wishlist:args tag.pos[2] double 0.0625 \
        run scoreboard players get @s mt_back_z
execute store result storage wishlist:args tag.dim double 1 \
        run scoreboard players get @s mt_back_d

tag @a[distance=..2,tag=!wishlist.hidden] add mt_teleport_subject
function magic_tools:impl/teleport
tag @a remove mt_teleport_subject
