# backtrack.mcfunction - teleport to location

data modify storage wishlist:args tag set value {pos:[0.0d,0.0d,0.0d],dim:0}
execute store result storage wishlist:args tag.pos[0] double 1 \
        run data get entity @s SpawnX
execute store result storage wishlist:args tag.pos[1] double 1 \
        run data get entity @s SpawnY
execute store result storage wishlist:args tag.pos[2] double 1 \
        run data get entity @s SpawnZ
execute if entity @s[nbt={SpawnDimension:"minecraft:overworld"}] \
        run data modify storage wishlist:args tag.dim set value 0
execute if entity @s[nbt={SpawnDimension:"minecraft:the_nether"}] \
        run data modify storage wishlist:args tag.dim set value 1
execute if entity @s[nbt={SpawnDimension:"minecraft:the_end"}] \
        run data modify storage wishlist:args tag.dim set value 2

tag @a[distance=..2,tag=!wishlist.hidden] add mt_teleport_subject
function magic_tools:impl/teleport
tag @a remove mt_teleport_subject

advancement grant @s only wishlist:use_home_skip
