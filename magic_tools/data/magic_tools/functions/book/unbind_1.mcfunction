# unbind.mcfunction - clear magic spells from book

summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:egg",Count:1b},Tags:[mt_just_made]}
data modify entity @e[tag=mt_just_made,limit=1] Item.tag set from entity @s Item.tag.tags[0]
tag @e remove mt_just_made
data remove entity @s Item.tag.tags[0]
particle minecraft:instant_effect ~ ~ ~ 0.1 0.1 0.1 0.005 10
playsound minecraft:block.grindstone.use player @a ~ ~ ~
execute unless data entity @s Item.tag.tags[0] \
        at @s \
        run function magic_tools:book/rewrite
execute if data entity @s Item.tag.tags[0] \
        run function magic_tools:book/unbind_1

