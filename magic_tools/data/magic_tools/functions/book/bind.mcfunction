# try_bind.mcfunction - copy spell from nearest magic item

tag @s add mt_self
data remove storage wishlist:args tags
data modify storage wishlist:args tags set from entity @s Item.tag.tags
execute as @e[type=minecraft:item,nbt=!{Item:{tag:{magic2:34}}},distance=..0.5,sort=nearest,limit=1] \
        if data entity @s Item.tag.magic2 \
        unless data entity @s Item.tag.noncopyable \
        run function magic_tools:book/bind_1
execute store result score go mt_ \
        run data modify entity @s Item.tag.tags set from storage wishlist:args tags
execute if score go mt_ matches 1 at @s run function magic_tools:book/rewrite
tag @s remove mt_self


