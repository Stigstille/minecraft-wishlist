# summon_gateway/season_1.mcfunction - bind egg to lodestone

# To determine dimension we copy the string Dimension of the nearest player to
# a score. This makes the score value the LENGTH of the dimension string;
# luckily the default dimensions have different lengths, but this is a horrible
# hack.

execute at @s align xyz run tp @s ~0.5 ~ ~0.5
data modify storage wishlist:args blockTags set value {ExactTeleport:1b,ExitPortal:{X:0,Y:0,Z:0}} 
execute store result storage wishlist:args blockTags.ExitPortal.X int 1 \
        run data get entity @s Pos[0]
execute store result storage wishlist:args blockTags.ExitPortal.Y int 1 \
        run data get entity @s Pos[1]
execute store result storage wishlist:args blockTags.ExitPortal.Z int 1 \
        run data get entity @s Pos[2]
data modify entity @s Item.tag.blockTags set from storage wishlist:args blockTags
particle minecraft:electric_spark ~ ~ ~ 0.1 0.1 0.1 0.05 1

