# summon_villager.mcfunction - Summon an NPC

summon minecraft:villager ~ ~ ~ {Tags:["wishlist.summon"]}
execute as @e[tag=wishlist.summon,distance=0,limit=1] \
        run function wishlist:skin_villager_by_biome
execute as @e[tag=wishlist.summon,distance=0,limit=1] \
        run function wishlist:recall
execute store result storage wishlist:args memories.employer int 1 \
        run scoreboard players get @s wishlist.playerId
execute as @e[tag=wishlist.summon,distance=0,limit=1] \
        run function wishlist:memorize
data modify entity @e[tag=wishlist.summon,limit=1] {} merge from storage wishlist:args tag.entityTag
tag @e remove wishlist.summon

