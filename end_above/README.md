End Above
=========

Teleports above the Overworld every player who falls to the void under The End.

Players keep their x and z coordinates when teleported to the overworld, but
their y coordinate is set to 480.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Overhead
--------

Updates every tick

* checks every player in the end to see if their y coordinate is less than
  zero.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Did it Hurt?: fall from the End into the Overworld's sky
