# tick.mcfunction - Check for players falling out of the End

schedule function end_above:tick 1t

execute as @a \
        at @s \
        in minecraft:the_end \
        if entity @a[distance=0] \
        if entity @s[y=-100,dy=100] \
        run function end_above:teleport
