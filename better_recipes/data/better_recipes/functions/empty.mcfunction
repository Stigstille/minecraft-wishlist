
advancement revoke @s only better_recipes:empty

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function better_recipes:empty_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"text":"Empty"}
execute unless score done wishlist.vars matches 0 \
        run item modify entity @s weapon.mainhand wishlist:consume_one
execute unless score done wishlist.vars matches 0 \
        run function better_recipes:give with storage wishlist:args item

