
advancement revoke @s only better_recipes:take_steamer

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s attack \
        run function better_recipes:take_steamer_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"text":"Unknown Error"}



