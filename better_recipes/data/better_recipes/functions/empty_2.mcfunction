
#empty_2
execute unless score @s wishlist.load matches 1.. run return fail
execute store result score now wishlist.vars run time query gametime
execute if score now wishlist.vars > @s wishlist.messTick \
        run data modify entity @s Tags set value ["mess"]
execute if score now wishlist.vars > @s wishlist.doneTick \
        run function better_recipes:empty_3
scoreboard players set @s wishlist.doneTick 2000000000
scoreboard players set @s wishlist.messTick 2000000000
data modify storage wishlist:args item set value {}
data modify storage wishlist:args item.id set from entity @s Tags[0]
scoreboard players remove @s wishlist.load 1
execute if score @s wishlist.load matches 0 run data modify entity @s Tags set value []
playsound minecraft:item.bundle.remove_one player @a ~ ~ ~
scoreboard players set done wishlist.vars 1

