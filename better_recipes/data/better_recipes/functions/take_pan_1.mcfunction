
execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run data modify entity @s item.tag.CustomModelData set value 271851
data modify entity @s {} merge from storage wishlist:custom_blocks cooktop.interaction
tag @s add wishlist.custom_block
loot spawn ~ ~0.5 ~ loot better_recipes:pan
scoreboard players set done wishlist.vars 1
