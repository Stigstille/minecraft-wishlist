
advancement revoke @s only better_recipes:cooktop
data modify storage wishlist:args args set from entity @s SelectedItem

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function better_recipes:cooktop_1 with storage wishlist:args args.tag.wishlist.cookware

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"text":"[cooktop] Unknown Error"}
execute unless score done wishlist.vars matches 0 \
        run item modify entity @s weapon.mainhand wishlist:consume_one


