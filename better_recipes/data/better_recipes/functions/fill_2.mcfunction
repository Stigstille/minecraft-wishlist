
#fill_2
scoreboard players add @s wishlist.load 1
execute on vehicle if entity @s[tag=wishlist.oven] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.pan] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.pot] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.steamer] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.rack] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.press] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.still] \
        run scoreboard players set ttl wishlist.vars 200
execute on vehicle if entity @s[tag=wishlist.cask] \
        run scoreboard players set ttl wishlist.vars 1000
execute store result score @s wishlist.doneTick run time query gametime
scoreboard players operation @s wishlist.doneTick += ttl wishlist.vars
scoreboard players operation @s wishlist.messTick = @s wishlist.doneTick
scoreboard players operation @s wishlist.messTick += ttl wishlist.vars
data modify entity @s Tags append from storage wishlist:args item.tag.wishlist.recipe.id
execute if data entity @s Tags[1] run data modify entity @s Tags set value ["mess"]
playsound minecraft:item.bundle.insert player @a ~ ~ ~
scoreboard players set done wishlist.vars 1
