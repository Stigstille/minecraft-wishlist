

execute unless score @s wishlist.load matches 1.. run return fail
execute store result score now wishlist.vars run time query gametime
execute if score now wishlist.vars < @s wishlist.doneTick \
        run return run particle minecraft:smoke ~ ~0.8 ~ 0.1 0.1 0.1 0 5
execute if score now wishlist.vars < @s wishlist.messTick \
        run return run particle minecraft:poof ~ ~0.8 ~ 0.1 0.1 0.1 0 10
particle minecraft:large_smoke ~ ~0.8 ~ 0.1 0.1 0.1 0 5
