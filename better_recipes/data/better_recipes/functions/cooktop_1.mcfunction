
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
$data modify entity @s {} merge from storage wishlist:cooktop_variants $(id).interaction
$execute on passengers run data modify entity @s {} merge from storage wishlist:cooktop_variants $(id).display
scoreboard players set done wishlist.vars 1
