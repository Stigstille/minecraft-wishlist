
#empty_3
data modify storage wishlist:args args set value {}
execute on vehicle if entity @s[tag=wishlist.oven] \
        run data modify storage wishlist:args args.where set value "oven"
execute on vehicle if entity @s[tag=wishlist.pan] \
        run data modify storage wishlist:args args.where set value "pan"
execute on vehicle if entity @s[tag=wishlist.pot] \
        run data modify storage wishlist:args args.where set value "pot"
execute on vehicle if entity @s[tag=wishlist.steamer] \
        run data modify storage wishlist:args args.where set value "steamer"
execute on vehicle if entity @s[tag=wishlist.rack] \
        run data modify storage wishlist:args args.where set value "rack"
execute on vehicle if entity @s[tag=wishlist.press] \
        run data modify storage wishlist:args args.where set value "press"
execute on vehicle if entity @s[tag=wishlist.still] \
        run data modify storage wishlist:args args.where set value "still"
execute on vehicle if entity @s[tag=wishlist.cask] \
        run data modify storage wishlist:args args.where set value "cask"
data modify storage wishlist:args args.what set from entity @s Tags[0]
function better_recipes:empty_4 with storage wishlist:args args
