
#push_item_2
scoreboard players add @s wishlist.load 1
execute store result score @s wishlist.doneTick run time query gametime
scoreboard players add @s wishlist.doneTick 200
scoreboard players operation @s wishlist.messTick = @s wishlist.doneTick
scoreboard players add @s wishlist.messTick 200
data modify entity @s Tags append string storage wishlist:args item.id 10
execute if data entity @s Tags[1] run data modify entity @s Tags set value ["mess"]
scoreboard players set done wishlist.vars 1
