
scoreboard objectives add wishlist.load dummy
scoreboard objectives add wishlist.doneTick dummy
scoreboard objectives add wishlist.messTick dummy

data modify storage wishlist:custom_blocks oven set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.oven], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271850, \
                      display:{Name:'{"text":"Oven","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

data modify storage wishlist:custom_blocks cooktop set value { \
        interaction: { \
                Tags: [wishlist.cooktop], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271851, \
                      display:{Name:'{"text":"Cooktop","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

data modify storage wishlist:custom_blocks press set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.press], \
                width: 0.5, \
                height: 0.5 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271852, \
                      display:{Name:'{"text":"Press","italic":false}'} \
              } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks rack set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.rack], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271853, \
                      display:{Name:'{"text":"Rack","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

data modify storage wishlist:custom_blocks sink set value { \
        interaction: { \
                Tags: [wishlist.sink], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271856, \
                      display:{Name:'{"text":"Sink","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

data modify storage wishlist:custom_blocks trash set value { \
        interaction: { \
                Tags: [wishlist.trash], \
                width: 0.75, \
                height: 0.875 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271857, \
                      display:{Name:'{"text":"Trash","italic":false}'} \
              } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks cask set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.cask], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271859, \
                      display:{Name:'{"text":"Cask","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

data modify storage wishlist:custom_blocks still set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.still], \
                width: 0.875, \
                height: 0.875 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271849, \
                      display:{Name:'{"text":"Still","italic":false}'} \
              } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:cooktop_variants pan set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.pan], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                item: { tag: { CustomModelData:271854 } } \
        } \
}

data modify storage wishlist:cooktop_variants pot set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.pot], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                item: { tag: { CustomModelData:271855 } } \
        } \
}

data modify storage wishlist:cooktop_variants steamer set value { \
        interaction: { \
                Tags: [wishlist.kitchen,wishlist.steamer], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                item: { tag: { CustomModelData:271858 } } \
        } \
}

function better_recipes:transitions
function better_recipes:results
function better_recipes:tick_1s
