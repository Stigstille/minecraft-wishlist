Better Recipes Result List
==========================

### ![628336] Mess
### ![628325] Raw Fish Broth
* Craft minecraft:bowl,#better_recipes:fish,minecraft:water_bucket
### ![628325] Raw Meat Broth
* Craft minecraft:bowl,#better_recipes:meat,minecraft:water_bucket
### ![628325] Raw Vegetable Broth
* Craft minecraft:bowl,#better_recipes:vegetables,minecraft:water_bucket
### ![628319] Raw Fish Stew
* Craft minecraft:bowl,#better_recipes:vegetables,#better_recipes:fish,minecraft:water_bucket
### ![628321] Raw Meat Stew
* Craft minecraft:bowl,#better_recipes:vegetables,#better_recipes:meat,minecraft:water_bucket
### ![628319] Raw Vegetable Stew
* Craft minecraft:bowl,#better_recipes:vegetables,#better_recipes:vegetables,minecraft:water_bucket
### ![628325] Raw Custard
* Craft minecraft:bowl,minecraft:egg,minecraft:sugar,minecraft:milk_bucket
### ![628319] Raw Cookies
* Craft minecraft:bowl,minecraft:wheat,minecraft:sugar,minecraft:egg
### ![628333] Raw Meat Patty
* Craft minecraft:bowl,#better_recipes:meat
### ![628321] Raw Meatballs
* Craft minecraft:bowl,#better_recipes:meat,minecraft:egg,minecraft:allium
### ![628325] Dough
* Craft minecraft:bowl,minecraft:wheat,minecraft:wheat,minecraft:water_bucket
### ![628331] Risen Dough
* Rest ![628325] [Dough](#dough)
### ![628331] Boiled Dough
* Boil ![628331] [Risen Dough](#risen-dough)
### ![628325] Milk Dough
* Craft minecraft:bowl,minecraft:wheat,minecraft:wheat,minecraft:milk_bucket
### ![628331] Risen Milk Dough
* Rest ![628325] [Milk Dough](#milk-dough)
### ![628331] Raw Noodles
* Craft minecraft:bowl,minecraft:wheat,minecraft:egg,minecraft:water_bucket
### ![628331] Cooked Noodles
* Boil ![628331] [Raw Noodles](#raw-noodles)
### ![628325] Mash
* Craft minecraft:bowl,minecraft:wheat,minecraft:water_bucket
### ![628329] Wort
* Boil ![628325] [Mash](#mash)
### ![628329] Beer
* Age ![628329] [Wort](#wort)
### ![628329] Whisky
* Distill ![628329] [Beer](#beer)
### ![628325] Fish Broth
* Boil ![628325] [Raw Fish Broth](#raw-fish-broth)
### ![628325] Meat Broth
* Boil ![628325] [Raw Meat Broth](#raw-meat-broth)
### ![628325] Vegetable Broth
* Boil ![628325] [Raw Vegetable Broth](#raw-vegetable-broth)
### ![628320] Fish Stew
* Boil ![628319] [Raw Fish Stew](#raw-fish-stew)
### ![628322] Meat Stew
* Boil ![628321] [Raw Meat Stew](#raw-meat-stew)
### ![628320] Vegetable Stew
* Boil ![628319] [Raw Vegetable Stew](#raw-vegetable-stew)
### ![628325] Custard
* Boil ![628325] [Raw Custard](#raw-custard)
### ![628322] Cookies
* Bake ![628319] [Raw Cookies](#raw-cookies)
### ![628334] Meat Patty
* Grill ![628333] [Raw Meat Patty](#raw-meat-patty)
### ![628322] Meatballs
* Grill ![628321] [Raw Meatballs](#raw-meatballs)
### ![628331] Hardtack
* Bake ![628325] [Dough](#dough)
### ![628341] Baguette
* Bake ![628331] [Risen Dough](#risen-dough)
### ![628342] Bagel
* Bake ![628331] [Boiled Dough](#boiled-dough)
### ![628347] Mantou
* Steam ![628331] [Risen Dough](#risen-dough)
### ![628343] Flatbread
* Grill ![628325] [Dough](#dough)
### ![628334] Milk Bread
* Bake ![628331] [Risen Milk Dough](#risen-milk-dough)
### ![628331] Potato
### ![628332] Carrot
### ![628333] Beetroot
### ![628322] Brown Mushroom
### ![628321] Red Mushroom
### ![628333] Raw Porkchop
### ![628333] Raw Beef
### ![628333] Raw Mutton
### ![628331] Raw Chicken
### ![628331] Raw Rabbit
### ![628331] Raw Cod
### ![628333] Raw Salmon
### ![628331] Baked Potato
* Bake ![628331] [Potato](#potato)
* Grill ![628331] [Potato](#potato)
* Boil ![628331] [Potato](#potato)
* Steam ![628331] [Potato](#potato)
### ![628334] Cooked Porkchop
* Bake ![628333] [Raw Porkchop](#raw-porkchop)
* Grill ![628333] [Raw Porkchop](#raw-porkchop)
* Boil ![628333] [Raw Porkchop](#raw-porkchop)
* Steam ![628333] [Raw Porkchop](#raw-porkchop)
### ![628334] Cooked Beef
* Bake ![628333] [Raw Beef](#raw-beef)
* Grill ![628333] [Raw Beef](#raw-beef)
* Boil ![628333] [Raw Beef](#raw-beef)
* Steam ![628333] [Raw Beef](#raw-beef)
### ![628334] Cooked Mutton
* Bake ![628333] [Raw Mutton](#raw-mutton)
* Grill ![628333] [Raw Mutton](#raw-mutton)
* Boil ![628333] [Raw Mutton](#raw-mutton)
* Steam ![628333] [Raw Mutton](#raw-mutton)
### ![628332] Cooked Chicken
* Bake ![628331] [Raw Chicken](#raw-chicken)
* Grill ![628331] [Raw Chicken](#raw-chicken)
* Boil ![628331] [Raw Chicken](#raw-chicken)
* Steam ![628331] [Raw Chicken](#raw-chicken)
### ![628332] Cooked Rabbit
* Bake ![628331] [Raw Rabbit](#raw-rabbit)
* Grill ![628331] [Raw Rabbit](#raw-rabbit)
* Boil ![628331] [Raw Rabbit](#raw-rabbit)
* Steam ![628331] [Raw Rabbit](#raw-rabbit)
### ![628331] Cooked Cod
* Bake ![628331] [Raw Cod](#raw-cod)
* Grill ![628331] [Raw Cod](#raw-cod)
* Boil ![628331] [Raw Cod](#raw-cod)
* Steam ![628331] [Raw Cod](#raw-cod)
### ![628333] Cooked Salmon
* Bake ![628333] [Raw Salmon](#raw-salmon)
* Grill ![628333] [Raw Salmon](#raw-salmon)
* Boil ![628333] [Raw Salmon](#raw-salmon)
* Steam ![628333] [Raw Salmon](#raw-salmon)
### ![628333] Apple
### ![628333] Melon Slice
### ![628332] Pumpkin
### ![628321] Sweet Berries
### ![628320] Glow Berries
### ![628331] Wheat Seeds
### ![628335] Melon Seeds
### ![628331] Pumpkin Seeds
### ![628331] Beetroot Seeds
### ![628331] Allium
### ![628319] Azure Bluet
### ![628319] Blue Orchid
### ![628319] Cornflower
### ![628319] Dandelion
### ![628318] Lily of the Valley
### ![628319] Oxeye Daisy
### ![628321] Poppy
### ![628320] Orange Tulip
### ![628321] Pink Tulip
### ![628321] Red Tulip
### ![628318] White Tulip
### ![628319] Lilac
### ![628319] Peony
### ![628321] Rose
### ![628319] Sunflower
### ![628331] Dried Azure Bluet
* Rest ![628319] [Azure Bluet](#azure-bluet)
### ![628331] Dried Blue Orchid
* Rest ![628319] [Blue Orchid](#blue-orchid)
### ![628331] Dried Cornflower
* Rest ![628319] [Cornflower](#cornflower)
### ![628331] Dried Dandelion
* Rest ![628319] [Dandelion](#dandelion)
### ![628330] Dried Lily of the Valley
* Rest ![628318] [Lily of the Valley](#lily-of-the-valley)
### ![628331] Dried Chamomile
* Rest ![628319] [Oxeye Daisy](#oxeye-daisy)
### ![628333] Dried Poppy
* Rest ![628321] [Poppy](#poppy)
### ![628332] Dried Orange Tulip
* Rest ![628320] [Orange Tulip](#orange-tulip)
### ![628333] Dried Pink Tulip
* Rest ![628321] [Pink Tulip](#pink-tulip)
### ![628333] Dried Red Tulip
* Rest ![628321] [Red Tulip](#red-tulip)
### ![628330] Dried White Tulip
* Rest ![628318] [White Tulip](#white-tulip)
### ![628331] Dried Lilac
* Rest ![628319] [Lilac](#lilac)
### ![628331] Dried Peony
* Rest ![628319] [Peony](#peony)
### ![628333] Dried Rose
* Rest ![628321] [Rose](#rose)
### ![628331] Dried Sunflower
* Rest ![628319] [Sunflower](#sunflower)
### ![628325] Azure Bluet Tea
* Boil ![628331] [Dried Azure Bluet](#dried-azure-bluet)
### ![628325] Blue Orchid Tea
* Boil ![628331] [Dried Blue Orchid](#dried-blue-orchid)
### ![628325] Cornflower Tea
* Boil ![628331] [Dried Cornflower](#dried-cornflower)
### ![628325] Dandelion Tea
* Boil ![628331] [Dried Dandelion](#dried-dandelion)
### ![628324] Lily of the Valley Tea
* Boil ![628330] [Dried Lily of the Valley](#dried-lily-of-the-valley)
### ![628325] Chamomile Tea
* Boil ![628331] [Dried Chamomile](#dried-chamomile)
### ![628327] Poppy Tea
* Boil ![628333] [Dried Poppy](#dried-poppy)
### ![628326] Orange Tulip Tea
* Boil ![628332] [Dried Orange Tulip](#dried-orange-tulip)
### ![628327] Pink Tulip Tea
* Boil ![628333] [Dried Pink Tulip](#dried-pink-tulip)
### ![628327] Red Tulip Tea
* Boil ![628333] [Dried Red Tulip](#dried-red-tulip)
### ![628324] White Tulip Tea
* Boil ![628330] [Dried White Tulip](#dried-white-tulip)
### ![628325] Lilac Tea
* Boil ![628331] [Dried Lilac](#dried-lilac)
### ![628325] Peony Tea
* Boil ![628331] [Dried Peony](#dried-peony)
### ![628327] Rose Tea
* Boil ![628333] [Dried Rose](#dried-rose)
### ![628325] Sunflower Tea
* Boil ![628331] [Dried Sunflower](#dried-sunflower)
### ![628325] Potato Juice
* Press ![628331] [Potato](#potato)
### ![628326] Carrot Juice
* Press ![628332] [Carrot](#carrot)
### ![628327] Beetroot Juice
* Press ![628333] [Beetroot](#beetroot)
### ![628325] Apple Juice
* Press ![628333] [Apple](#apple)
### ![628327] Melon Juice
* Press ![628333] [Melon Slice](#melon-slice)
### ![628326] Pumpkin Juice
* Press ![628332] [Pumpkin](#pumpkin)
### ![628327] Sweet Berry Juice
* Press ![628321] [Sweet Berries](#sweet-berries)
### ![628326] Glow Berry Juice
* Press ![628320] [Glow Berries](#glow-berries)
### ![628325] Potato Wine
* Age ![628325] [Potato Juice](#potato-juice)
### ![628326] Carrot Wine
* Age ![628326] [Carrot Juice](#carrot-juice)
### ![628327] Beetroot Wine
* Age ![628327] [Beetroot Juice](#beetroot-juice)
### ![628325] Apple Wine
* Age ![628325] [Apple Juice](#apple-juice)
### ![628327] Melon Wine
* Age ![628327] [Melon Juice](#melon-juice)
### ![628326] Pumpkin Wine
* Age ![628326] [Pumpkin Juice](#pumpkin-juice)
### ![628327] Sweet Berry Wine
* Age ![628327] [Sweet Berry Juice](#sweet-berry-juice)
### ![628326] Glow Berry Wine
* Age ![628326] [Glow Berry Juice](#glow-berry-juice)
### ![628325] Potato Liquor
* Distill ![628325] [Potato Wine](#potato-wine)
### ![628326] Carrot Liquor
* Distill ![628326] [Carrot Wine](#carrot-wine)
### ![628327] Beetroot Liquor
* Distill ![628327] [Beetroot Wine](#beetroot-wine)
### ![628325] Apple Liquor
* Distill ![628325] [Apple Wine](#apple-wine)
### ![628327] Melon Liquor
* Distill ![628327] [Melon Wine](#melon-wine)
### ![628326] Pumpkin Liquor
* Distill ![628326] [Pumpkin Wine](#pumpkin-wine)
### ![628327] Sweet Berry Liquor
* Distill ![628327] [Sweet Berry Wine](#sweet-berry-wine)
### ![628326] Glow Berry Liquor
* Distill ![628326] [Glow Berry Wine](#glow-berry-wine)
### ![628324] Curdled Milk
* Craft minecraft:bowl,minecraft:milk_bucket,minecraft:chorus_fruit
### ![628319] Cheese Curds
* Boil ![628324] [Curdled Milk](#curdled-milk)
### ![628331] Cheese
* Age ![628319] [Cheese Curds](#cheese-curds)
### ![628332] Aged Cheese
* Age ![628331] [Cheese](#cheese)
### ![628331] Raw Pizza
* Craft ![628331] [Risen Dough](#risen-dough),![628331] [Cheese](#cheese)
### ![628339] Cooked Pizza
* Bake ![628331] [Raw Pizza](#raw-pizza)
### ![628337] Burger
* Craft ![628334] [Milk Bread](#milk-bread),![628334] [Meat Patty](#meat-patty)
### ![271835] Fish Ramen
* Craft ![628325] [Fish Broth](#fish-broth),![628331] [Cooked Noodles](#cooked-noodles)
### ![271835] Meat Ramen
* Craft ![628325] [Meat Broth](#meat-broth),![628331] [Cooked Noodles](#cooked-noodles)
### ![271835] Vegetable Ramen
* Craft ![628325] [Vegetable Broth](#vegetable-broth),![628331] [Cooked Noodles](#cooked-noodles)
### ![628323] Prunes
* Age ![628321] [Sweet Berries](#sweet-berries)
* Age ![628320] [Glow Berries](#glow-berries)

[628318]: resources/assets/wishlist/textures/item/bowl_of_white_chunks.png ""
[628319]: resources/assets/wishlist/textures/item/bowl_of_pale_chunks.png ""
[628320]: resources/assets/wishlist/textures/item/bowl_of_orange_chunks.png ""
[628321]: resources/assets/wishlist/textures/item/bowl_of_red_chunks.png ""
[628322]: resources/assets/wishlist/textures/item/bowl_of_brown_chunks.png ""
[628323]: resources/assets/wishlist/textures/item/bowl_of_black_chunks.png ""
[628324]: resources/assets/wishlist/textures/item/bowl_of_white_liquid.png ""
[628325]: resources/assets/wishlist/textures/item/bowl_of_pale_liquid.png ""
[628326]: resources/assets/wishlist/textures/item/bowl_of_orange_liquid.png ""
[628327]: resources/assets/wishlist/textures/item/bowl_of_red_liquid.png ""
[628328]: resources/assets/wishlist/textures/item/bowl_of_brown_liquid.png ""
[628329]: resources/assets/wishlist/textures/item/bowl_of_black_liquid.png ""
[628330]: resources/assets/wishlist/textures/item/bowl_of_white_stuff.png ""
[628331]: resources/assets/wishlist/textures/item/bowl_of_pale_stuff.png ""
[628332]: resources/assets/wishlist/textures/item/bowl_of_orange_stuff.png ""
[628333]: resources/assets/wishlist/textures/item/bowl_of_red_stuff.png ""
[628334]: resources/assets/wishlist/textures/item/bowl_of_brown_stuff.png ""
[628335]: resources/assets/wishlist/textures/item/bowl_of_black_stuff.png ""
[628336]: resources/assets/wishlist/textures/item/bowl_of_mess.png ""
[628336]: resources/assets/wishlist/textures/item/bowl_of_mess.png ""
[628337]: resources/assets/wishlist/textures/item/burger.png ""
[628338]: resources/assets/wishlist/textures/item/cheeseburger.png ""
[628339]: resources/assets/wishlist/textures/item/pizza_slice.png ""
[628340]: resources/assets/wishlist/textures/item/waffle.png ""
[628341]: resources/assets/grabeltone/textures/item/baguette.png ""
[628342]: resources/assets/grabeltone/textures/item/bagel.png ""
[628343]: resources/assets/grabeltone/textures/item/flatbread.png ""
[628344]: resources/assets/grabeltone/textures/item/rupjmaize.png ""
[628345]: resources/assets/grabeltone/textures/item/pretzel.png ""
[628346]: resources/assets/grabeltone/textures/item/hardtack.png ""
[628347]: resources/assets/grabeltone/textures/item/mantou.png ""
[628348]: resources/assets/grabeltone/textures/item/pandesal.png ""
[628349]: resources/assets/grabeltone/textures/item/cornbread.png ""
[271835]: resources/assets/grabeltone/textures/item/noodles.png ""
