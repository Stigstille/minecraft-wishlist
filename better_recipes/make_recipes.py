
import json
import re

# Custom model data for specific items

bowl_of_white_chunks = 628318
bowl_of_pale_chunks = 628319
bowl_of_orange_chunks = 628320
bowl_of_red_chunks = 628321
bowl_of_brown_chunks = 628322
bowl_of_black_chunks = 628323
bowl_of_white_liquid = 628324
bowl_of_pale_liquid = 628325
bowl_of_orange_liquid = 628326
bowl_of_red_liquid = 628327
bowl_of_brown_liquid = 628328
bowl_of_black_liquid = 628329
bowl_of_white_stuff = 628330
bowl_of_pale_stuff = 628331
bowl_of_orange_stuff = 628332
bowl_of_red_stuff = 628333
bowl_of_brown_stuff = 628334
bowl_of_black_stuff = 628335

# All possible custom food items

results = {
    "mess": [ "Mess",628336 ],
    "raw_fish_broth": [ "Raw Fish Broth",bowl_of_pale_liquid ],
    "raw_meat_broth": [ "Raw Meat Broth",bowl_of_pale_liquid ],
    "raw_vegetable_broth": [ "Raw Vegetable Broth",bowl_of_pale_liquid ],
    "raw_fish_stew": [ "Raw Fish Stew",bowl_of_pale_chunks ],
    "raw_meat_stew": [ "Raw Meat Stew",bowl_of_red_chunks ],
    "raw_vegetable_stew": [ "Raw Vegetable Stew",bowl_of_pale_chunks ],
    "raw_custard": [ "Raw Custard",bowl_of_pale_liquid ],
    "raw_cookies": [ "Raw Cookies",bowl_of_pale_chunks ],
    "raw_meat_patty": [ "Raw Meat Patty",bowl_of_red_stuff ],
    "raw_meatballs": [ "Raw Meatballs",bowl_of_red_chunks ],
    "dough": [ "Dough",bowl_of_pale_liquid ],
    "risen_dough": [ "Risen Dough",bowl_of_pale_stuff ],
    "boiled_dough": [ "Boiled Dough",bowl_of_pale_stuff ],
    "milk_dough": [ "Milk Dough",bowl_of_pale_liquid ],
    "risen_milk_dough": [ "Risen Milk Dough",bowl_of_pale_stuff ],
    "noodles": [  "Raw Noodles",bowl_of_pale_stuff ],
    "cooked_noodles": [  "Cooked Noodles",bowl_of_pale_stuff ],
    "mash": [ "Mash",bowl_of_pale_liquid ],
    "wort": [ "Wort",bowl_of_black_liquid ],
    "beer": [ "Beer",bowl_of_black_liquid ],
    "whisky": [ "Whisky",bowl_of_black_liquid ],
    "fish_broth": [ "Fish Broth",bowl_of_pale_liquid ],
    "meat_broth": [ "Meat Broth",bowl_of_pale_liquid ],
    "vegetable_broth": [ "Vegetable Broth",bowl_of_pale_liquid ],
    "fish_stew": [ "Fish Stew",bowl_of_orange_chunks ],
    "meat_stew": [ "Meat Stew",bowl_of_brown_chunks ],
    "vegetable_stew": [ "Vegetable Stew",bowl_of_orange_chunks ],
    "custard": [ "Custard",bowl_of_pale_liquid ],
    "cookies": [ "Cookies",bowl_of_brown_chunks ],
    "meat_patty": [ "Meat Patty",bowl_of_brown_stuff ],
    "meatballs": [ "Meatballs",bowl_of_brown_chunks ],
    "hardtack": [ "Hardtack",628331 ],
    "baguette": [ "Baguette",628341 ],
    "bagel": [ "Bagel",628342 ],
    "mantou": [ "Mantou",628347 ],
    "flatbread": [ "Flatbread",628343 ],
    "milk_bread": [ "Milk Bread",bowl_of_brown_stuff ],
    "potato": [ "Potato",bowl_of_pale_stuff ],
    "carrot": [ "Carrot",bowl_of_orange_stuff ],
    "beetroot": [ "Beetroot",bowl_of_red_stuff ],
    "brown_mushroom": [ "Brown Mushroom",bowl_of_brown_chunks ],
    "red_mushroom": [ "Red Mushroom",bowl_of_red_chunks ],
    "porkchop": [ "Raw Porkchop",bowl_of_red_stuff ],
    "beef": [ "Raw Beef",bowl_of_red_stuff ],
    "mutton": [ "Raw Mutton",bowl_of_red_stuff ],
    "chicken": [ "Raw Chicken",bowl_of_pale_stuff ],
    "rabbit": [ "Raw Rabbit",bowl_of_pale_stuff ],
    "cod": [ "Raw Cod",bowl_of_pale_stuff ],
    "salmon": [ "Raw Salmon",bowl_of_red_stuff ],
    "baked_potato": [ "Baked Potato",bowl_of_pale_stuff ],
    "cooked_porkchop": [ "Cooked Porkchop",bowl_of_brown_stuff ],
    "cooked_beef": [ "Cooked Beef",bowl_of_brown_stuff ],
    "cooked_mutton": [ "Cooked Mutton",bowl_of_brown_stuff ],
    "cooked_chicken": [ "Cooked Chicken",bowl_of_orange_stuff ],
    "cooked_rabbit": [ "Cooked Rabbit",bowl_of_orange_stuff ],
    "cooked_cod": [ "Cooked Cod",bowl_of_pale_stuff ],
    "cooked_salmon": [ "Cooked Salmon",bowl_of_red_stuff ],
    "apple": [ "Apple",bowl_of_red_stuff ],
    "melon_slice": [ "Melon Slice",bowl_of_red_stuff ],
    "pumpkin": [ "Pumpkin",bowl_of_orange_stuff ],
    "sweet_berries": [ "Sweet Berries",bowl_of_red_chunks ],
    "glow_berries": [ "Glow Berries",bowl_of_orange_chunks ],
    "wheat_seeds": [ "Wheat Seeds",bowl_of_pale_stuff ],
    "melon_seeds": [ "Melon Seeds",bowl_of_black_stuff ],
    "pumpkin_seeds": [ "Pumpkin Seeds",bowl_of_pale_stuff ],
    "beetroot_seeds": [ "Beetroot Seeds",bowl_of_pale_stuff ],
    "allium": [ "Allium",bowl_of_pale_stuff ],
    "azure_bluet": [ "Azure Bluet",bowl_of_pale_chunks ],
    "blue_orchid": [ "Blue Orchid",bowl_of_pale_chunks ],
    "cornflower": [ "Cornflower",bowl_of_pale_chunks ],
    "dandelion": [ "Dandelion",bowl_of_pale_chunks ],
    "lily_of_the_valley": [ "Lily of the Valley",bowl_of_white_chunks ],
    "oxeye_daisy": [ "Oxeye Daisy",bowl_of_pale_chunks ],
    "poppy": [ "Poppy",bowl_of_red_chunks ],
    "orange_tulip": [ "Orange Tulip",bowl_of_orange_chunks ],
    "pink_tulip": [ "Pink Tulip",bowl_of_red_chunks ],
    "red_tulip": [ "Red Tulip",bowl_of_red_chunks ],
    "white_tulip": [ "White Tulip",bowl_of_white_chunks ],
    "lilac": [ "Lilac",bowl_of_pale_chunks ],
    "peony": [ "Peony",bowl_of_pale_chunks ],
    "rose_bush": [ "Rose",bowl_of_red_chunks ],
    "sunflower": [ "Sunflower",bowl_of_pale_chunks ],
    "dried_azure_bluet": [ "Dried Azure Bluet",bowl_of_pale_stuff ],
    "dried_blue_orchid": [ "Dried Blue Orchid",bowl_of_pale_stuff ],
    "dried_cornflower": [ "Dried Cornflower",bowl_of_pale_stuff ],
    "dried_dandelion": [ "Dried Dandelion",bowl_of_pale_stuff ],
    "dried_lily_of_the_valley": [ "Dried Lily of the Valley",bowl_of_white_stuff ],
    "dried_oxeye_daisy": [ "Dried Chamomile",bowl_of_pale_stuff ],
    "dried_poppy": [ "Dried Poppy",bowl_of_red_stuff ],
    "dried_orange_tulip": [ "Dried Orange Tulip",bowl_of_orange_stuff ],
    "dried_pink_tulip": [ "Dried Pink Tulip",bowl_of_red_stuff ],
    "dried_red_tulip": [ "Dried Red Tulip",bowl_of_red_stuff ],
    "dried_white_tulip": [ "Dried White Tulip",bowl_of_white_stuff ],
    "dried_lilac": [ "Dried Lilac",bowl_of_pale_stuff ],
    "dried_peony": [ "Dried Peony",bowl_of_pale_stuff ],
    "dried_rose_bush": [ "Dried Rose",bowl_of_red_stuff ],
    "dried_sunflower": [ "Dried Sunflower",bowl_of_pale_stuff ],
    "azure_bluet_tea": [ "Azure Bluet Tea",bowl_of_pale_liquid ],
    "blue_orchid_tea": [ "Blue Orchid Tea",bowl_of_pale_liquid ],
    "cornflower_tea": [ "Cornflower Tea",bowl_of_pale_liquid ],
    "dandelion_tea": [ "Dandelion Tea",bowl_of_pale_liquid ],
    "lily_of_the_valley_tea": [ "Lily of the Valley Tea",bowl_of_white_liquid ],
    "oxeye_daisy_tea": [ "Chamomile Tea",bowl_of_pale_liquid ],
    "poppy_tea": [ "Poppy Tea",bowl_of_red_liquid ],
    "orange_tulip_tea": [ "Orange Tulip Tea",bowl_of_orange_liquid ],
    "pink_tulip_tea": [ "Pink Tulip Tea",bowl_of_red_liquid ],
    "red_tulip_tea": [ "Red Tulip Tea",bowl_of_red_liquid ],
    "white_tulip_tea": [ "White Tulip Tea",bowl_of_white_liquid ],
    "lilac_tea": [ "Lilac Tea",bowl_of_pale_liquid ],
    "peony_tea": [ "Peony Tea",bowl_of_pale_liquid ],
    "rose_bush_tea": [ "Rose Tea",bowl_of_red_liquid ],
    "sunflower_tea": [ "Sunflower Tea",bowl_of_pale_liquid ],
    "potato_juice": [ "Potato Juice",bowl_of_pale_liquid ],
    "carrot_juice": [ "Carrot Juice",bowl_of_orange_liquid ],
    "beetroot_juice": [ "Beetroot Juice",bowl_of_red_liquid ],
    "apple_juice": [ "Apple Juice",bowl_of_pale_liquid ],
    "melon_juice": [ "Melon Juice",bowl_of_red_liquid ],
    "pumpkin_juice": [ "Pumpkin Juice",bowl_of_orange_liquid ],
    "sweet_berry_juice": [ "Sweet Berry Juice",bowl_of_red_liquid ],
    "glow_berry_juice": [ "Glow Berry Juice",bowl_of_orange_liquid ],
    "potato_wine": [ "Potato Wine",bowl_of_pale_liquid ],
    "carrot_wine": [ "Carrot Wine",bowl_of_orange_liquid ],
    "beetroot_wine": [ "Beetroot Wine",bowl_of_red_liquid ],
    "apple_wine": [ "Apple Wine",bowl_of_pale_liquid ],
    "melon_wine": [ "Melon Wine",bowl_of_red_liquid ],
    "pumpkin_wine": [ "Pumpkin Wine",bowl_of_orange_liquid ],
    "sweet_berry_wine": [ "Sweet Berry Wine",bowl_of_red_liquid ],
    "glow_berry_wine": [ "Glow Berry Wine",bowl_of_orange_liquid ],
    "potato_liquor": [ "Potato Liquor",bowl_of_pale_liquid ],
    "carrot_liquor": [ "Carrot Liquor",bowl_of_orange_liquid ],
    "beetroot_liquor": [ "Beetroot Liquor",bowl_of_red_liquid ],
    "apple_liquor": [ "Apple Liquor",bowl_of_pale_liquid ],
    "melon_liquor": [ "Melon Liquor",bowl_of_red_liquid ],
    "pumpkin_liquor": [ "Pumpkin Liquor",bowl_of_orange_liquid ],
    "sweet_berry_liquor": [ "Sweet Berry Liquor",bowl_of_red_liquid ],
    "glow_berry_liquor": [ "Glow Berry Liquor",bowl_of_orange_liquid ],
    "curdled_milk": [ "Curdled Milk",bowl_of_white_liquid ],
    "cheese_curds": [ "Cheese Curds",bowl_of_pale_chunks ],
    "cheese": [ "Cheese",bowl_of_pale_stuff ],
    "aged_cheese": [ "Aged Cheese",bowl_of_orange_stuff ],
    "pizza": [ "Raw Pizza",bowl_of_pale_stuff ],
    "cooked_pizza": [ "Cooked Pizza",628339 ],
    "burger": [ "Burger",628337 ],
    "fish_ramen": [ "Fish Ramen",271835 ],
    "meat_ramen": [ "Meat Ramen",271835 ],
    "vegetable_ramen": [ "Vegetable Ramen",271835 ],
    "prunes": [ "Prunes",bowl_of_black_chunks ],
}

# Data to generate all vanilla recipes

recipes = {
        "raw_fish_broth": [
            "minecraft:bowl",
            "#better_recipes:fish",
            "minecraft:water_bucket"
            ],
        "raw_meat_broth": [
            "minecraft:bowl",
            "#better_recipes:meat",
            "minecraft:water_bucket"
            ],
        "raw_vegetable_broth": [
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "minecraft:water_bucket"
            ],
        "raw_fish_stew":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:fish",
            "minecraft:water_bucket"
            ],
        "raw_meat_stew":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:meat",
            "minecraft:water_bucket"
            ],
        "raw_vegetable_stew":[
            "minecraft:bowl",
            "#better_recipes:vegetables",
            "#better_recipes:vegetables",
            "minecraft:water_bucket"
            ],
        "raw_custard":[
            "minecraft:bowl",
            "minecraft:egg",
            "minecraft:sugar",
            "minecraft:milk_bucket"
            ],
        "raw_cookies":[
            "minecraft:bowl",
            "minecraft:wheat",
            "minecraft:sugar",
            "minecraft:egg"
            ],
        "dough":[
            "minecraft:bowl",
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:water_bucket"
            ],
        "milk_dough":[
            "minecraft:bowl",
            "minecraft:wheat",
            "minecraft:wheat",
            "minecraft:milk_bucket"
            ],
        "mash":[
            "minecraft:bowl",
            "minecraft:wheat",
            "minecraft:water_bucket"
            ],
        "noodles":[
            "minecraft:bowl",
            "minecraft:wheat",
            "minecraft:egg",
            "minecraft:water_bucket"
            ],
        "raw_meat_patty":[
            "minecraft:bowl",
            "#better_recipes:meat",
            ],
        "raw_meatballs":[
            "minecraft:bowl",
            "#better_recipes:meat",
            "minecraft:egg",
            "minecraft:allium",
            ],
        "curdled_milk":[
            "minecraft:bowl",
            "minecraft:milk_bucket",
            "minecraft:chorus_fruit",
            ]
    }

# data to generate all recipes that merge two custom items

merge = {
        "pizza": [ "risen_dough","cheese" ],
        "burger": [ "milk_bread","meat_patty" ],
        "fish_ramen": [ "fish_broth","cooked_noodles" ],
        "meat_ramen": [ "meat_broth","cooked_noodles" ],
        "vegetable_ramen": [ "vegetable_broth","cooked_noodles" ],
}

# Transformation recipes in each station

trans_oven = {
        "raw_cookies":"cookies",
        "dough":"hardtack",
        "boiled_dough":"bagel",
        "risen_dough":"baguette",
        "risen_milk_dough":"milk_bread",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        "pizza":"cooked_pizza",
        }

trans_pan = {
        "raw_meat_patty":"meat_patty",
        "raw_meatballs":"meatballs",
        "dough":"flatbread",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        }

trans_pot = {
        "raw_fish_broth":"fish_broth",
        "raw_meat_broth":"meat_broth",
        "raw_vegetable_broth":"vegetable_broth",
        "raw_fish_stew":"fish_stew",
        "raw_meat_stew":"meat_stew",
        "raw_vegetable_stew":"vegetable_stew",
        "raw_custard":"custard",
        "risen_dough":"boiled_dough",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        "dried_azure_bluet":"azure_bluet_tea",
        "dried_blue_orchid":"blue_orchid_tea",
        "dried_cornflower":"cornflower_tea",
        "dried_dandelion":"dandelion_tea",
        "dried_lily_of_the_valley":"lily_of_the_valley_tea",
        "dried_oxeye_daisy":"oxeye_daisy_tea",
        "dried_poppy":"poppy_tea",
        "dried_orange_tulip":"orange_tulip_tea",
        "dried_pink_tulip":"pink_tulip_tea",
        "dried_red_tulip":"red_tulip_tea",
        "dried_white_tulip":"white_tulip_tea",
        "dried_lilac":"lilac_tea",
        "dried_peony":"peony_tea",
        "dried_rose_bush":"rose_bush_tea",
        "dried_sunflower":"sunflower_tea",
        "mash":"wort",
        "curdled_milk":"cheese_curds",
        "noodles":"cooked_noodles",
        }

trans_steamer = {
        "risen_dough":"mantou",
        "potato":"baked_potato",
        "porkchop":"cooked_porkchop",
        "beef":"cooked_beef",
        "mutton":"cooked_mutton",
        "chicken":"cooked_chicken",
        "rabbit":"cooked_rabbit",
        "cod":"cooked_cod",
        "salmon":"cooked_salmon",
        }

trans_rack = {
        "dough":"risen_dough",
        "milk_dough":"risen_milk_dough",
        "azure_bluet":"dried_azure_bluet",
        "blue_orchid":"dried_blue_orchid",
        "cornflower":"dried_cornflower",
        "dandelion":"dried_dandelion",
        "lily_of_the_valley":"dried_lily_of_the_valley",
        "oxeye_daisy":"dried_oxeye_daisy",
        "poppy":"dried_poppy",
        "orange_tulip":"dried_orange_tulip",
        "pink_tulip":"dried_pink_tulip",
        "red_tulip":"dried_red_tulip",
        "white_tulip":"dried_white_tulip",
        "lilac":"dried_lilac",
        "peony":"dried_peony",
        "rose_bush":"dried_rose_bush",
        "sunflower":"dried_sunflower",
        }

trans_press = {
        "potato":"potato_juice",
        "carrot":"carrot_juice",
        "beetroot":"beetroot_juice",
        "apple":"apple_juice",
        "melon_slice":"melon_juice",
        "pumpkin":"pumpkin_juice",
        "sweet_berries":"sweet_berry_juice",
        "glow_berries":"glow_berry_juice",
}

trans_cask = {
        "potato_juice":"potato_wine",
        "carrot_juice":"carrot_wine",
        "beetroot_juice":"beetroot_wine",
        "apple_juice":"apple_wine",
        "melon_juice":"melon_wine",
        "pumpkin_juice":"pumpkin_wine",
        "sweet_berry_juice":"sweet_berry_wine",
        "glow_berry_juice":"glow_berry_wine",
        "wort":"beer",
        "cheese_curds":"cheese",
        "cheese":"aged_cheese",
        "sweet_berries":"prunes",
        "glow_berries":"prunes",
}

trans_still = {
        "potato_wine":"potato_liquor",
        "carrot_wine":"carrot_liquor",
        "beetroot_wine":"beetroot_liquor",
        "apple_wine":"apple_liquor",
        "melon_wine":"melon_liquor",
        "pumpkin_wine":"pumpkin_liquor",
        "sweet_berry_wine":"sweet_berry_liquor",
        "glow_berry_wine":"glow_berry_liquor",
        "beer":"whisky",
}

# Generate data table for all possible results of a recipe
# This data is tagged to a suspicious_stew item to make the actual cooked item

with open("data/better_recipes/functions/results.mcfunction", 'w') as g:
    for r in results:
        name = json.dumps({"text":results[r][0],"italic":False})
        g.write("data modify storage wishlist:better_recipes {} set value ".format(r[0]))
        g.write(json.dumps({
            "components":{
                "CustomModelData":results[r][1],
                "display":{"Name":name}
                }
            }))
        g.write("\n\n")

# Generate data tables for all possible transitions in a cooking station

with open("data/better_recipes/functions/transitions.mcfunction", 'w') as f:
    f.write("data modify storage wishlist:better_recipes oven set value { ")
    for t in trans_oven:
        f.write("{}:{},".format(t, trans_oven[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes pan set value { ")
    for t in trans_pan:
        f.write("{}:{},".format(t, trans_pan[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes pot set value { ")
    for t in trans_pot:
        f.write("{}:{},".format(t, trans_pot[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes steamer set value { ")
    for t in trans_steamer:
        f.write("{}:{},".format(t, trans_steamer[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes press set value { ")
    for t in trans_press:
        f.write("{}:{},".format(t, trans_press[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes rack set value { ")
    for t in trans_rack:
        f.write("{}:{},".format(t, trans_rack[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes cask set value { ")
    for t in trans_cask:
        f.write("{}:{},".format(t, trans_cask[t]))
    f.write("}\n\n")

    f.write("data modify storage wishlist:better_recipes still set value { ")
    for t in trans_still:
        f.write("{}:{},".format(t, trans_still[t]))
    f.write("}\n\n")


# Generate vanilla recipes for custom data items
# These are the cooked items that can be constructed on a crafting table from
# vanilla minecraft objects; before 1.20.5 allows us to use components in
# recipe outputs, we have to use a recipe/advancement/function combo to
# actually give the right item to the player who crafts it.

for r in recipes:
    ingredients = []
    for i in recipes[r]:
        if i[0] == '#':
            ingredients.append({"tag":i[1:]})
        else:
            ingredients.append({"item":i})

    with open("data/better_recipes/recipes/craft/{}.json".format(r), 'w') as f:
        f.write(json.dumps({
            "type": "minecraft:crafting_shapeless",
            "category": "food",
            "ingredients": ingredients,
            "result":{
                "item":"minecraft:barrier"
                }
            }, indent=2))

    with open("data/better_recipes/advancements/craft/{}.json".format(r), 'w') as f:
        f.write(json.dumps({
            "criteria": {
                "example": {
                    "trigger": "minecraft:recipe_crafted",
                    "conditions": {
                        "recipe_id": "better_recipes:craft/{}".format(r)
                        }
                    }
                },
            "rewards": {
                "function": "better_recipes:craft/{}".format(r)
                }
            }, indent=2))

    with open("data/better_recipes/functions/craft/{}.mcfunction".format(r), 'w') as f:
        f.write("advancement revoke @s only better_recipes:craft/{}\n".format(r))
        f.write("clear @s minecraft:barrier\n")
        f.write("function better_recipes:give {{id:{}}}\n".format(r))

# Generate custom recipes for merging two custom items
# Even after 1.20.5, minecraft vanilla recipes can't handle custom data on
# inputs, so we have to use an advancement to see when the general recipe to
# merge two custom foods is used with specific data, and call a function as
# advancement reword.

for m in merge:
    with open("data/better_recipes/advancements/merge/{}.json".format(m), 'w') as f:
        f.write(json.dumps({
            "criteria": {
                "example": {
                    "trigger": "minecraft:recipe_crafted",
                    "conditions": {
                        "recipe_id": "better_recipes:merge",
                        "ingredients": [
                            { "nbt":"{{wishlist:{{recipe:{{id:{}}}}}}}".format(merge[m][0]) },
                            { "nbt":"{{wishlist:{{recipe:{{id:{}}}}}}}".format(merge[m][1]) }
                            ]
                        }
                    }
                },
            "rewards": {
                "function": "better_recipes:merge/{}".format(m)
                }
            }, indent=2))

    with open("data/better_recipes/functions/merge/{}.mcfunction".format(m), 'w') as f:
        f.write("advancement revoke @s only better_recipes:merge/{}\n".format(m))
        f.write("clear @s minecraft:barrier\n")
        f.write("function better_recipes:give {{id:{}}}\n".format(m))
        f.write("give @s minecraft:bowl\n")

# Generate knowledge book holding all the vanilla recipes

recipe_names = []
for r in recipes:
    recipe_names.append(r)

with open("data/better_recipes/loot_tables/book.json", 'w') as f:
    f.write(json.dumps({
  "pools": [
    {
      "rolls": 1,
      "entries": [
        {
          "type": "minecraft:item",
          "name": "minecraft:knowledge_book",
          "functions": [
            {
              "function": "minecraft:set_name",
              "name": {
                "translate":"Recipe Book",
                "italic": False
              }
            },
            {
              "function": "minecraft:set_nbt",
              "tag": "{{Recipes:{}}}".format(json.dumps(recipe_names))
            }
          ]
        }
      ]
    }
  ]
}, indent=2))

# Generate documentation for all possible results

def name(x): return "![{}] [{}](#{})".format(results[x][1], results[x][0],
        re.sub("[^a-z]", "-", results[x][0].lower()))

with open("results.md", 'w') as f:
    f.write("Better Recipes Result List\n")
    f.write("==========================\n")
    f.write("\n")
    for r in results:
        f.write("### ![{}] {}\n".format(results[r][1], results[r][0]))
        if r in recipes:
            f.write("* Craft {}\n".format(",".join(recipes[r])))
        if r in merge:
            f.write("* Craft {}\n".format(",".join(name(x) for x in merge[r])))
        for t in trans_oven:
            if trans_oven[t] == r:
                f.write("* Bake {}\n".format(name(t)))
        for t in trans_pan:
            if trans_pan[t] == r:
                f.write("* Grill {}\n".format(name(t)))
        for t in trans_pot:
            if trans_pot[t] == r:
                f.write("* Boil {}\n".format(name(t)))
        for t in trans_steamer:
            if trans_steamer[t] == r:
                f.write("* Steam {}\n".format(name(t)))
        for t in trans_rack:
            if trans_rack[t] == r:
                f.write("* Rest {}\n".format(name(t)))
        for t in trans_press:
            if trans_press[t] == r:
                f.write("* Press {}\n".format(name(t)))
        for t in trans_cask:
            if trans_cask[t] == r:
                f.write("* Age {}\n".format(name(t)))
        for t in trans_still:
            if trans_still[t] == r:
                f.write("* Distill {}\n".format(name(t)))

    f.write("\n")
    f.write("[628318]: resources/assets/wishlist/textures/item/bowl_of_white_chunks.png \"\"\n")
    f.write("[628319]: resources/assets/wishlist/textures/item/bowl_of_pale_chunks.png \"\"\n")
    f.write("[628320]: resources/assets/wishlist/textures/item/bowl_of_orange_chunks.png \"\"\n")
    f.write("[628321]: resources/assets/wishlist/textures/item/bowl_of_red_chunks.png \"\"\n")
    f.write("[628322]: resources/assets/wishlist/textures/item/bowl_of_brown_chunks.png \"\"\n")
    f.write("[628323]: resources/assets/wishlist/textures/item/bowl_of_black_chunks.png \"\"\n")
    f.write("[628324]: resources/assets/wishlist/textures/item/bowl_of_white_liquid.png \"\"\n")
    f.write("[628325]: resources/assets/wishlist/textures/item/bowl_of_pale_liquid.png \"\"\n")
    f.write("[628326]: resources/assets/wishlist/textures/item/bowl_of_orange_liquid.png \"\"\n")
    f.write("[628327]: resources/assets/wishlist/textures/item/bowl_of_red_liquid.png \"\"\n")
    f.write("[628328]: resources/assets/wishlist/textures/item/bowl_of_brown_liquid.png \"\"\n")
    f.write("[628329]: resources/assets/wishlist/textures/item/bowl_of_black_liquid.png \"\"\n")
    f.write("[628330]: resources/assets/wishlist/textures/item/bowl_of_white_stuff.png \"\"\n")
    f.write("[628331]: resources/assets/wishlist/textures/item/bowl_of_pale_stuff.png \"\"\n")
    f.write("[628332]: resources/assets/wishlist/textures/item/bowl_of_orange_stuff.png \"\"\n")
    f.write("[628333]: resources/assets/wishlist/textures/item/bowl_of_red_stuff.png \"\"\n")
    f.write("[628334]: resources/assets/wishlist/textures/item/bowl_of_brown_stuff.png \"\"\n")
    f.write("[628335]: resources/assets/wishlist/textures/item/bowl_of_black_stuff.png \"\"\n")
    f.write("[628336]: resources/assets/wishlist/textures/item/bowl_of_mess.png \"\"\n")
    f.write("[628336]: resources/assets/wishlist/textures/item/bowl_of_mess.png \"\"\n")
    f.write("[628337]: resources/assets/wishlist/textures/item/burger.png \"\"\n")
    f.write("[628338]: resources/assets/wishlist/textures/item/cheeseburger.png \"\"\n")
    f.write("[628339]: resources/assets/wishlist/textures/item/pizza_slice.png \"\"\n")
    f.write("[628340]: resources/assets/wishlist/textures/item/waffle.png \"\"\n")
    f.write("[628341]: resources/assets/grabeltone/textures/item/baguette.png \"\"\n")
    f.write("[628342]: resources/assets/grabeltone/textures/item/bagel.png \"\"\n")
    f.write("[628343]: resources/assets/grabeltone/textures/item/flatbread.png \"\"\n")
    f.write("[628344]: resources/assets/grabeltone/textures/item/rupjmaize.png \"\"\n")
    f.write("[628345]: resources/assets/grabeltone/textures/item/pretzel.png \"\"\n")
    f.write("[628346]: resources/assets/grabeltone/textures/item/hardtack.png \"\"\n")
    f.write("[628347]: resources/assets/grabeltone/textures/item/mantou.png \"\"\n")
    f.write("[628348]: resources/assets/grabeltone/textures/item/pandesal.png \"\"\n")
    f.write("[628349]: resources/assets/grabeltone/textures/item/cornbread.png \"\"\n")
    f.write("[271835]: resources/assets/grabeltone/textures/item/noodles.png \"\"\n")

