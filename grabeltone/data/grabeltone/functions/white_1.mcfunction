# white_1.mcfunction

data merge entity @s \
        { \
          text:'{"text":""}', \
          background:-1, \
          brightness:{ \
            block:15, \
            sky:15 \
          } \
        }

data modify entity @s transformation.scale set value [160.f,160.f,1.f]
data modify entity @s transformation.translation set value [-2.f, -2.f, 0.f]

execute if score rot wishlist.vars matches -180..-135 \
        run tp @s ~ ~ ~0.01 0 0
execute if score rot wishlist.vars matches -135..-45 \
        run tp @s ~-0.01 ~ ~ 90 0
execute if score rot wishlist.vars matches -45..45 \
        run tp @s ~ ~ ~-0.01 180 0
execute if score rot wishlist.vars matches 45..135 \
        run tp @s ~0.01 ~ ~ -90 0
execute if score rot wishlist.vars matches 135..180 \
        run tp @s ~ ~ ~0.01 0 0
