# medium_light_square_1.mcfunction

data merge entity @s \
        { \
          text:'{"text":""}', \
          background:-1, \
          brightness:{ \
            block:15, \
            sky:15 \
          } \
        }

data modify entity @s transformation.scale set value [20.f,20.f,1.f]
data modify entity @s transformation.translation set value [-0.25f, -0.25f, 0.f]

execute if score facing wishlist.vars matches 0 \
        run tp @s ~ ~0.02 ~ 0 90
execute if score facing wishlist.vars matches 1 \
        run tp @s ~ ~-0.02 ~ 0 -90
execute if score facing wishlist.vars matches 2 \
        run tp @s ~ ~ ~0.02 180 0
execute if score facing wishlist.vars matches 3 \
        run tp @s ~ ~ ~-0.02 0 0
execute if score facing wishlist.vars matches 4 \
        run tp @s ~0.02 ~ ~ 90 0
execute if score facing wishlist.vars matches 5 \
        run tp @s ~-0.02 ~ ~ -90 0

