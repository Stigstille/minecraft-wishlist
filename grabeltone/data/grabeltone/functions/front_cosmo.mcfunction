# front_cosmo.mcfunction

execute if score @s fronting matches 0 \
        run team modify player.cosmo prefix {"text":"Alya ","color":"dark_aqua"}
execute if score @s fronting matches 0 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 1 \
        run team modify player.cosmo prefix {"text":"Red ","color":"dark_aqua"}
execute if score @s fronting matches 1 \
        run team modify player.cosmo suffix {"text":" (he/him)","color":"gray"}

execute if score @s fronting matches 2 \
        run team modify player.cosmo prefix {"text":"Alex ","color":"dark_aqua"}
execute if score @s fronting matches 2 \
        run team modify player.cosmo suffix {"text":" (they/them)","color":"gray"}

execute if score @s fronting matches 3 \
        run team modify player.cosmo prefix {"text":"Emerald ","color":"dark_aqua"}
execute if score @s fronting matches 3 \
        run team modify player.cosmo suffix {"text":" (she/they)","color":"gray"}

execute if score @s fronting matches 4 \
        run team modify player.cosmo prefix {"text":"Ariana ","color":"dark_aqua"}
execute if score @s fronting matches 4 \
        run team modify player.cosmo suffix {"text":" (she/they)","color":"gray"}

execute if score @s fronting matches 5 \
        run team modify player.cosmo prefix {"text":"Helena ","color":"dark_red"}
execute if score @s fronting matches 5 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 6 \
        run team modify player.cosmo prefix {"text":"Unknown ","color":"dark_aqua"}
execute if score @s fronting matches 6 \
        run team modify player.cosmo suffix {"text":" (any/all)","color":"gray"}

execute if score @s fronting matches 7 \
        run team modify player.cosmo prefix {"text":"Rowan ","color":"dark_aqua"}
execute if score @s fronting matches 7 \
        run team modify player.cosmo suffix {"text":" (he/they)","color":"gray"}

execute if score @s fronting matches 8 \
        run team modify player.cosmo prefix {"text":"Aurelia ","color":"dark_aqua"}
execute if score @s fronting matches 8 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 9 \
        run team modify player.cosmo prefix {"text":"Emily ","color":"dark_aqua"}
execute if score @s fronting matches 9 \
        run team modify player.cosmo suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 10 \
        run team modify player.cosmo prefix {"text":"Virgil ","color":"dark_aqua"}
execute if score @s fronting matches 10 \
        run team modify player.cosmo suffix {"text":" (she/Faer)","color":"gray"}

execute if score @s fronting matches 11 \
        run team modify player.cosmo prefix {"text":"Eleanor ","color":"light_purple"}
execute if score @s fronting matches 11 \
        run team modify player.cosmo suffix {"text":" (fae/Her)","color":"gray"}

scoreboard players enable @s fronting
