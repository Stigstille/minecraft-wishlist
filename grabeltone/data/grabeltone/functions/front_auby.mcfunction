# front_auby.mcfunction

execute if score @s fronting matches 0 \
        run team modify player.auby prefix {"text":"Monika ","color":"light_purple"}
execute if score @s fronting matches 0 \
        run team modify player.auby suffix {"text":" (they/them)","color":"gray"}

execute if score @s fronting matches 1 \
        run team modify player.auby prefix {"text":"Bonnie ","color":"yellow"}
execute if score @s fronting matches 1 \
        run team modify player.auby suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 2 \
        run team modify player.auby prefix {"text":"Natsuki ","color":"gold"}
execute if score @s fronting matches 2 \
        run team modify player.auby suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 3 \
        run team modify player.auby prefix {"text":"Grimm ","color":"gold"}
execute if score @s fronting matches 3 \
        run team modify player.auby suffix {"text":" (they/he)","color":"gray"}

execute if score @s fronting matches 4 \
        run team modify player.auby prefix {"text":"Ziyah ","color":"gold"}
execute if score @s fronting matches 4 \
        run team modify player.auby suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 5 \
        run team modify player.auby prefix {"text":"Giratina ","color":"gold"}
execute if score @s fronting matches 5 \
        run team modify player.auby suffix {"text":" (he/they)","color":"gray"}

execute if score @s fronting matches 6 \
        run team modify player.auby prefix {"text":"Scarlett ","color":"gold"}
execute if score @s fronting matches 6 \
        run team modify player.auby suffix {"text":" (fae/she)","color":"gray"}

execute if score @s fronting matches 7 \
        run team modify player.auby prefix {"text":"Michael ","color":"gold"}
execute if score @s fronting matches 7 \
        run team modify player.auby suffix {"text":" (he/him)","color":"gray"}

scoreboard players enable @s fronting
