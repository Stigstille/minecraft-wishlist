# front_alphayix.mcfunction

execute if score @s fronting matches 0 \
        run team modify player.alphayix prefix {"text":"Ziyah ","color":"gold"}
execute if score @s fronting matches 0 \
        run team modify player.alphayix suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 1 \
        run team modify player.alphayix prefix {"text":"Rain ","color":"dark_purple"}
execute if score @s fronting matches 1 \
        run team modify player.alphayix suffix {"text":" (she/her)","color":"gray"}

execute if score @s fronting matches 2 \
        run team modify player.alphayix prefix {"text":"Helen ","color":"dark_red"}
execute if score @s fronting matches 2 \
        run team modify player.alphayix suffix {"text":" (she/they)","color":"gray"}

execute if score @s fronting matches 3 \
        run team modify player.alphayix prefix {"text":"Ursa ","color":"dark_purple"}
execute if score @s fronting matches 3 \
        run team modify player.alphayix suffix {"text":" (any/all)","color":"gray"}

execute if score @s fronting matches 4 \
        run team modify player.alphayix prefix {"text":"Vesper ","color":"dark_purple"}
execute if score @s fronting matches 4 \
        run team modify player.alphayix suffix {"text":" (they/them)","color":"gray"}

execute if score @s fronting matches 5 \
        run team modify player.alphayix prefix {"text":"Harmon ","color":"dark_purple"}
execute if score @s fronting matches 5 \
        run team modify player.alphayix suffix {"text":" (he/him)","color":"gray"}

execute if score @s fronting matches 6 \
        run team modify player.alphayix prefix {"text":"Patches ","color":"dark_purple"}
execute if score @s fronting matches 6 \
        run team modify player.alphayix suffix {"text":" (she/fae)","color":"gray"}

execute if score @s fronting matches 7 \
        run team modify player.alphayix prefix {"text":"Maple ","color":"dark_purple"}
execute if score @s fronting matches 7 \
        run team modify player.alphayix suffix {"text":" (she/it)","color":"gray"}

execute if score @s fronting matches 8 \
        run team modify player.alphayix prefix {"text":"Wonder ","color":"dark_purple"}
execute if score @s fronting matches 8 \
        run team modify player.alphayix suffix {"text":" (no pronouns)","color":"gray"}

scoreboard players enable @s fronting
