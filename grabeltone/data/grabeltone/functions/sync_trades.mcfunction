# sync_trades.mcfunction

data modify entity b5dccb72-d3e0-4fe2-b7e4-d9751664fe5c Offers.Recipes[{}]."Paper.IgnoreDiscounts" set value 1b

data modify entity @s[tag=magic_egg_trader] Offers \
        set from entity bfb6f6e9-36d5-4727-b427-6114668963c8 Offers
data modify entity @s[tag=banknote_trader] Offers \
        set from entity b5dccb72-d3e0-4fe2-b7e4-d9751664fe5c Offers
data modify entity @s[tag=bus_driver] Offers \
        set from entity 838d2205-821e-45d6-bf77-970f873030a2 Offers
data modify entity @s[tag=intercity_driver] Offers \
        set from entity b88729cf-aefd-458e-b624-95cd7fcc27ae Offers
data modify entity @s[tag=train_driver] Offers \
        set from entity 42b84071-67be-48fe-acd7-4d52c68a89d0 Offers
data modify entity @s[tag=ferry_driver] Offers \
        set from entity 2c715576-b85a-4dfc-87cf-608f90bf234f Offers
data modify entity @s[tag=box_trader] Offers \
        set from entity 0b098631-f2a2-48ce-a8de-c220dab0fd67 Offers
data modify entity @s[tag=drink_trader] Offers \
        set from entity c107a988-7faf-4696-bc39-fb50ec31405b Offers
data modify entity @s[tag=sushi_trader] Offers \
        set from entity 1df45c69-8d43-4b69-8af7-5783c5798a20 Offers
data modify entity @s[tag=intercity2_driver] Offers \
        set from entity f7d2654b-ff0b-4df8-85cb-914f22539116 Offers
data modify entity @s[tag=server_tools_trader] Offers \
        set from entity 3a1d47d2-1579-42d3-aaf0-60e66767cbec Offers
data modify entity @s[tag=map_tools_trader] Offers \
        set from entity c082feab-1cea-4233-be03-91a32bc1d2e9 Offers
data modify entity @s[tag=bread_trader] Offers \
        set from entity bb3a6920-0d07-4112-93de-5efb87feee71 Offers
data modify entity @s[tag=airship_pilot] Offers \
        set from entity 511ce5b4-b91f-46df-a25b-d8d3db970cb1 Offers
data modify entity @s[tag=intercity3_driver] Offers \
        set from entity ae354d82-985d-44f1-8951-143d17f7ab2a Offers
data modify entity @s[tag=intercity4_driver] Offers \
        set from entity b928b173-5b22-433d-886b-576a1c934e0f Offers
data modify entity @s[tag=grabeltone.quip_trader] Offers \
        set from entity dcd0019d-51bb-481d-af39-bf5ce13aad84 Offers
data modify entity @s[tag=grabeltone.emerald_trader] Offers \
        set from entity b5dccb72-d3e0-4fe2-b7e4-d9751664fe5c Offers
data modify entity @s[tag=grabeltone.ferry_driver_2] Offers.Recipes \
        set from storage grabeltone:args ferry_trades_2

tag @s[tag=magic_egg_trader] add wishlist.server_trader
tag @s[tag=banknote_trader] add wishlist.server_trader
tag @s[tag=bus_driver] add wishlist.server_trader
tag @s[tag=intercity_driver] add wishlist.server_trader
tag @s[tag=train_driver] add wishlist.server_trader
tag @s[tag=ferry_driver] add wishlist.server_trader
tag @s[tag=box_trader] add wishlist.server_trader
tag @s[tag=drink_trader] add wishlist.server_trader
tag @s[tag=sushi_trader] add wishlist.server_trader
tag @s[tag=intercity2_driver] add wishlist.server_trader
tag @s[tag=server_tools_trader] add wishlist.server_trader
tag @s[tag=map_tools_trader] add wishlist.server_trader
tag @s[tag=bread_trader] add wishlist.server_trader
tag @s[tag=airship_pilot] add wishlist.server_trader
tag @s[tag=intercity3_driver] add wishlist.server_trader
tag @s[tag=intercity4_driver] add wishlist.server_trader

data modify entity @s[tag=bus_driver] ArmorItems[3].tag.CustomModelData set value 271830
data modify entity @s[tag=intercity_driver] ArmorItems[3].tag.CustomModelData set value 271830
data modify entity @s[tag=intercity2_driver] ArmorItems[3].tag.CustomModelData set value 271830
data modify entity @s[tag=intercity3_driver] ArmorItems[3].tag.CustomModelData set value 271830
data modify entity @s[tag=intercity4_driver] ArmorItems[3].tag.CustomModelData set value 271830
