# load.mcfunction

forceload add -16 -16 15 15
forceload add -48 -80 -18 -49

scoreboard objectives add grabeltone.die minecraft.custom:minecraft.deaths

data modify storage wishlist:args all_chatter append value \
        "Welcome to Grabeltone!"
data modify storage wishlist:args all_chatter append value \
        "Have you found the Eight Bits yet?"
data modify storage wishlist:args all_chatter append value \
        "There's something weird under Ixuwata's well"
data modify storage wishlist:args all_chatter append value \
        "There's a strange tree east of L`ong Sông"
data modify storage wishlist:args all_chatter append value \
        "There's a temple on the jungle west from Spawn that goes deeper than people think"
data modify storage wishlist:args all_chatter append value \
        "Electrum Factory was built on top of something older and stranger"
data modify storage wishlist:args all_chatter append value \
        "The grandparent of all monuments is under some sandy islands not far from the Medieval Road"
data modify storage wishlist:args all_chatter append value \
        "Some of the ruins under the Western Desert may not be entirely dead"
data modify storage wishlist:args all_chatter append value \
        "The darkest of ruins is said to be under White Cliffs"
data modify storage wishlist:args all_chatter append value \
        "Somewhere under the Winter Road, there's a very cold hidden fortress"
data modify storage wishlist:args all_chatter append value \
        "Rumor says that there's a very deep well no one dares to enter in a forest southeast"
data modify storage wishlist:args all_chatter append value \
        "Ever got to hold an impossible thing?"
data modify storage wishlist:args all_chatter append value \
        "Who you gonna call? check the advancement"
data modify storage wishlist:args all_chatter append value \
        "A blacksmith at Black Bastion can produce incredible tools"
data modify storage wishlist:args all_chatter append value \
        "There's this little shop at Narshe that sells everlasting lighters and shears"
data modify storage wishlist:args all_chatter append value \
        "You can buy magical arrows at the Alcázar where the Medieval Road starts"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of lifetime stuff? It will last your entire life. One of them, at least"
data modify storage wishlist:args all_chatter append value \
        "Dying is not the end; we have an agreement with the conductors of the Train"
data modify storage wishlist:args all_chatter append value \
        "Do not linger in train tunnels; you never know when it's coming"
data modify storage wishlist:args all_chatter append value \
        "The great libraries at Spawn and Bastion have everything. For a price"
data modify storage wishlist:args all_chatter append value \
        "There are five bus lines around the world, and one each of train, airship and ferry"
data modify storage wishlist:args all_chatter append value \
        "I don't use the transports that much since we started having map halls"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of Vonturbola and Rallyim? They have been everywhere"

function grabeltone:tick
function grabeltone:tick_1s
function grabeltone:tick_3s
