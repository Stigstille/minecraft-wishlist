# front_deathisacatgirl.mcfunction

execute if score @s fronting matches 0 \
        run team modify player.catgirl suffix {"text":" [hazel] (she/her)","color":"gray"}
execute if score @s fronting matches 1 \
        run team modify player.catgirl suffix {"text":" [void] (she/her)","color":"gray"}
execute if score @s fronting matches 2 \
        run team modify player.catgirl suffix {"text":" [joy] (they/she)","color":"gray"}
execute if score @s fronting matches 3 \
        run team modify player.catgirl suffix {"text":" [helena] (she/her)","color":"gray"}
execute if score @s fronting matches 4 \
        run team modify player.catgirl suffix {"text":" [jackie] (she/her)","color":"gray"}
execute if score @s fronting matches 5 \
        run team modify player.catgirl suffix {"text":" [???] (she/her)","color":"gray"}
scoreboard players enable @s fronting
