# tick_3s.mcfunction

execute at @a \
        as @e[type=#wishlist:trader,distance=..16] \
        run function grabeltone:sync_trades

execute at @a run tag @e[type=villager,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=pillager,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=piglin,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=piglin_brute,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=wandering_trader,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=witch,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=vindicator,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=illusioner,distance=..8] add wishlist.gossipy
execute at @a run tag @e[type=evoker,distance=..8] add wishlist.gossipy

schedule function grabeltone:tick_3s 3s


