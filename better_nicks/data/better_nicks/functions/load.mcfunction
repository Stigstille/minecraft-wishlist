# load.mcfunction

scoreboard objectives add wishlist.better_nicks.trigger trigger

data modify storage wishlist:args all_chatter append value \
        "You're a builder? I heard you people could have colored names"
data modify storage wishlist:args all_chatter append value \
        "You can set up your pronouns for people to check with a book"

function better_nicks:tick_1s
