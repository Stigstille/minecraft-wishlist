# create.mcfunction - create temporary seat

title @s actionbar { \
        "translate":"Press %s to Sit", \
        "with":[{"keybind":"key.use"}] \
        }

execute if block ~ ~ ~ minecraft:air \
        if block ~ ~-0.01 ~ #minecraft:stairs[half=bottom] \
        run return run summon minecraft:interaction ~ ~-0.5 ~ {height:0.51,Tags:[wishlist.seat]}

summon minecraft:interaction ~ ~ ~ {height:0.01,Tags:[wishlist.seat]}
