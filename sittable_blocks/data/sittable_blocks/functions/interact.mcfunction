# interact.mcfunction

tag @s add wishlist.match

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=minecraft:interaction,tag=wishlist.seat,distance=..4] \
        at @s \
        if data entity @s interaction \
        run function sittable_blocks:interact_1

advancement revoke @s only sittable_blocks:interact
tag @s remove wishlist.match

