# tick.mcfunction - update sittable blocks datapack

scoreboard players enable @a wishlist.sittable_blocks.disable

execute at @a run tag @e[tag=wishlist.seat,distance=..8] add wishlist.seat.prune
execute as @a on vehicle run tag @s remove wishlist.seat.prune
execute at @a run kill @e[tag=wishlist.seat.prune,distance=..8]

execute as @a[tag=wishlist.still,nbt={OnGround:1b},nbt=!{SelectedItem:{}},x_rotation=90] \
        unless score @s wishlist.sittable_blocks.disable matches 1.. \
        at @s \
        align xz \
        positioned ~0.5 ~ ~0.5 \
        run function sittable_blocks:create

schedule function sittable_blocks:tick 5t

