# load.mcfunction - setup sittable blocks datapack

scoreboard objectives add wishlist.sittable_blocks.disable trigger

data modify storage wishlist:args all_chatter append value \
        "I heard there was a challenge to sit on weird places"
data modify storage wishlist:args all_chatter append value \
        "Here! just hold nothing and look right down; you'll see"

function sittable_blocks:tick
