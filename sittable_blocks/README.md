Sittable Blocks
===============

Allows all users in the server to sit down on any block by looking right down
while holding nothing on their main hand and not moving, then clicking the use
button.

Compare with [Cushion](../cushion):

* Sittable Blocks does not allow to sit on interactions.
* Sittable Blocks does not trigger interactive blocks when sitting on them.
* Sittable Blocks will sit the player where they stand.
* Sittable Blocks requires the use of gestures to trigger the sitting.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Includes a trigger score that can be used to toggle the sitting on a per-player
basis; disable with:

```
/trigger wishlist.sittable_blocks.disable set 1
```

And to enable again:

```
/trigger wishlist.sittable_blocks.disable set 0
```

Overhead
--------

Updates every five ticks:

* kills unused seats in an 8-block radius around each player,
* sits down every player satisfying the conditions.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Take a Rest: sit on an interaction with the `wishlist.seat` tag
    * The Darkest Seat: sit on an interaction with the
      `wishlist.seat` tag on an ancient city
    * The Hottest Seat: sit on an interaction with the
      `wishlist.seat` tag on a bastion remnant
