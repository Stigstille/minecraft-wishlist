Modern Lights
=============

Provides light items that can be placed on a face of an existing block to
create bright spots that suggest LED panels of varying sizes or car lamps; the
lights can be removed by breaking the block they are placed on.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

To give a light item to a player, the following function is provided:

```
function modern_lights:give {id:<light type>}
```

Where the type os one of `small_bright`, `small_soft`, `small_dim`,
`medium_bright`, `medium_soft`, `medium_dim`, `large_bright`, `large_soft`,
`large_dim`.

Small lights are as wide as a fence post, medium lights are as wide as a wall,
and large lights are a full block in size.

Bright, soft, and dim lights emit a ligth level of 15, 11, and 7 respectively.

Overhead
--------

Uses one advancements:

* checks for players placing item frames with the nbt data that defines a light
  type.

Updates every second:

* checks every entity in a 6-block radius around each player in search of
  lights whose blocks have been removed to destroy and drop them as items.

Known Issues
------------

The datapack interacts badly with protection mods such as WorldGuard: if you
place a light, the mod will react by removing the item, and placing it back in
your inventory, but the datapack will still register the light as placed, so
the base item will be duped.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:


