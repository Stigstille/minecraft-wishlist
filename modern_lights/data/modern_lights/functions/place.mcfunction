# modern_lights/place.mcfunction

advancement revoke @s only modern_lights:place

execute as @e[type=item_frame,distance=..8] \
        if data entity @s Item.tag.wishlist.modern_light \
        at @s \
        run function modern_lights:place_1 with entity @s Item.tag.wishlist.modern_light

