# load.mcfunction

data modify storage wishlist:args all_chatter append value \
        "Have you seen the new light fixtures?"
data modify storage wishlist:args all_chatter append value \
        "There are some issues with the new light stuff, but damn if they aren't pretty"

data modify storage wishlist:modern_lights small_bright set value { \
        id: "small_bright", \
        scale: [10.f,10.f,1.f], \
        translation: [-0.125f,-0.125f,0.5f], \
        level: 15, \
        item:{ \
                tag: { \
                CustomModelData:271846, \
                display:{Name:'{"text":"Small Bright Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights medium_bright set value { \
        id: "medium_bright", \
        scale: [20.f,20.f,1.f], \
        translation: [-0.25f,-0.25f,0.5f], \
        level: 15, \
        item:{ \
                tag: { \
                CustomModelData:271847, \
                display:{Name:'{"text":"Medium Bright Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights large_bright set value { \
        id: "large_bright", \
        scale: [40.f,40.f,1.f], \
        translation: [-0.5f,-0.5f,0.5f], \
        level: 15, \
        item:{ \
                tag: { \
                CustomModelData:271848, \
                display:{Name:'{"text":"Large Bright Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights small_soft set value { \
        id: "small_soft", \
        scale: [10.f,10.f,1.f], \
        translation: [-0.125f,-0.125f,0.5f], \
        level: 11, \
        item:{ \
                tag: { \
                CustomModelData:271846, \
                display:{Name:'{"text":"Small Soft Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights medium_soft set value { \
        id: "medium_soft", \
        scale: [20.f,20.f,1.f], \
        translation: [-0.25f,-0.25f,0.5f], \
        level: 11, \
        item:{ \
                tag: { \
                CustomModelData:271847, \
                display:{Name:'{"text":"Medium Soft Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights large_soft set value { \
        id: "large_soft", \
        scale: [40.f,40.f,1.f], \
        translation: [-0.5f,-0.5f,0.5f], \
        level: 11, \
        item:{ \
                tag: { \
                CustomModelData:271848, \
                display:{Name:'{"text":"Large Soft Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights small_dim set value { \
        id: "small_dim", \
        scale: [10.f,10.f,1.f], \
        translation: [-0.125f,-0.125f,0.5f], \
        level: 7, \
        item:{ \
                tag: { \
                CustomModelData:271846, \
                display:{Name:'{"text":"Small Dim Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights medium_dim set value { \
        id: "medium_dim", \
        scale: [20.f,20.f,1.f], \
        translation: [-0.25f,-0.25f,0.5f], \
        level: 7, \
        item:{ \
                tag: { \
                CustomModelData:271847, \
                display:{Name:'{"text":"Medium Dim Light","italic":false}'} \
                } \
        } \
}

data modify storage wishlist:modern_lights large_dim set value { \
        id: "large_dim", \
        scale: [40.f,40.f,1.f], \
        translation: [-0.5f,-0.5f,0.5f], \
        level: 7, \
        item:{ \
                tag: { \
                CustomModelData:271848, \
                display:{Name:'{"text":"Large Dim Light","italic":false}'} \
                } \
        } \
}

function modern_lights:tick
