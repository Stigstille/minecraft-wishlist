# tick_3.mcfunction 

fill ~ ~ ~ ~ ~ ~ minecraft:air replace minecraft:light

execute if entity @s[tag=wishlist.modern_lights.small_bright] \
        run function modern_lights:drop {id:small_bright}
execute if entity @s[tag=wishlist.modern_lights.small_soft] \
        run function modern_lights:drop {id:small_soft}
execute if entity @s[tag=wishlist.modern_lights.small_dim] \
        run function modern_lights:drop {id:small_dim}

execute if entity @s[tag=wishlist.modern_lights.medium_bright] \
        run function modern_lights:drop {id:medium_bright}
execute if entity @s[tag=wishlist.modern_lights.medium_soft] \
        run function modern_lights:drop {id:medium_soft}
execute if entity @s[tag=wishlist.modern_lights.medium_dim] \
        run function modern_lights:drop {id:medium_dim}

execute if entity @s[tag=wishlist.modern_lights.large_bright] \
        run function modern_lights:drop {id:large_bright}
execute if entity @s[tag=wishlist.modern_lights.large_soft] \
        run function modern_lights:drop {id:large_soft}
execute if entity @s[tag=wishlist.modern_lights.large_dim] \
        run function modern_lights:drop {id:large_dim}

kill @s

