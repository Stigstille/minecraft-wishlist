# modern_lights/place_2.mcfunction

data merge entity @s { \
        Tags:[wishlist.modern_light], \ 
        text:'{"text":""}', \
        background:-1, \
        brightness:{ \
                block:15, \
                sky:15 \
        } \
}

data modify entity @s Rotation set from storage wishlist:args rotation
$data modify entity @s Tags append value $(id)
$data modify entity @s transformation.scale set value $(scale)
$data modify entity @s transformation.translation set value $(translation)
$setblock ^ ^ ^1 light[level=$(level)]
