# tick.mcfunction

schedule function modern_lights:tick 1s

execute at @a \
        anchored eyes \
        run function modern_lights:tick_1

execute at @a \
        anchored eyes \
        as @e[tag=wishlist.modern_light,distance=..6] \
        at @s \
        if block ~ ~ ~ #wishlist:empty \
        run function modern_lights:break
