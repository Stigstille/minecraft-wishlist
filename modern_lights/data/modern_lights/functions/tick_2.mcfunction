# tick_2.mcfunction

execute if entity @s[tag=wishlist.modern_lights.down] \
        if block ~ ~1 ~ #wishlist:empty \
        run function modern_lights:tick_3
execute if entity @s[tag=wishlist.modern_lights.up] \
        if block ~ ~-1 ~ #wishlist:empty \
        run function modern_lights:tick_3
execute if entity @s[tag=wishlist.modern_lights.north] \
        if block ~ ~ ~1 #wishlist:empty \
        run function modern_lights:tick_3
execute if entity @s[tag=wishlist.modern_lights.south] \
        if block ~ ~ ~-1 #wishlist:empty \
        run function modern_lights:tick_3
execute if entity @s[tag=wishlist.modern_lights.west] \
        if block ~1 ~ ~ #wishlist:empty \
        run function modern_lights:tick_3
execute if entity @s[tag=wishlist.modern_lights.east] \
        if block ~-1 ~ ~ #wishlist:empty \
        run function modern_lights:tick_3

