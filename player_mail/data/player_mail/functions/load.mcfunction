# load.mcfunction

data modify storage wishlist:args all_chatter append value \
        "Fast mail is a thing we would not have if we hadn't all worked on it"
data modify storage wishlist:args all_chatter append value \
        "Post deliverers will wait until you're on firm ground to deliver, but you may still be in an interesting position when they do so"

function player_mail:tick_1t
function player_mail:tick_1s
