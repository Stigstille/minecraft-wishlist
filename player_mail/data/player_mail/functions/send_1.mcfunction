# player_mail:send_1

execute unless data entity @s Item.tag.display.Name \
        run return run function player_mail:unnamed

# Do not accept any name that is not a plain quoted string; this applies mostly
# to JSON text from custom items, and is not a thing that can be done by naming
# the item in an anvil.

data modify entity @s Tags set value ['"']
data modify entity @s Tags append string entity @s Item.tag.display.Name 0 1
execute store result score num wishlist.vars if data entity @s Tags[]
data modify entity @s Tags set value []
execute if score num wishlist.vars matches 2 \
        run function player_mail:messnamed

# Clip the quotes from what's supposed to be a player name from the name of the
# item to be sent.

data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.name set string entity @s Item.tag.display.Name 1 -1
function player_mail:send_2 with storage wishlist:args args

