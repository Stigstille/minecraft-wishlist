# receive_2.mcfunction

execute at @s \
        rotated ~ 0 \
        run summon minecraft:enderman ^ ^ ^3 { \
          Tags:[wishlist.mail_giver,wishlist.look_alive], \
          CustomName:'"Post Carrier"', \
          NoAI:1b \
        }
playsound minecraft:entity.enderman.teleport neutral @a[distance=..8] ~ ~ ~
tellraw @s { \
        "translate":"wishlist.player_mail.receive_mail", \
        "with":[{"storage":"wishlist:args","nbt":"mail[0].src","interpret":true}], \
        "color":"dark_gray" \
        }
execute at @s \
        rotated ~ 0 \
        run summon minecraft:item ^ ^1.25 ^1 { \
          Tags:[wishlist.mail,wishlist.discarded_mail], \
          Item:{id:"minecraft:stick",Count:1b} \
        }
data modify entity @e[tag=wishlist.mail,sort=nearest,limit=1] Item \
        set from storage wishlist:args mail[0].Item
tag @e[tag=wishlist.mail,distance=..4] remove wishlist.mail
data remove storage wishlist:args mail[0]
scoreboard players set success wishlist.vars 1
advancement grant @s only wishlist:receive_mail
