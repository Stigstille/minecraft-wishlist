
execute on origin run title @s actionbar { \
        "translate":"<%s> Mail must be named with a player's name", \
        "with":[{"selector":"@e[tag=wishlist.mail_taker,sort=nearest,limit=1]"}] \
        }
tag @s add wishlist.discarded_mail
execute as @e[tag=wishlist.mail_taker,sort=nearest,limit=1] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:angry_villager ~ ~ ~ 0.2 0.2 0.2 1 1 normal
