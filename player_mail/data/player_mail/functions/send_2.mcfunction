# send_2

$execute unless score $(name) wishlist.playerId matches 0.. \
        run return run function player_mail:misnamed {name:$(name)}

execute on origin run tag @s add wishlist.match
setblock 0 0 0 minecraft:oak_sign
data modify block 0 0 0 front_text.messages[0] set value '{"selector":"@a[tag=wishlist.match,limit=1]"}'

data modify storage wishlist:args mail append value {}
data modify storage wishlist:args mail[-1].Item set from entity @s Item
data modify storage wishlist:args mail[-1].src set from block 0 0 0 front_text.messages[0]
$execute store result storage wishlist:args mail[-1].dst int 1 \
        run scoreboard players get $(name) wishlist.playerId
execute store result storage wishlist:args mail[-1].time int 1 \
        run time query gametime

setblock 0 0 0 minecraft:air

advancement grant @a[tag=wishlist.match] only wishlist:send_mail

kill @s
particle minecraft:happy_villager ~ ~1 ~ 0.2 0.6 0.2 1 10 normal
tag @a remove wishlist.match

