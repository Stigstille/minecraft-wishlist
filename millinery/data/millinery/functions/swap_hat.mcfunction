# swap_hat.mcfunction

data modify storage wishlist:args uuid set from entity @s Thrower
function wishlist:find_player_by_uuid
execute if data entity @a[tag=wishlist.match,limit=1] Inventory[{Slot:103b}] \
        run function millinery:swap_hat_2
execute unless data entity @a[tag=wishlist.match,limit=1] Inventory[{Slot:103b}] \
        run function millinery:swap_hat_1
data modify entity @s Thrower set value [I;0,0,0,0]
tag @a remove wishlist.match
