# swap_hat_1.mcfunction

setblock 0 0 0 minecraft:barrel
data modify block 0 0 0 Items append from entity @s Item
data modify block 0 0 0 Items[0].Count set value 1b
item replace entity @a[tag=wishlist.match,limit=1] armor.head from block 0 0 0 container.0

setblock 0 0 0 minecraft:air
function wishlist:decrement_item_entity_count

advancement grant @a[tag=wishlist.match] only wishlist:swap_hat
particle minecraft:happy_villager ~ ~1 ~ 0.2 0.6 0.2 1 10 normal

