# face_entity.mcfunction - face random entity

function wishlist:recall
scoreboard players set t1 wishlist.vars 0
execute store result score t1 wishlist.vars \
        run data get storage wishlist:args memories.focus_timeout
execute store result score t0 wishlist.vars run time query gametime
execute if score t1 wishlist.vars < t0 wishlist.vars \
        run function look_alive:face_entity_1
