# face_entity_1.mcfunction

tag @s add wishlist.self
execute positioned ~-4 ~-1 ~-4 \
        run tag @e[type=!marker,type=!interaction,tag=!wishlist.dull,dx=8,dy=2,dz=8,sort=random,limit=1] add wishlist.match
execute if entity @e[tag=wishlist.match,tag=!wishlist.self] \
        run function look_alive:face_entity_2
tag @e[distance=..8] remove wishlist.match
tag @s remove wishlist.match

function wishlist:rand
scoreboard players operation t1 wishlist.vars = rand wishlist.vars
scoreboard players operation t1 wishlist.vars %= 160 wishlist.vars
scoreboard players operation t1 wishlist.vars += 160 wishlist.vars
scoreboard players operation t1 wishlist.vars += t0 wishlist.vars
execute store result storage wishlist:args memories.focus_timeout int 1 \
        run scoreboard players get t1 wishlist.vars
function wishlist:memorize
