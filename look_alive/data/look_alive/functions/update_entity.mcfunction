# update_entity.mcfunction - switch entity focus somewhere else

execute if entity @a[gamemode=!spectator,distance=..4] \
        run function look_alive:face_player
execute unless entity @a[gamemode=!spectator,distance=..4] \
        run function look_alive:face_entity

