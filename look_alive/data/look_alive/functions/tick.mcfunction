# tick.mcfunction - animate some AI-less entities

schedule function look_alive:tick 1s
execute at @a \
        as @e[type=#wishlist:mobs,tag=wishlist.look_alive,distance=..16] \
        at @s \
        run function look_alive:update_entity
