# clamp_angle.mcfunction - prevent mobs from looking too straight up/down

execute store result score angle wishlist.vars run data get entity @s Rotation[1]
execute if score angle wishlist.vars matches 30.. run tp @s ~ ~ ~ ~ 30
execute if score angle wishlist.vars matches ..-30 run tp @s ~ ~ ~ ~ -30
