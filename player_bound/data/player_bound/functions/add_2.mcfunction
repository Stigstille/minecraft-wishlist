# add_2.mcfunction

data modify storage wishlist:args data set from entity @s data
kill @s

execute store result score n wishlist.vars \
        run data get storage wishlist:args data.type
execute if score n wishlist.vars matches 1 \
        summon minecraft:villager \
        run function player_bound:add_3
execute if score n wishlist.vars matches 2 \
        summon minecraft:pillager \
        run function player_bound:add_3
execute if score n wishlist.vars matches 3 \
        summon minecraft:vindicator \
        run function player_bound:add_3
execute if score n wishlist.vars matches 4 \
        summon minecraft:evoker \
        run function player_bound:add_3
execute if score n wishlist.vars matches 5 \
        summon minecraft:witch \
        run function player_bound:add_3
execute if score n wishlist.vars matches 6 \
        summon minecraft:piglin \
        run function player_bound:add_3
execute if score n wishlist.vars matches 7 \
        summon minecraft:piglin_brute \
        run function player_bound:add_3
execute if score n wishlist.vars matches 8 \
        summon minecraft:enderman \
        run function player_bound:add_3
execute if score n wishlist.vars matches 9 \
        summon minecraft:wandering_trader \
        run function player_bound:add_3
execute if score n wishlist.vars matches 10 \
        summon minecraft:illusioner \
        run function player_bound:add_3

