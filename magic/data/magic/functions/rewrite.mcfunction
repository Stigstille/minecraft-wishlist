# rewrite_book.mcfunction - rewrite book according to spells

data modify storage wishlist:args items set from entity @s Item.tag.wishlist_grimoire
data modify entity @s Item.tag.pages set value []

setblock 0 0 0 oak_sign
data modify block 0 0 0 front_text.messages[0] set from storage wishlist:magic page1
data modify entity @s Item.tag.pages append from block 0 0 0 front_text.messages[0]

data modify block 0 0 0 front_text.messages[0] set from storage wishlist:magic page2
data modify entity @s Item.tag.pages append from block 0 0 0 front_text.messages[0]

data modify block 0 0 0 front_text.messages[0] set from storage wishlist:magic page3
data modify entity @s Item.tag.pages append from block 0 0 0 front_text.messages[0]

setblock 0 0 0 air

