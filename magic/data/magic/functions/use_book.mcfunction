
execute unless data entity @s SelectedItem.tag.wishlist_grimoire \
        run return run title @s actionbar "You are not holding a magic book!"
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.grimoire
execute at @s run function magic:use_book_1 with storage wishlist:args args
advancement grant @s only wishlist:use_grimoire
