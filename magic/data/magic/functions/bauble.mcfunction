
data modify storage wishlist:args bauble set value {}
scoreboard players set done wishlist.vars 0
execute as @e[type=item,distance=..1] \
        if data entity @s Item.tag.wishlist_magic \
        unless data entity @s Item.tag.wishlist_bauble \
        run function magic:bauble_1
execute if score done wishlist.vars matches 0 run return fail
data remove entity @s Item.tag.display
data remove entity @s Item.tag.wishlist_magic
data modify entity @s Item.tag merge from storage wishlist:args bauble
playsound minecraft:block.anvil.use player @a ~ ~ ~

