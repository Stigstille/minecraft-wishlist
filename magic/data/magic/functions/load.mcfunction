
scoreboard objectives add wishlist.grimoire trigger

data modify storage wishlist:args all_chatter append value \
        "You have to tear Tickets to use them so they're only good once"
data modify storage wishlist:args all_chatter append value \
        "Do you have a Magic Wand? those things are pretty useful"
data modify storage wishlist:args all_chatter append value \
        "A Magic Book can hold up to twenty-seven magics, I guess they couldn't fit more"
data modify storage wishlist:args all_chatter append value \
        "I love Baubles: they're so fragile, but that's part of the charm"

function magic:pages
function magic:tick_1s
function magic:tick_1t
