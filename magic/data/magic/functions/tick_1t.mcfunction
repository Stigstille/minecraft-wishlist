
schedule function magic:tick_1t 1t

# Books are triggered when read
execute as @a[gamemode=!spectator,scores={wishlist.grimoire=0..}] \
        run function magic:use_book
scoreboard players enable @a wishlist.grimoire
scoreboard players set @a wishlist.grimoire -1

