
execute if entity @s[tag=wishlist.rebinded] run return fail

data modify storage wishlist:args items set from entity @s Item.tag.wishlist_grimoire
data modify entity @s Item.tag.wishlist_grimoire set from block ~ ~-1 ~ Items
data modify block ~ ~-1 ~ Items set from storage wishlist:args items
function magic:rewrite

tag @s add wishlist.rebinded

playsound minecraft:block.anvil.use player @a ~ ~ ~
execute on origin run title @s actionbar "Book rebinded"
