

schedule function magic:tick_1s 1s

execute at @a \
        as @e[type=item,distance=..4] \
        if data entity @s Item.tag.wishlist_grimoire \
        at @s \
        run function magic:book

execute at @a \
        as @e[type=item,distance=..4] \
        if data entity @s Item.tag.wishlist_bauble \
        at @s \
        run function magic:bauble
