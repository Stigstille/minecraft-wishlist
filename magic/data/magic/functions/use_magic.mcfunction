
function magic:use_magic_1 with entity @s SelectedItem.tag.wishlist_magic
execute unless entity @s[gamemode=creative] \
        if data entity @s SelectedItem.tag.wishlist_one_use \
        run item modify entity @s weapon.mainhand wishlist:consume_one

