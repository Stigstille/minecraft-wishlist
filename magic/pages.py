
import json

def page(number, begin, end):
    page = [""]
    for i in range(begin,end):
        page.append({
            "storage":"wishlist:args",
            "nbt":"items[{{Slot:{}b}}].tag.display.Name".format(i),
            "interpret":True,
            "bold":False,
            "clickEvent":{
                "action":"run_command",
                "value":"/trigger wishlist.grimoire set {}".format(i)
            },
            "hoverEvent":{
                "action":"show_text",
                "contents":{
                    "storage":"wishlist:args",
                    "nbt":"items[{{Slot:{}b}}].tag.display.Lore[0]".format(i),
                    "interpret":True,
                }
            }
        })
        page.append({"text":"\\n"})
    f.write("data modify storage wishlist:magic page{} set value '".format(number))
    f.write(json.dumps(page))
    f.write("'\n\n")

with open("data/magic/functions/pages.mcfunction",'w') as f:
    page(1, 0, 9)
    page(2, 9, 18)
    page(3, 18, 27)
