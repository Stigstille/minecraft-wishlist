Popular Displays
================

Provides tools to create display entity objects from items, or get the items
back from them. A low-level API to the datapack is provided through the
`wishlist.popular_displays.trigger` scoreboard, and a high-level API is
provided through a book that can be acquired with the `popular_displays:book`
loot table.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

To make a display item from an item, hold the book on your off hand and drop
the item. It will become a display item on the center of the block where it
fell.

Everything else is handled through the book UI.

Each line in the book is on the form:

```
[1 2 4 8 S] action
```

Where `action` refers to the action to be performed and the numbers are
clickable elements that perform that action on a number of item frames:

* `1` targets the item nearest to the player's face but no further than
  8 blocks,
* `2` targets every item up to a 2-block distance from the player's feet,
* `4` targets every item up to a 4-block distance from the player's feet,
* `8` targets every item up to a 8-block distance from the player's feet.
* `S` targets every selected (glowing) item up to a 16-block distance from the
  player's feet.

If you hover the cursor over the `action` a short explanation will pop up.

* `drop` destroy displays and drops the items that were used to spawn them

### Billboard

* `fixed` makes displays face a fixed direction,
* `horizontal` makes displays pivot horizontally to face players,
* `center` makes displays pivot in both axes to face players.

### Brightness

* `dark` displays render with light level 0,
* `dim` displays render with light level 7,
* `soft` displays render with light level 11,
* `bright` displays render with light level 15,
* `auto` displays render with light level on their position.

### Select

* `add` makes displays glow (and targetable by `S`),
* `remove` makes displays not glow (and not targetable by `S`),

### Move

* `+X` moves displays `1/16` of a block east,
* `-X` moves displays `1/16` of a block west,
* `+Y` moves displays `1/16` of a block up,
* `-Y` moves displays `1/16` of a block down,
* `+Z` moves displays `1/16` of a block south,
* `-Z` moves displays `1/16` of a block north,
* `center` places displays in the center of the block they occupy.

### Rotate

* `+yaw` rotates displays `22.5` degrees on the vertical axis,
* `-yaw` rotates displays `-22.5` degrees on the vertical axis,
* `+pitch` rotates displays `22.5` degrees on the horizontal axis,
* `-pitch` rotates displays `-22.5` degrees on the horizontal axis,
* `center` resets display rotation to zero in all axes.

### Lock

* `lock` locks displays so they won't be affected by further operations,
* `unlock` unlocks displays so they will be affected by further operations.

Overhead
--------

Updates once per second:

* searches all entities in a 4-block radius around players holding the Display
  Tools book for items to display,
* checks for players who have triggered a command with the Display Tools book.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* A Joy to Behold: create an item display
