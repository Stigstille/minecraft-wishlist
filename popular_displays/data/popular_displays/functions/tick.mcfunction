# tick.mcfunction

execute at @a[predicate=popular_displays:hold_displayer] \
        run function popular_displays:tick_player

execute as @a[scores={wishlist.popular_displays.trigger=1..}] \
        at @s \
        run function popular_displays:trigger

scoreboard players enable @a wishlist.popular_displays.trigger
scoreboard players set @a wishlist.popular_displays.trigger 0

schedule function popular_displays:tick 1s

