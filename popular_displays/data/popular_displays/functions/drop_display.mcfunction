# drop_display.mcfunction

data modify storage wishlist:args tags set from entity @s {}

execute at @s run summon minecraft:item ~ ~ ~ { \
        Item:{id:"minecraft:stick",Count:1b}, \
        Tags:[wishlist.popular_displays.drop] \
        }

data modify entity @e[tag=wishlist.popular_displays.drop,limit=1] Item \
        set from storage wishlist:args tags.item

tag @e[tag=wishlist.popular_displays.drop,limit=1] remove wishlist.popular_displays.drop
kill @s
