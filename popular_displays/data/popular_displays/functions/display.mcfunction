# display.mcfunction

data modify storage wishlist:args tags set value { \
        Tags:[wishlist.popular_displays], \
        view_range:0.5f, \
        transformation:{ \
          scale:[0.5f,0.5f,0.5f] \
        } \
      }

data modify storage wishlist:args tags.item set from entity @s Item

execute align xyz \
        positioned ~0.5 ~0.25 ~0.5 \
        summon minecraft:item_display \
        run data modify entity @s {} merge from storage wishlist:args tags

kill @s

advancement grant @a[predicate=popular_displays:hold_displayer,distance=..4] \
        only wishlist:create_item_display
