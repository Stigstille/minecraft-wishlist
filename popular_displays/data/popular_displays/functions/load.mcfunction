# load.mcfunction

scoreboard objectives add wishlist.popular_displays.trigger trigger

data modify storage wishlist:args all_chatter append value \
        "If you get your hands on a Display Book you'll be able to do some insane decoration"
data modify storage wishlist:args all_chatter append value \
        "Remember to lock your displays so people won't accidentally drop them!"

function popular_displays:tick
