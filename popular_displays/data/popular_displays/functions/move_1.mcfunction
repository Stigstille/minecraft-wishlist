# function popular_displays:move_1
execute if score facing wishlist.vars matches 0 \
        run tp @s ~0.0625 ~ ~
execute if score facing wishlist.vars matches 1 \
        run tp @s ~-0.0625 ~ ~
execute if score facing wishlist.vars matches 2 \
        run tp @s ~ ~0.0625 ~
execute if score facing wishlist.vars matches 3 \
        run tp @s ~ ~-0.0625 ~
execute if score facing wishlist.vars matches 4 \
        run tp @s ~ ~ ~0.0625
execute if score facing wishlist.vars matches 5 \
        run tp @s ~ ~ ~-0.0625

