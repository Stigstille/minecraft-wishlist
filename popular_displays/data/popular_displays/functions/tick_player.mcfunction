# tick_player.mcfunction

# This HAS to be a separate function; if we do this in the same command that
# selects the player's positions, the same item may be displayed at once by two
# different players, effectively duping them

execute as @e[type=item,distance=..4,predicate=popular_displays:on_ground] \
        at @s \
        run function popular_displays:display

