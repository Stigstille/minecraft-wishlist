
scoreboard players set amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_boots \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_chestplate \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_helmet \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_cold_leggings \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_boots \
        run scoreboard players remove amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_chestplate \
        run scoreboard players remove amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_helmet \
        run scoreboard players remove amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_warm_leggings \
        run scoreboard players remove amount wishlist.vars 1
data modify storage wishlist:args args set value {type:"minecraft:freeze"}
execute store result storage wishlist:args args.amount int 1 \
        run scoreboard players get amount wishlist.vars
function extreme_biomes:damage with storage wishlist:args args

execute if score amount wishlist.vars matches 1.. \
        run effect give @s minecraft:slowness 10 2 true

execute if block ~ ~ ~ minecraft:water run damage @s 1 minecraft:freeze


