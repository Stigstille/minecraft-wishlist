
schedule function extreme_biomes:tick_1s 1s

execute as @a[gamemode=!creative,gamemode=!spectator] \
        at @s \
        if biome ~ ~ ~ wishlist:supercool \
        run function extreme_biomes:supercool

execute as @a[gamemode=!creative,gamemode=!spectator] \
        at @s \
        if biome ~ ~ ~ wishlist:superhot \
        run function extreme_biomes:superhot

