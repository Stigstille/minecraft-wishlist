
scoreboard players set amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_boots \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_chestplate \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_helmet \
        run scoreboard players add amount wishlist.vars 1
execute if predicate extreme_biomes:wearing_leggings \
        run scoreboard players add amount wishlist.vars 1
data modify storage wishlist:args args set value {type:"minecraft:on_fire"}
execute store result storage wishlist:args args.amount int 1 \
        run scoreboard players get amount wishlist.vars
function extreme_biomes:damage with storage wishlist:args args

