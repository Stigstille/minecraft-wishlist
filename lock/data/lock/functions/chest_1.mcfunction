
# lock:chest_1
data modify storage wishlist:args lock set from block ~ ~ ~ Lock
execute store success score fail wishlist.vars \
        run data modify storage wishlist:args lock \
        set from entity @s SelectedItem.tag.wishlist_magic.args.code
execute unless score fail wishlist.vars matches 0 run return run function lock:locked
data remove block ~ ~ ~ Lock
playsound wishlist:lock.unlock block @a ~ ~ ~
title @s actionbar "Unlocked"


