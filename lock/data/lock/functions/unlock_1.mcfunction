
# lock:unlock_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
$execute store success score go wishlist.vars run tag @s remove $(code)
execute if score go wishlist.vars matches 0 run return fail
playsound wishlist:lock.unlock block @a ~ ~ ~
kill @s
scoreboard players set done wishlist.vars 1


