
advancement revoke @s only lock:unlock
data modify storage wishlist:args args set from entity @s SelectedItem.tag.wishlist_magic.args

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        at @s \
        run function lock:unlock_1 with storage wishlist:args args

execute if score done wishlist.vars matches 0 \
        run function lock:locked
execute unless score done wishlist.vars matches 0 \
        run title @s actionbar "Unlocked"
