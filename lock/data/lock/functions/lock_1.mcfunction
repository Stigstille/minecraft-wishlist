
# lock:lock_1
execute if score steps wishlist.vars matches 0 run return fail
execute if block ~ ~ ~ #wishlist:lockable \
        run return run function lock:chest
execute if block ~ ~ ~ #minecraft:trapdoors \
        run return run function lock:lock_2 {height:1}
execute if block ~ ~ ~ #minecraft:doors[half=lower] \
        run return run function lock:lock_2 {height:2}
execute if block ~ ~ ~ #minecraft:doors[half=upper] \
        positioned ~ ~-1 ~ \
        run return run function lock:lock_2 {height:2}
execute unless block ~ ~ ~ #wishlist:non_full run return fail
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run function lock:lock_1
