#!/bin/python3

# book.py - Create a loot table that generates a book from its JSON pages
#
# Usage: book.py
#       [-a|--author <author>]
#       [-t|--title <title>]
#       [-x|--extra <extra>]
#       <pages...>

import argparse
import json

parser = argparse.ArgumentParser(
        prog='book.py',
        description='Create minecraft loot table for a book from its pages',
        epilog='You must provide at least one page, and it must be valid JSON')
parser.add_argument('-a', '--author')
parser.add_argument('-t', '--title')
parser.add_argument('-x', '--extra')
parser.add_argument('pages', metavar='P', type=str, nargs='+', help='a page for the book')
args = parser.parse_args()

nbt = {
    "pages": [],
    "CustomModelData":271832,
    }

if args.title:
    nbt["title"] = args.title
if args.author:
    nbt["author"] = args.author
if args.extra:
    nbt |= json.loads(args.extra)

for p in args.pages:
    with open(p, 'r') as f:
        nbt["pages"].append(json.dumps(json.load(f)))

print(json.dumps({
    "pools": [
        {
            "rolls": 1,
            "entries": [
                {
                    "type": "minecraft:item",
                    "name": "minecraft:written_book",
                    "functions": [
                        {
                            "function": "minecraft:set_nbt",
                            "tag": json.dumps(nbt)
                            }
                        ]
                    }
                ]
            }
        ]
    }, indent=2))

