# rand.mcfunction

# Implements simple LCG for outputting a random number sequence

scoreboard players operation rand_seed wishlist.vars *= rand_multiplier wishlist.vars
scoreboard players operation rand_seed wishlist.vars += rand_increment wishlist.vars
scoreboard players operation rand_seed wishlist.vars %= rand_modulus wishlist.vars
scoreboard players operation rand wishlist.vars = rand_seed wishlist.vars

