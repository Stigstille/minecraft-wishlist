# impl/tick_1s.mcfunction

schedule function wishlist:impl/tick_1s 1s
execute at @a as @e[tag=wishlist.cash_register,distance=..8] \
        run function wishlist:cash_register/tick
execute at @a as @e[tag=wishlist.globe,distance=..8] \
        run function wishlist:globe/tick

# Grant wake up advancement to project contributors
# QuirkyQuixote
tag b51be5e1-572c-3a8e-9802-4d2b53d1970a add wishlist.contributor
# Xn0AK0xn
tag 3abea40c-657f-34b8-9163-618844710e72 add wishlist.contributor
# EssenseOfLuna
tag 7ac5a819-98d0-3a41-96c1-38ad0643ff7b add wishlist.contributor
# pipar_
tag pipar_ add wishlist.contributor
# Stigstille
tag Stigstille add wishlist.contributor

advancement grant @a[tag=wishlist.contributor] only wishlist:wake_up
