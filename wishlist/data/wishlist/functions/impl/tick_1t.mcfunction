# tick.mcfunction - update system

schedule function wishlist:impl/tick_1t 1t

execute as @a[scores={wishlist.source=1..}] \
        run function wishlist:impl/link_source
scoreboard players reset @a wishlist.source
scoreboard players enable @a wishlist.source

execute as @a unless score @s wishlist.join matches 0 \
        run function #wishlist:login
scoreboard players set @a wishlist.join 0

execute as @a run function wishlist:impl/sneak
execute as @a at @s run function wishlist:impl/store_position

execute as @a[scores={wishlist.used_carrot_on_a_stick=1..}] \
        at @s \
        run function #wishlist:used_carrot_on_a_stick
scoreboard players reset * wishlist.used_carrot_on_a_stick

