# impl/store_position.mcfunction

data modify storage wishlist:args p set from entity @s {}

execute store result score @s wishlist.x run data get storage wishlist:args p.Pos[0]
execute store result score @s wishlist.y run data get storage wishlist:args p.Pos[1]
execute store result score @s wishlist.z run data get storage wishlist:args p.Pos[2]

execute store result score @s wishlist.yaw run data get storage wishlist:args p.Rotation[0]
execute store result score @s wishlist.pitch run data get storage wishlist:args p.Rotation[1]

execute store result score x wishlist.vars run data get storage wishlist:args p.Motion[0] 100
execute store result score y wishlist.vars run data get storage wishlist:args p.Motion[1] 100
execute store result score z wishlist.vars run data get storage wishlist:args p.Motion[2] 100

tag @s remove wishlist.still
execute if score x wishlist.vars matches 0 \
        if score y wishlist.vars matches -8 \
        if score z wishlist.vars matches 0 \
        run tag @s add wishlist.still

scoreboard players set @s wishlist.facing 5
execute if score @s wishlist.yaw matches -135..-45 \
        run scoreboard players set @s wishlist.facing 0
execute if score @s wishlist.yaw matches -45..45 \
        run scoreboard players set @s wishlist.facing 4
execute if score @s wishlist.yaw matches 45..135 \
        run scoreboard players set @s wishlist.facing 1
execute if score @s wishlist.pitch matches ..-45 \
        run scoreboard players set @s wishlist.facing 2
execute if score @s wishlist.pitch matches 45.. \
        run scoreboard players set @s wishlist.facing 3

scoreboard players set @s wishlist.dimension -1
execute if dimension minecraft:overworld \
        run scoreboard players set @s wishlist.dimension 0
execute if dimension minecraft:the_nether \
        run scoreboard players set @s wishlist.dimension 1
execute if dimension minecraft:the_end \
        run scoreboard players set @s wishlist.dimension 2
execute if dimension wishlist:sketch \
        run scoreboard players set @s wishlist.dimension 3
execute if dimension wishlist:ocean \
        run scoreboard players set @s wishlist.dimension 4
execute if dimension wishlist:sky \
        run scoreboard players set @s wishlist.dimension 5

scoreboard players set 512 wishlist.vars 512
scoreboard players operation @s wishlist.rx = @s wishlist.x
scoreboard players operation @s wishlist.rx /= 512 wishlist.vars
scoreboard players operation @s wishlist.rz = @s wishlist.z
scoreboard players operation @s wishlist.rz /= 512 wishlist.vars

scoreboard players set 16 wishlist.vars 16
scoreboard players operation @s wishlist.cx = @s wishlist.x
scoreboard players operation @s wishlist.cx /= 16 wishlist.vars
scoreboard players operation @s wishlist.cy = @s wishlist.y
scoreboard players operation @s wishlist.cy /= 16 wishlist.vars
scoreboard players operation @s wishlist.cz = @s wishlist.z
scoreboard players operation @s wishlist.cz /= 16 wishlist.vars

execute store success score ground0 wishlist.vars if entity @s[tag=wishlist.ground]
execute store result score ground1 wishlist.vars run data get storage wishlist:args p.OnGround

tag @s remove wishlist.ground
tag @s remove wishlist.jump
tag @s remove wishlist.land

execute if score ground1 wishlist.vars matches 1 \
        run tag @s add wishlist.ground

execute if score ground0 wishlist.vars matches 1 \
        if score ground1 wishlist.vars matches 0 \
        run tag @s add wishlist.jump

execute if score ground0 wishlist.vars matches 0 \
        if score ground1 wishlist.vars matches 1 \
        run tag @s add wishlist.land

