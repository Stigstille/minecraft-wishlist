# give_player_id.mcfunction - give player an id

execute if score @s wishlist.playerId matches 1.. run return fail
scoreboard players operation @s wishlist.playerId = $next wishlist.playerId
scoreboard players add $next wishlist.playerId 1
