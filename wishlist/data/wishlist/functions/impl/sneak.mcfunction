# sneak.mcfunction

execute if entity @s[tag=!wishlist.sneaking] \
        unless score @s wishlist.last_sneak_time = @s wishlist.sneak_time \
        at @s \
        run function #wishlist:crouch
execute if score @s wishlist.last_sneak_time = @s wishlist.sneak_time \
        run tag @s remove wishlist.sneaking
execute unless score @s wishlist.last_sneak_time = @s wishlist.sneak_time \
        run tag @s add wishlist.sneaking
scoreboard players operation @s wishlist.last_sneak_time = @s wishlist.sneak_time
