# impl/link_source.mcfunction

tellraw @s [ \
        { \
          "translate":"This server uses %s", \
          "with":[ \
            { \
              "text":"Quirky's Wishlist", \
              "color":"blue", \
              "underlined":true, \
              "clickEvent":{ \
                "action":"open_url", \
                "value":"https://gitlab.com/QuirkyQuixote/minecraft-wishlist" \
                }, \
              "hoverEvent":{ \
                "action":"show_text", \
                "value":"https://gitlab.com/QuirkyQuixote/minecraft-wishlist" \
                } \
              } \
            ] \
          } \
        ]

