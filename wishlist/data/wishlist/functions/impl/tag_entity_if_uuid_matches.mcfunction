# tag_entity_if_uuid.mcfunction - tag entity if it's UUID matches
# @s: entity to test
# wishlist.vars uuid: UUID to search for

data modify storage wishlist:args buf set from storage wishlist:args uuid
execute store success score go wishlist.vars \
        run data modify storage wishlist:args buf set from entity @s UUID
execute if score go wishlist.vars matches 0 \
        run tag @s add wishlist.match

