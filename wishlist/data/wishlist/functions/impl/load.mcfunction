# load.mcfunction - initialize wishlist

scoreboard objectives add wishlist.vars dummy
scoreboard objectives add wishlist.playerId dummy
scoreboard objectives add wishlist.employer dummy
scoreboard objectives add wishlist.source trigger
scoreboard objectives add wishlist.join minecraft.custom:minecraft.leave_game
scoreboard objectives add wishlist.sneak_time minecraft.custom:minecraft.sneak_time
scoreboard objectives add wishlist.last_sneak_time dummy
scoreboard objectives add wishlist.x dummy
scoreboard objectives add wishlist.y dummy
scoreboard objectives add wishlist.z dummy
scoreboard objectives add wishlist.yaw dummy
scoreboard objectives add wishlist.pitch dummy
scoreboard objectives add wishlist.facing dummy
scoreboard objectives add wishlist.dimension dummy
scoreboard objectives add wishlist.rx dummy
scoreboard objectives add wishlist.rz dummy
scoreboard objectives add wishlist.cx dummy
scoreboard objectives add wishlist.cy dummy
scoreboard objectives add wishlist.cz dummy
scoreboard objectives add wishlist.used_carrot_on_a_stick minecraft.used:minecraft.carrot_on_a_stick

execute unless score $next wishlist.playerId matches 0.. \
        run scoreboard players set $next wishlist.playerId 1

scoreboard players set rand_multiplier wishlist.vars 1103515245
scoreboard players set rand_increment wishlist.vars 12345
scoreboard players set rand_modulus wishlist.vars 1073741824

scoreboard players set 0 wishlist.vars 0
scoreboard players set 1 wishlist.vars 1
scoreboard players set 2 wishlist.vars 2
scoreboard players set 3 wishlist.vars 3
scoreboard players set 4 wishlist.vars 4
scoreboard players set 5 wishlist.vars 5
scoreboard players set 6 wishlist.vars 6
scoreboard players set 7 wishlist.vars 7
scoreboard players set 8 wishlist.vars 8
scoreboard players set 9 wishlist.vars 9
scoreboard players set 10 wishlist.vars 10
scoreboard players set 100 wishlist.vars 100

data modify storage wishlist:args all_chatter set value [ \
        "Hi!", \
        "Howdy?", \
        "Did you break the weather in the upworld?", \
        "Oh, sorry", \
        "Never mind me", \
        "I told them but they don't listen!", \
        "Step carefully", \
        "Nice duds!", \
        "Ohh, I like your face; very chic", \
        "Hm?", \
        "You know? I actually like this weather", \
        "Ready?", \
        "They're here, they're there, they're everywhere", \
        "I always come back here; it's nice", \
        "Good judgement comes from bad experiences", \
        "Bad experiences come from bad judgement", \
        "I could do this all day", \
        "Do you think even the worst person can change?", \
        "It's a beautiful day outside", \
        "Chickens are clucking", \
        "Parrots are talking", \
        "Flowers are blooming", \
        "Would you rather fight a hundred chicken-sized zombies or a zombie-sized chicken?", \
        "This is the way; dunno where it goes…", \
        "I hate sand: you touch it and it falls on you and you suffocate", \
        "My feet are sore", \
        "So what's a 'pesky bird', anyway?", \
        "I heard it was the person in the chicken costume", \
        "Trans rights!" \
        ]

function wishlist:impl/tick_1t
function wishlist:impl/tick_1s
