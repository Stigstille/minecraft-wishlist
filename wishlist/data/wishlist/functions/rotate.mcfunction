# rotate.mcfunction

# in: storage wishlist:args elems - list to rotate
# in: score pivot wishlist.vars - element that becomes the first

execute if score pivot wishlist.vars matches 1.. run function wishlist:rotate_1
