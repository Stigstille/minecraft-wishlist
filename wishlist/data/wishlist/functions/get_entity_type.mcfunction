
summon interaction ~ ~ ~ {Tags:[wishlist.temporary]}
ride @s mount @e[tag=wishlist.temporary,distance=..1,limit=1]
$data modify $(type) $(root) $(path) \
        set from entity @e[tag=wishlist.temporary,distance=..1,limit=1] Passengers[0].id
kill @e[tag=wishlist.temporary,distance=..1,limit=1]
