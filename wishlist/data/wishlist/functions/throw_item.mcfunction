# throw_item.mcfunction - Summon item at position

summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:stick",Count:1},Tags:[wishlist.thrown_item]}
data modify entity @e[tag=wishlist.thrown_item,limit=1] Item set from storage wishlist:args Item
tag @e[tag=wishlist.thrown_item] remove wishlist.thrown_item
