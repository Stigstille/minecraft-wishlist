# find_player_by_uuid.mcfunction - tag player if it's UUID matches
# wishlist.vars uuid: UUID to search for

tag @e remove wishlist.match
execute as @a run function wishlist:impl/tag_entity_if_uuid_matches
