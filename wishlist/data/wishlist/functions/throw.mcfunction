
execute positioned ^ ^ ^0 summon marker run function wishlist:throw_1
execute positioned ^ ^ ^1 summon marker run function wishlist:throw_2

data modify storage wishlist:args args.data.Owner set from entity @s UUID
data modify storage wishlist:args args.data.Motion set value [0.0,0.0,0.0]
$execute store result storage wishlist:args args.data.Motion[0] double $(speed) \
        run scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
$execute store result storage wishlist:args args.data.Motion[1] double $(speed) \
        run scoreboard players operation y1 wishlist.vars -= y0 wishlist.vars
$execute store result storage wishlist:args args.data.Motion[2] double $(speed) \
        run scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars

function wishlist:throw_3 with storage wishlist:args args
