# wishlist:restore_inventory_2
$execute unless data storage wishlist:inventory $(id)[{Slot:$(src)b}] \
        run return run item replace entity @s $(dst) with minecraft:air
$data modify storage wishlist:args args set value {id:$(id),dst:$(dst),tag:{}}
$data modify storage wishlist:args args merge from storage wishlist:inventory $(id)[{Slot:$(src)b}]
function wishlist:restore_inventory_3 with storage wishlist:args args
