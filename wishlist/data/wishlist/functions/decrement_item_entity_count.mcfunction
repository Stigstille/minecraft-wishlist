# decrement_item_entity_count.mcfunction - remove one item from a stack
# @s: entity of type item whose Count will be reduced by 1

execute store result score count wishlist.vars \
        run data get entity @s Item.Count
execute store result entity @s Item.Count byte 1 \
        run scoreboard players remove count wishlist.vars 1
