# max_stack_size.mcfunction - determine maximum stack size for item
#
# input storage wishlist:args Item: item to be stacked
# output score msize wishlist.: maximum size of a stack of Item

setblock 0 0 0 minecraft:barrel
data modify block 0 0 0 Items append from storage wishlist:args Item
item modify block 0 0 0 container.0 wishlist:full_stack
execute store result score msize wishlist.vars \
        run data get block 0 0 0 Items[0].Count
execute unless data block 0 0 0 Items[0].Count \
        run scoreboard players set msize wishlist.vars 999999
setblock 0 0 0 minecraft:air

