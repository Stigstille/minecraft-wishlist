# skin_villager_by_biome.mcfunction

execute if predicate wishlist:spawn_desert_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:desert"}}
execute if predicate wishlist:spawn_jungle_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:jungle"}}
execute if predicate wishlist:spawn_savanna_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:savanna"}}
execute if predicate wishlist:spawn_snow_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:snow"}}
execute if predicate wishlist:spawn_swamp_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:swamp"}}
execute if predicate wishlist:spawn_taiga_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:taiga"}}
execute if predicate wishlist:spawn_plains_villagers \
        run data merge entity @s {VillagerData:{type:"minecraft:plains"}}

