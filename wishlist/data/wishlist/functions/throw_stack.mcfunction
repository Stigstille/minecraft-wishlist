# throw_stack.mcfunction - throw stack of items
#
# input storage wishlist:args Item: The item to be thrown.
# input score ssize wishlist:args: max number of items to throw
# output score ssize wishlist:args: actual number of items thrown
#
# at most ssize items will be thrown, but no more than the maximum amount of
# items like Item that can be naturally stacked

function wishlist:max_stack_size
scoreboard players operation ssize wishlist.vars < msize wishlist.vars
execute store result storage wishlist:args Item.Count byte 1 \
        run scoreboard players get ssize wishlist.vars
function wishlist:throw_item
