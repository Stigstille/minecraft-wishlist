# find_entity_by_uuid.mcfunction - tag entity if it's UUID matches
# wishlist.vars uuid: UUID to search for

tag @e remove wishlist.match
execute as @e run function wishlist:impl/tag_entity_if_uuid_matches
