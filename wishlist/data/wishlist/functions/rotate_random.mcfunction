# rotate_random.mcfunction

# in: storage wishlist:args elems

function wishlist:rand
execute store result score length wishlist.vars \
        run data get storage wishlist:args elems
scoreboard players operation pivot wishlist.vars = rand wishlist.vars
scoreboard players operation pivot wishlist.vars %= length wishlist.vars
function wishlist:rotate
