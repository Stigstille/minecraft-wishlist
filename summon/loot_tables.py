#!/bin/python3

# generate loot tables to obtain npcs

import json
import re

pattern = re.compile(r'\s+')

villager_types = {
        "nitwit":"Nitwit",
        "armorer":"Armorer",
        "butcher":"Butcher",
        "cartographer":"Cartographer",
        "cleric":"Cleric",
        "farmer":"Farmer",
        "fisherman":"Fisher",
        "fletcher":"Fletcher",
        "leatherworker":"Leatherworker",
        "librarian":"Librarian",
        "mason":"Mason",
        "shepherd":"Shepherd",
        "toolsmith":"Toolsmith",
        "weaponsmith":"Weaponsmith"
        }

illager_types = {
        "pillager":"Pillager",
        "vindicator":"Vindicator",
        "evoker":"Evoker",
        "witch":"Witch",
        "illusioner":"Illusioner",
        }

piglin_types = {
        "piglin":"Piglin",
        "piglin_brute":"Piglin Brute",
        }

def make_loot_table(fname, id, dname, data):
    nbt = """{
        CustomModelData:271870,
        EntityTag:{
            Item:{
                id:"minecraft:item_frame",
                Count:1b,
                tag:{
                    wishlist_summon:{
                        id:"minecraft:"""+id+"""",
                        data:{
                            NoAI:1b,
                            Invulnerable:1b,
                            Tags:[wishlist.look_alive],
                            ArmorItems:[
                                {},
                                {},
                                {},
                                {
                                    id:"minecraft:stone_button",
                                    Count:1b
                                    }
                                ],
                            ArmorDropChances:[0f,0f,0f,0f],
                            """+data+"""
                            }
                        }
                    }
                }
            }
        }"""

    with open('data/summon/loot_tables/summon_'+fname+'.json', 'w') as f:
        f.write(json.dumps({
            "pools": [
                {
                    "rolls": 1,
                    "entries": [
                        {
                            "type": "minecraft:item",
                            "name": "minecraft:item_frame",
                            "functions": [
                                {
                                    "function": "minecraft:set_name",
                                    "name": {
                                        "translate":"Summon "+dname,
                                        "italic": False
                                        }
                                    },
                                {
                                    "function": "minecraft:set_lore",
                                    "lore": [
                                        {
                                            "text":"minecraft:"+id,
                                            "italic":False,
                                            "color":"gray"
                                            }
                                        ]
                                    },
                                {
                                    "function": "minecraft:set_nbt",
                                    "tag":re.sub(pattern, '', nbt)
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }, indent=2))

for i in villager_types:
    make_loot_table(i, "villager", villager_types[i], """
        VillagerData:{
            Offers:{
                Recipes:[]
                },
            profession:"minecraft:"""+i+""""
            }
        """)

for i in illager_types:
    make_loot_table(i, i, illager_types[i], """
        PersistenceRequired:1b,
        CanJoinRaids:0b
        """)

for i in piglin_types:
    make_loot_table(i, i, piglin_types[i], """
        PersistenceRequired:1b,
        IsImmuneToZombification:1b
        """)

make_loot_table("enderman", "enderman", "Enderfolk", """
    PersistenceRequired:1b,
    """)

make_loot_table("villager_child", "villager", "Villager Child", """
    Age:-2000000000
    """)

make_loot_table("piglin_child", "piglin", "Piglin Child", """
    PersistenceRequired:1b,
    IsImmuneToZombification:1b,
    IsBaby:1b
    """)

