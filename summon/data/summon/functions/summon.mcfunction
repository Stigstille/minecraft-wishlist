# summon/summon

advancement revoke @s only summon:summon
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
execute as @e[type=item_frame,distance=..8] \
        if data entity @s Item.tag.wishlist_summon \
        at @s \
        align xyz \
        positioned ~0.5 ~ ~0.5 \
        run function summon:summon_1


