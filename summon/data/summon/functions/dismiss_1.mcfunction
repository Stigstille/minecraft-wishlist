
# summon:dismiss_1
execute if score done wishlist.vars matches 1 run return fail
scoreboard players set done wishlist.vars 0

function wishlist:recall
scoreboard players set employer wishlist.vars -1
execute store result score employer wishlist.vars \
        run data get storage wishlist:args memories.employer
execute unless score employer wishlist.vars = playerId wishlist.vars \
        run return fail

data modify storage wishlist:args summon set value {}
execute at @s run function wishlist:get_entity_type \
        {type:"storage",root:"wishlist:args",path:"summon.id"}
data modify storage wishlist:args summon.data set from entity @s {}
data remove storage wishlist:args summon.data.UUID
data remove storage wishlist:args summon.data.Pos
data remove storage wishlist:args summon.data.Motion
data remove storage wishlist:args summon.data.Rotation
function summon:spawn with storage wishlist:args summon

function wishlist:dismiss

scoreboard players set done wishlist.vars 1
