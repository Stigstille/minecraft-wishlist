
$data modify storage wishlist:args summon set value { id:"$(id)", data:$(data) }
$execute unless data storage wishlist:args summon.data.CustomName \
        run data modify storage wishlist:args lore set value [ \
                '{"text":"$(id)","color":"gray","italic":false}' \
        ]
execute if data storage wishlist:args summon.data.CustomName \
        run function summon:lore
loot give @s loot summon:summon
