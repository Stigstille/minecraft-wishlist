
# summon/summon_1

data modify storage wishlist:args args set from entity @s Item.tag.wishlist_summon
execute unless data storage wishlist:args args.data.ArmorItems \
        run data modify storage wishlist:args args.data.ArmorItems \
        set value [{},{},{},{}]
execute unless data storage wishlist:args args.data.ArmorItems[3].id \
        run data modify storage wishlist:args args.data.ArmorItems[3] \
        set value {id:"minecraft:stone_button",Count:1b,tag:{CustomModelData:999999}}
execute store result storage wishlist:args args.data.ArmorItems[3].tag.employer int 1 \
        run scoreboard players get playerId wishlist.vars
function summon:summon_2 with storage wishlist:args args
kill @s


