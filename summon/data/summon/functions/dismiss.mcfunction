

# summon:dismiss
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
scoreboard players set done wishlist.vars -1
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=!minecraft:player,type=!minecraft:item,distance=..2,sort=nearest] \
        run function summon:dismiss_1
execute if score done wishlist.vars matches -1 \
        run title @s actionbar "No entities in range"
execute if score done wishlist.vars matches 0 \
        run title @s actionbar "That's not one of your employees"
