
$execute in $(d) \
        positioned $(x) $(y) $(z) \
        run summon minecraft:interaction ~ ~ ~ { \
                Tags:[wishlist.dungeon.pot,wishlist.dungeon.passive], \
                Passengers: [ \
                        { \
                                id: "minecraft:item_display", \
                                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                                item: { \
                                        id: "minecraft:carrot_on_a_stick", \
                                        Count: 1b, \
                                        tag:{ \
                                                CustomModelData:271838, \
                                                item: $(item) \
                                        } \
                                } \
                        } \
                ] \
        }
