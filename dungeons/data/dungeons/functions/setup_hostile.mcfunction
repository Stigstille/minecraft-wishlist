
scoreboard players operation @s wishlist.currentRun = currentRun wishlist.vars

data modify storage wishlist:args args set value []

scoreboard players operation n wishlist.vars = enchants wishlist.vars
scoreboard players operation n wishlist.vars *= 10 wishlist.vars
scoreboard players add n wishlist.vars 100

# grant 10% increase to health for every enchant in play
data modify storage wishlist:args args set value \
        {attr: "minecraft:generic.max_health"}
execute store result score base wishlist.vars \
        run attribute @s minecraft:generic.max_health base get
scoreboard players operation base wishlist.vars *= n wishlist.vars
execute store result storage wishlist:args args.base int 0.01 \
        run scoreboard players get base wishlist.vars
function dungeons:setup_hostile_1 with storage wishlist:args args
execute store result entity @s Health float 1 \
        run attribute @s minecraft:generic.max_health get

# grant 10% increase to damage for every enchant in play
data modify storage wishlist:args args set value \
        {attr: "minecraft:generic.attack_damage"}
execute store result score base wishlist.vars \
        run attribute @s minecraft:generic.attack_damage base get
scoreboard players operation base wishlist.vars *= n wishlist.vars
execute store result storage wishlist:args args.base int 0.01 \
        run scoreboard players get base wishlist.vars
function dungeons:setup_hostile_1 with storage wishlist:args args

execute if data entity @s HandItems[0].tag.wishlist.run \
        run data modify entity @s HandItems[0].tag.wishlist.run \
        set from storage wishlist:dungeon currentRun
execute if data entity @s HandItems[1].tag.wishlist.run \
        run data modify entity @s HandItems[1].tag.wishlist.run \
        set from storage wishlist:dungeon currentRun

# Prevent zombies from calling reinforcements
attribute @s minecraft:zombie.spawn_reinforcements base set 0
