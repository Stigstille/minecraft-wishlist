
$execute if entity @a[scores={wishlist.currentRoom=$(room)},predicate=dungeons:editing] \
        run return run function dungeons:update_debug {room:$(room)}
$execute store result score state wishlist.vars \
        run data get storage wishlist:dungeon rooms[{id:$(room)}].state
$execute if score state wishlist.vars matches 1 \
        run return run function dungeons:update_ready {room:$(room)}
$execute if score state wishlist.vars matches 2 \
        run return run function dungeons:update_open {room:$(room)}
$execute if score state wishlist.vars matches 3 \
        run return run function dungeons:update_clear {room:$(room)}
