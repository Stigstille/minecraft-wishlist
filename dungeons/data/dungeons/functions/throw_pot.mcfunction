
data modify storage wishlist:args args set value { \
        id: "minecraft:snowball", \
        data: { \
                Item:{ \
                        id:"minecraft:carrot_on_a_stick", \
                        Count:1b, \
                        tag:{CustomModelData:271839} \
                }, \
                PickupDelay: 10000, \
                Tags:[wishlist.dungeon.pot], \
        } \
}

execute unless entity @s[tag=wishlist.sneaking] \
        anchored eyes \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0010}
execute if entity @s[tag=wishlist.sneaking] \
        anchored feet \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0010}

item modify entity @s weapon.mainhand wishlist:consume_one

