
function dungeons:room_args
execute unless data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No chunk to remove", \
                "color":"yellow" \
        }
execute store result storage wishlist:dungeon args.room int 1 \
        run scoreboard players get @s wishlist.selectedRoom
function dungeons:remove_chunk_1 with storage wishlist:dungeon args
