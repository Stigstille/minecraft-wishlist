
$execute in $(d) positioned $(x) $(y) $(z) unless entity @a[distance=..64] \
        run return fail

$data modify storage wishlist:dungeon rooms[{id:$(id)}].state set value 1
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Room %s ready!", \
        "with": ["$(id)"], \
        "color": "yellow" \
}

