
$data remove storage wishlist:dungeon chunks."$(chunk)"
$tellraw @s { \
        "translate":"[dungeons] Removed chunk %s from room %s", \
        "with": ["$(chunk)","$(room)"], \
        "color":"yellow" \
}

$data remove storage wishlist:dungeon rooms[{id:$(room)}].chunks.$(chunk)
$execute store result score n wishlist.vars \
        run data get storage wishlist:dungeon rooms[{id:$(room)}].chunks
execute unless score n wishlist.vars matches 0 run return fail
$data remove storage wishlist:dungeon rooms[{id:$(room)}]
$tellraw @s { \
        "translate":"[dungeons] Removed room %s", \
        "with": ["$(room)"], \
        "color":"yellow" \
}
