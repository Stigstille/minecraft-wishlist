
execute unless data storage wishlist:dungeon buf[0] run return 0
execute store result score state wishlist.vars \
        run data get storage wishlist:dungeon buf[0].state
execute if score state wishlist.vars matches 0 \
        run function dungeons:update_empty with storage wishlist:dungeon buf[0]
data remove storage wishlist:dungeon buf[0]
function dungeons:ready_rooms
