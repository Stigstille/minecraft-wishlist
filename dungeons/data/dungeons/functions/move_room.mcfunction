
function dungeons:room_args
execute unless data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to move", \
                "color":"yellow" \
        }
execute store result storage wishlist:dungeon args.x int 1 \
        run scoreboard players get @s wishlist.x
execute store result storage wishlist:dungeon args.y int 1 \
        run scoreboard players get @s wishlist.y
execute store result storage wishlist:dungeon args.z int 1 \
        run scoreboard players get @s wishlist.z
data modify storage wishlist:dungeon args.d set from entity @s Dimension
function dungeons:move_room_1 with storage wishlist:dungeon args

