
scoreboard players set acc wishlist.vars 0
$execute as @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:update_health
$execute store result bossbar dungeons:$(room) value \
        run scoreboard players get acc wishlist.vars

$execute if entity @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run return fail

$execute as @e[tag=wishlist.dungeon.mob_door,scores={wishlist.currentRoom=$(room)}] \
        at @s \
        run function dungeons:unlock_2

$execute if data storage wishlist:dungeon rooms[{id:$(room)}].boss \
        run function dungeons:setdown_boss

$data modify storage wishlist:dungeon rooms[{id:$(room)}].state set value 3
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Room %s clear!", \
        "with": ["$(room)"], \
        "color": "yellow" \
}

