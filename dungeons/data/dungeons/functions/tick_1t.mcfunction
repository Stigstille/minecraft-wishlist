
# Update current room for all players.
# if the value changes, force a room update
scoreboard players set updateRooms wishlist.vars 0
execute as @a at @s run function dungeons:update_player
execute unless score updateRooms wishlist.vars matches 0 \
        run function dungeons:update_rooms

# Reschedule for next tick
schedule function dungeons:tick_1t 1t
