

execute if block ~ ~ ~ #minecraft:doors[facing=east,half=lower,hinge=left] \
        positioned ~0.093750 ~ ~1 run function dungeons:add_door_x
execute if block ~ ~ ~ #minecraft:doors[facing=east,half=lower,hinge=right] \
        positioned ~0.093750 ~ ~ run function dungeons:add_door_x
execute if block ~ ~ ~ #minecraft:doors[facing=east,half=upper,hinge=left] \
        positioned ~0.093750 ~-1 ~1 run function dungeons:add_door_x
execute if block ~ ~ ~ #minecraft:doors[facing=east,half=upper,hinge=right] \
        positioned ~0.093750 ~-1 ~ run function dungeons:add_door_x

execute if block ~ ~ ~ #minecraft:doors[facing=north,half=lower,hinge=left] \
        positioned ~1 ~ ~0.9062 run function dungeons:add_door_z
execute if block ~ ~ ~ #minecraft:doors[facing=north,half=lower,hinge=right] \
        positioned ~ ~ ~0.9062 run function dungeons:add_door_z
execute if block ~ ~ ~ #minecraft:doors[facing=north,half=upper,hinge=left] \
        positioned ~1 ~-1 ~0.9062 run function dungeons:add_door_z
execute if block ~ ~ ~ #minecraft:doors[facing=north,half=upper,hinge=right] \
        positioned ~ ~-1 ~0.9062 run function dungeons:add_door_z

execute if block ~ ~ ~ #minecraft:doors[facing=west,half=lower,hinge=left] \
        positioned ~0.9062 ~ ~ run function dungeons:add_door_x
execute if block ~ ~ ~ #minecraft:doors[facing=west,half=lower,hinge=right] \
        positioned ~0.9062 ~ ~1 run function dungeons:add_door_x
execute if block ~ ~ ~ #minecraft:doors[facing=west,half=upper,hinge=left] \
        positioned ~0.9062 ~-1 ~ run function dungeons:add_door_x
execute if block ~ ~ ~ #minecraft:doors[facing=west,half=upper,hinge=right] \
        positioned ~0.9062 ~-1 ~1 run function dungeons:add_door_x

execute if block ~ ~ ~ #minecraft:doors[facing=south,half=lower,hinge=left] \
        positioned ~ ~ ~0.09375 run function dungeons:add_door_z
execute if block ~ ~ ~ #minecraft:doors[facing=south,half=lower,hinge=right] \
        positioned ~1 ~ ~0.09375 run function dungeons:add_door_z
execute if block ~ ~ ~ #minecraft:doors[facing=south,half=upper,hinge=left] \
        positioned ~ ~-1 ~0.09375 run function dungeons:add_door_z
execute if block ~ ~ ~ #minecraft:doors[facing=south,half=upper,hinge=right] \
        positioned ~1 ~-1 ~0.09375 run function dungeons:add_door_z
