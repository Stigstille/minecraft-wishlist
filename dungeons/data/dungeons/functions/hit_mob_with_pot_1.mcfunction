
execute on attacker unless entity @s[tag=wishlist.match] run return fail
execute store result score tmp wishlist.vars run data get entity @s HurtTime
execute if score tmp wishlist.vars matches 0 run return fail

execute facing entity @a[tag=wishlist.match,limit=1] feet \
        positioned ^ ^ ^0 \
        summon marker \
        run function wishlist:throw_1
execute facing entity @a[tag=wishlist.match,limit=1] feet \
        positioned ^ ^ ^-1 \
        summon marker \
        run function wishlist:throw_2

execute store result entity @s Motion[0] double 0.0025 \
        run scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
data modify entity @s Motion[1] set value 0.2
execute store result entity @s Motion[2] double 0.0025 \
        run scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars

damage @s 16 minecraft:falling_block
