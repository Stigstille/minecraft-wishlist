
function dungeons:room_args
execute unless data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to pop", \
                "color":"yellow" \
        }
function dungeons:pop_loot_1 with storage wishlist:dungeon args
tellraw @s { \
        "translate":"[dungeons] Popped %s loot", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}


