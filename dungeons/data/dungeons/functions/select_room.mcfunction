
function dungeons:room_args
execute unless data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to select", \
                "color":"yellow" \
        }
execute store result score @s wishlist.selectedRoom run data get storage wishlist:dungeon args.room
tellraw @s { \
        "translate":"[dungeons] Selected room %s", \
        "with":[{"score":{"name":"@s","objective":"wishlist.selectedRoom"}}], \
        "color":"yellow" \
}
