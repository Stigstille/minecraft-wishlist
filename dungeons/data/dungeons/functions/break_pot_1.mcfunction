
#break_pot_1
execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
function dungeons:empty_pot
execute at @s run particle minecraft:item \
                minecraft:carrot_on_a_stick{CustomModelData:271838} \
                ~ ~ ~ 0.3 0.3 0.3 0.05 40
execute at @s run playsound minecraft:block.decorated_pot.shatter block @a ~ ~ ~
function wishlist:dismiss



