
$scoreboard players set n wishlist.vars $(room)
execute store result storage wishlist:dungeon nextRoom int 1 \
        run scoreboard players add n wishlist.vars 1

$data modify storage wishlist:dungeon rooms append value {id:$(room),d:"$(d)",x:$(x),y:$(y),z:$(z)}
$scoreboard players set @s wishlist.selectedRoom $(room)
$tellraw @s { \
        "translate":"[dungeons] Created room %s", \
        "with": ["$(room)"], \
        "color":"yellow" \
}

