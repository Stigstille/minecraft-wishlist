
function dungeons:room_args
execute if data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] Chunk is already in a room", \
                "color":"yellow" \
        }
execute store result storage wishlist:dungeon args.room int 1 \
        run scoreboard players get @s wishlist.selectedRoom
function dungeons:add_chunk_1 with storage wishlist:dungeon args
