
data modify storage wishlist:args args set value { \
        id: "minecraft:item", \
        data: { \
                Item:{ \
                        id:"minecraft:carrot_on_a_stick", \
                        Count:1b, \
                        tag:{CustomModelData:271837} \
                }, \
                PickupDelay: 10000, \
                Tags:[wishlist.bomb], \
        } \
}

execute unless entity @s[tag=wishlist.sneaking] \
        anchored eyes \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0006}
execute if entity @s[tag=wishlist.sneaking] \
        anchored feet \
        positioned ^ ^ ^ \
        run function wishlist:throw {speed:0.0006}

playsound minecraft:entity.tnt.primed player @s ~ ~ ~

item modify entity @s weapon.mainhand wishlist:consume_one
