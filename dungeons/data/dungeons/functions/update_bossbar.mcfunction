
$bossbar set dungeons:$(room) players @a[scores={wishlist.currentRoom=$(room)}]
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Update bossbar for room %s", \
        "with": ["$(room)"], \
        "color": "yellow" \
}
