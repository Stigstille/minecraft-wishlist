
scoreboard objectives add wishlist.currentRun dummy
scoreboard objectives add wishlist.selectedRoom dummy
scoreboard objectives add wishlist.currentRoom dummy
scoreboard objectives add wishlist.lastRoom dummy

execute unless data storage wishlist:dungeon chunks \
        run data modify storage wishlist:dungeon chunks set value {}
execute unless data storage wishlist:dungeon rooms \
        run data modify storage wishlist:dungeon rooms set value []
execute unless data storage wishlist:dungeon nextRoom \
        run data modify storage wishlist:dungeon nextRoom set value 0

data modify storage wishlist:args all_chatter append value \
        "It's mildly inconvenient to go alone"
data modify storage wishlist:args all_chatter append value \
        "Dungeon maintenance is hard: they change the locks when no one's looking"
data modify storage wishlist:args all_chatter append value \
        "A small key will open one door; a big one will open all of them"
data modify storage wishlist:args all_chatter append value \
        "The dynamite thing is scary, but also scarce"

data modify storage wishlist:dungeon music_track set value \
        {track:"wishlist:music.dungeon",ttl:242,volume:0.25,pitch:1,minVolume:0}

data modify storage wishlist:dungeon boss_track set value \
        {track:"wishlist:music.boss",ttl:97,volume:0.25,pitch:1,minVolume:0}

data modify storage wishlist:custom_blocks statue1 set value { \
        interaction: { \
                Tags: [], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271866, \
                      display:{Name:'{"text":"Statue","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

data modify storage wishlist:custom_blocks statue2 set value { \
        interaction: { \
                Tags: [], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271867, \
                      display:{Name:'{"text":"Statue","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

function dungeons:tick_1s
function dungeons:tick_5t
function dungeons:tick_1t
