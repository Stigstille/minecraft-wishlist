
execute store result score age wishlist.vars run data get entity @s Age
execute if score age wishlist.vars matches ..40 run return fail
particle minecraft:explosion_emitter ~ ~ ~
playsound minecraft:entity.generic.explode block @a ~ ~ ~
execute as @e[distance=0..1] run damage @s 128 minecraft:explosion by @p
execute as @e[distance=1..2] run damage @s 96 minecraft:explosion by @p
execute as @e[distance=2..3] run damage @s 64 minecraft:explosion by @p
execute as @e[distance=3..4] run damage @s 32 minecraft:explosion by @p
execute as @e[tag=wishlist.dungeon.crack,distance=..4] at @s run function dungeons:open_crack
kill @s
