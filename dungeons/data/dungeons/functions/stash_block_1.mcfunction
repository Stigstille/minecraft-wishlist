
data modify storage wishlist:dungeon blocks[-1].d \
        set from entity @p Dimension
execute store result storage wishlist:dungeon blocks[-1].x int 1 \
        run data get entity @s Pos[0]
execute store result storage wishlist:dungeon blocks[-1].y int 1 \
        run data get entity @s Pos[1]
execute store result storage wishlist:dungeon blocks[-1].z int 1 \
        run data get entity @s Pos[2]
kill @s
