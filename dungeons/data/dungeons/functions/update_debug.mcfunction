
scoreboard players operation room wishlist.vars = @s wishlist.currentRoom
execute as @e[tag=wishlist.dungeon.hostile] \
        if score @s wishlist.currentRoom = room wishlist.vars \
        run function wishlist:dismiss
execute as @e[tag=wishlist.dungeon.passive] \
        if score @s wishlist.currentRoom = room wishlist.vars \
        run function wishlist:dismiss

