
execute positioned ~-1 ~-1 ~-1 run function dungeons:stash_block
execute positioned ~-1 ~-1 ~ run function dungeons:stash_block
execute positioned ~-1 ~ ~-1 run function dungeons:stash_block
execute positioned ~-1 ~ ~ run function dungeons:stash_block
execute positioned ~ ~-1 ~-1 run function dungeons:stash_block
execute positioned ~ ~-1 ~ run function dungeons:stash_block
execute positioned ~ ~ ~-1 run function dungeons:stash_block
execute positioned ~ ~ ~ run function dungeons:stash_block
fill ~-1 ~-1 ~-1 ~ ~ ~ minecraft:air
kill @s

tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Door opened!", \
        "color": "yellow" \
}

execute as @a[scores={wishlist.currentRoom=0..}] \
        at @s \
        run playsound wishlist:dungeon.open_door block @s ~ ~ ~ 0.5
