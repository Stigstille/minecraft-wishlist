
# Top level update function works as follows

# if no players are inside the dungeon, and the dungeon is not marked as empty,
# mark it as empty, remove all mobs, and set every room state to zero
execute unless entity @a[gamemode=!spectator,scores={wishlist.currentRoom=0..}] \
        if score emptyDungeon wishlist.vars matches 0 \
        run function dungeons:clear_rooms

# traverse every room; if its state is zero, and there's a player in a 64-block
# radius of the center, the room goes from empty to ready
data modify storage wishlist:dungeon buf set from storage wishlist:dungeon rooms
function dungeons:ready_rooms

# Attempt to pop blocks
data modify storage wishlist:dungeon unpopped set value []
function dungeons:pop_blocks
data modify storage wishlist:dungeon blocks1 set from storage wishlist:dungeon unpopped

# if there are players inside the dungeon, update the rooms where they are
execute if entity @a[gamemode=!spectator,scores={wishlist.currentRoom=0..}] \
        run function dungeons:update_rooms

# update bombs
execute at @a \
        as @e[tag=wishlist.bomb,distance=..32] \
        at @s \
        run function dungeons:update_bomb

# inside the dungeon, dungeon music rules; outside, it stops
execute as @a[scores={wishlist.currentRoom=-1}] run function looping_music:stop

# Clear mobs that are not from this run
execute as @e[tag=wishlist.dungeon.hostile] \
        unless score @s wishlist.currentRun = currentRun wishlist.vars \
        run function wishlist:dismiss
execute as @e[tag=wishlist.dungeon.passive] \
        unless score @s wishlist.currentRun = currentRun wishlist.vars \
        run function wishlist:dismiss

# reschedule function for next second
schedule function dungeons:tick_1s 1s

