
execute if score @s wishlist.facing matches 0 \
        positioned ~1 ~0.5 ~0.5 \
        align xyz \
        run function dungeons:add_crack_3
execute if score @s wishlist.facing matches 1 \
        positioned ~ ~0.5 ~0.5 \
        align xyz \
        run function dungeons:add_crack_3
execute if score @s wishlist.facing matches 4 \
        positioned ~0.5 ~0.5 ~1 \
        align xyz \
        run function dungeons:add_crack_3
execute if score @s wishlist.facing matches 5 \
        positioned ~0.5 ~0.5 ~ \
        align xyz \
        run function dungeons:add_crack_3
