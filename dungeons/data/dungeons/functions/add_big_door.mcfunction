
data modify storage wishlist:dungeon door set value { \
        id:"minecraft:interaction", \
        y:0.75, \
        data: { \
                Tags:[wishlist.dungeon.big_door], \
                width: 0.5, \
                height: 0.5, \
                Passengers: [ \
                        { \
                                id:"minecraft:item_display", \
                                transformation:[1.0f,0f,0f,0f,0f,1.0f,0f,0.25f,0f,0f,1.0f,0f,0f,0f,0f,1f], \
                                item: { \
                                        id: "minecraft:item_frame", \
                                        Count:1b, \
                                        tag:{CustomModelData:271869} \
                                } \
                        } \
                ] \
        } \
}

function dungeons:add_door
