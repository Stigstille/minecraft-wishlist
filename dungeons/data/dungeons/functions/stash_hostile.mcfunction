
function dungeons:room_args
execute unless data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=!player,distance=..4,sort=nearest,limit=1] \
        run function dungeons:stash_hostile_1 with storage wishlist:dungeon args
execute unless score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Stashed %s as mob", \
                "with": [{"nbt":"stash.id","storage":"wishlist:args"}], \
                "color":"yellow" \
        }
execute if score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Nothing to stash", \
                "color":"yellow" \
        }

