
execute positioned ~-1 ~-2 ~-1 run function dungeons:stash_block
execute positioned ~-1 ~-2 ~ run function dungeons:stash_block
execute positioned ~ ~-2 ~-1 run function dungeons:stash_block
execute positioned ~ ~-2 ~ run function dungeons:stash_block
fill ~-1 ~-2 ~-1 ~ ~-2 ~ redstone_torch
function wishlist:dismiss
scoreboard players set done wishlist.vars 1

tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Door opened!", \
        "color": "yellow" \
}

execute as @a[scores={wishlist.currentRoom=0..}] \
        at @s \
        run playsound wishlist:dungeon.open_door block @s ~ ~ ~ 0.5
