
scoreboard players set done wishlist.vars 0

data modify storage wishlist:args item set from entity @s SelectedItem
execute store result score run wishlist.vars \
        run data get storage wishlist:args item.tag.wishlist.run
execute unless score run wishlist.vars = currentRun wishlist.vars \
        run return run title @s actionbar {"text":"That's an old key and it won't work anymore"}

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        at @s \
        if data entity @s interaction \
        run function dungeons:unlock_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"text":"Unknown Error"}
