
function dungeons:room_args
execute unless data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] No room to stash", \
                "color":"yellow" \
        }
scoreboard players set done wishlist.vars 0
$data modify storage wishlist:dungeon loot set value {table:"$(table)"}
data modify storage wishlist:dungeon loot.d set from entity @p Dimension
execute store result storage wishlist:dungeon loot.x int 1 \
        run scoreboard players get @s wishlist.x
execute store result storage wishlist:dungeon loot.y int 1 \
        run scoreboard players get @s wishlist.y
execute store result storage wishlist:dungeon loot.z int 1 \
        run scoreboard players get @s wishlist.z
function dungeons:stash_loot_1 with storage wishlist:dungeon args
execute unless score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Stashed %s", \
                "with": [{"nbt":"loot.table","storage":"wishlist:dungeon"}], \
                "color":"yellow" \
        }
execute if score done wishlist.vars matches 0 \
        run tellraw @s { \
                "translate":"[dungeons] Nothing to stash", \
                "color":"yellow" \
        }


