
# Set state of every room in the dungeon to empty
execute if data storage wishlist:dungeon rooms[{}] \
        run data modify storage wishlist:dungeon rooms[{}].state set value 0

# Push every block that was removed during this run to the list of blocks that
# should be restored
data modify storage wishlist:dungeon blocks1 append from storage wishlist:dungeon blocks[]
data modify storage wishlist:dungeon blocks set value []

# Increase magic number for current run.
execute store result storage wishlist:dungeon currentRun int 1 \
        run scoreboard players add currentRun wishlist.vars 1
scoreboard players set emptyDungeon wishlist.vars 1

# Set down boss flag
scoreboard players set dungeonBoss wishlist.vars 0

# Log
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Dungeon reset!", \
        "color":"yellow" \
}
