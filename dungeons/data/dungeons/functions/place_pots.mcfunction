
execute unless data storage wishlist:dungeon pop[0] run return 0
execute if data storage wishlist:dungeon pop[0].table \
        run function dungeons:place_pot_1 with storage wishlist:dungeon pop[0]
execute if data storage wishlist:dungeon pop[0].item \
        run function dungeons:place_pot_2 with storage wishlist:dungeon pop[0]
data remove storage wishlist:dungeon pop[0]
scoreboard players add count wishlist.vars 1
function dungeons:place_pots
