
$execute at @s run summon minecraft:item ~ ~1 ~ { \
        Item:$(item), \
        Motion: [0.0,0.2,0.0], \
        PickupDelay:40s, \
        Tags:[wishlist.dungeon.treasure] \
}

execute as @a[scores={wishlist.currentRoom=0..}] \
        at @s \
        run playsound wishlist:dungeon.find_treasure block @s ~ ~ ~ 0.5
