
function dungeons:room_args
execute if data storage wishlist:dungeon args.room \
        run return run tellraw @s { \
                "translate":"[dungeons] Room already exists", \
                "color":"yellow" \
        }
execute store result storage wishlist:dungeon args.x int 1 \
        run scoreboard players get @s wishlist.x
execute store result storage wishlist:dungeon args.y int 1 \
        run scoreboard players get @s wishlist.y
execute store result storage wishlist:dungeon args.z int 1 \
        run scoreboard players get @s wishlist.z
data modify storage wishlist:dungeon args.d set from entity @s Dimension
data modify storage wishlist:dungeon args.room set from storage wishlist:dungeon nextRoom
function dungeons:add_room_1 with storage wishlist:dungeon args
function dungeons:add_chunk_1 with storage wishlist:dungeon args

