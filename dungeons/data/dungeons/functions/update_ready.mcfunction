
$data modify storage wishlist:dungeon rooms[{id:$(room)}].state set value 2
$tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Room %s open!", \
        "with": ["$(room)"], \
        "color": "yellow" \
}

$data modify storage wishlist:dungeon pop set from storage wishlist:dungeon rooms[{id:$(room)}].hostiles
execute if data storage wishlist:dungeon pop[0] \
        run data modify storage wishlist:dungeon pop[] merge value { \
                NoAI:0b, \
                DeathLootTable:"dungeons:mob_drop", \
                PersistenceRequired:1b \
        }
execute if data storage wishlist:dungeon pop[0] \
        run data modify storage wishlist:dungeon pop[].Tags append value wishlist.dungeon.hostile
function dungeons:pop
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Placed %s hostiles", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

$data modify storage wishlist:dungeon pop set from storage wishlist:dungeon rooms[{id:$(room)}].passives
execute if data storage wishlist:dungeon pop[0] \
        run data modify storage wishlist:dungeon pop[] merge value { \
                DeathLootTable:"minecraft:empty", \
                PersistenceRequired:1b \
        }
execute if data storage wishlist:dungeon pop[0] \
        run data modify storage wishlist:dungeon pop[].Tags append value wishlist.dungeon.passive
function dungeons:pop
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Placed %s passives", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

$data modify storage wishlist:dungeon pop set from storage wishlist:dungeon rooms[{id:$(room)}].loot
scoreboard players set count wishlist.vars 0
function dungeons:place_pots
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Placed %s pots", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}], \
        "color":"yellow" \
}

$execute as @e[tag=wishlist.dungeon.hostile] \
        unless score @s wishlist.currentRoom matches 0.. \
        run scoreboard players set @s wishlist.currentRoom $(room)
$execute as @e[tag=wishlist.dungeon.passive] \
        unless score @s wishlist.currentRoom matches 0.. \
        run scoreboard players set @s wishlist.currentRoom $(room)

data modify storage wishlist:args enchants set value []
execute as @a[scores={wishlist.currentRoom=0..}] \
        run data modify storage wishlist:args enchants \
        append from entity @s Inventory[{}].tag.Enchantments[].lvl
execute store result score enchants wishlist.vars \
        run data get storage wishlist:args enchants
tellraw @a[tag=wishlist.debug] { \
        "translate":"[dungeons] Adjusting hostiles by %s", \
        "with": [{"score":{"name":"enchants","objective":"wishlist.vars"}}], \
        "color": "yellow" \
}
$execute as @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:setup_hostile

$execute as @e[tag=wishlist.dungeon.passive,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:setup_passive

scoreboard players set acc wishlist.vars 0
$execute as @e[tag=wishlist.dungeon.hostile,scores={wishlist.currentRoom=$(room)}] \
        run function dungeons:update_max_health
$execute store result bossbar dungeons:$(room) max \
        run scoreboard players get acc wishlist.vars

$execute if data storage wishlist:dungeon rooms[{id:$(room)}].boss \
        run function dungeons:setup_boss


