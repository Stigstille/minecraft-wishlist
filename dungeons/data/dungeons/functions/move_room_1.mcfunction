
$data modify storage wishlist:dungeon rooms[{id:$(room)}] merge value {d:"$(d)",x:$(x),y:$(y),z:$(z)}
$tellraw @s { \
        "translate":"[dungeons] Moved room %s", \
        "with": ["$(room)"], \
        "color":"yellow" \
}

