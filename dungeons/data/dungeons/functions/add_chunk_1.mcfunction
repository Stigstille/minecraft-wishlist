
$data modify storage wishlist:dungeon chunks."$(chunk)" set value $(room)
$data modify storage wishlist:dungeon rooms[{id:$(room)}].chunks."$(chunk)" set value {}
$tellraw @s { \
        "translate":"[dungeons] Added chunk %s to room %s", \
        "with": ["$(chunk)","$(room)"], \
        "color":"yellow" \
}

