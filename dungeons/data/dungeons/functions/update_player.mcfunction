
scoreboard players operation @s wishlist.lastRoom = @s wishlist.currentRoom
scoreboard players set @s wishlist.currentRoom -1
function dungeons:room_args
execute if data storage wishlist:dungeon args.room \
        store result score @s wishlist.currentRoom \
        run data get storage wishlist:dungeon args.room
execute if score @s wishlist.lastRoom = @s wishlist.currentRoom run return 0

# everything below here happens only if the player changed room
execute if entity @s[gamemode=!spectator] \
        run scoreboard players set updateRooms wishlist.vars 1

execute if score @s wishlist.currentRoom matches 0.. \
        if score @s wishlist.lastRoom matches -1 \
        unless score dungeonBoss wishlist.vars matches 1 \
        run function looping_music:play with storage wishlist:dungeon music_track
execute if score @s wishlist.currentRoom matches 0.. \
        if score @s wishlist.lastRoom matches -1 \
        if score dungeonBoss wishlist.vars matches 1 \
        run function looping_music:play with storage wishlist:dungeon boss_track

data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.room int 1 \
        run scoreboard players get @s wishlist.currentRoom
function dungeons:update_bossbar with storage wishlist:args args
execute store result storage wishlist:args args.room int 1 \
        run scoreboard players get @s wishlist.lastRoom
function dungeons:update_bossbar with storage wishlist:args args

