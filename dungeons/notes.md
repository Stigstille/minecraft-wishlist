

States
------

### Reset

* Every room enters this state when no players are in the dungeon

### Ready

* Reset rooms enter this state when at least one player in the dungeon is close
  enough to them that the chunks will be loaded.
* All pots are placed
* All blocks are restored

### Open

* Ready rooms enter this state when at least one player in the dungeon is
  inside them
* All hostile and passive mobs are spawned

### Clear

* Open rooms enter this state when no hostile mobs associated with them remain

