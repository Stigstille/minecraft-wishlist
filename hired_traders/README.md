Hired Traders
=============

Provides villagers that will sell up to nine kinds of items at the price
configured by their employer, get their stock from their employer, and give
them the money they earn.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Summon](../summon) for summon contract
* Requires [Wishlist Resources](../resources) for media and text translations.
* Recommends [Look Alive](../look_alive) to make the traders look more lively.

Usage
-----

You can get a ![contract] contract to summon a hired trader with
the `hired_traders:contract` loot table.

The hired trader should be summoned on top of a barrel, whose contents are used
to configure their trades.

By default the hired trader looks like a vanilla villager wearing a green
eyeshade.

The signal for the trader to update their trades is for their employer to
crouch near them. When this happens, a number of checks are performed (see
below).

After updating their inventory, the trader will give their employer a full list
of what earnings and stocks they hold.

### Configuring Trades

Hired traders are configured with the same UI as VanillaTweaks Custom Traders.

Each column of the barrel's inventory is used to configure a different trade,
first and second row are the price for the items in the third row.

Note that when a trade changes, the trader will forcefully give their employer
all the money and stock they were holding for the previous trade.

### Restocking and Payment

When updating, the trader will take items around their feet to restock their
trades. There's no coded limit to the amount of stock a hired trader can store
beyond what a Java Integer can hold.

The trader will react to their employer holding specific items by performing
specific actions:

* if the employer holds paper, they'll give back their earnings;
* if the employer holds a chest, they'll give back unsold stock;

Overhead
--------

Updates every tick:

* checks every player to see if they have crouched this frame
    * checks every entity in a 5-block radius around the player to see if it is
      a hired trader to be updated.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Reasonable Hiring Practices: hire an employee
    * Grand Opening: restock an employee's trade
        * Cash Handling Procedures: take payment from an employee
        * Inventory Management: destock employee's trade

[contract]: resources/assets/wishlist/textures/item/contract.png ""
