# pay_employer_b.mcfunction - give coins back to employer

data modify storage wishlist:args Item set from storage ht_stor recipe.buyB
scoreboard players operation ssize wishlist.vars = earnb wishlist.vars
function hired_traders:throw_stack_at_employer
scoreboard players operation earnb wishlist.vars -= ssize wishlist.vars
advancement grant @a[tag=wishlist.employer] only wishlist:get_paid_by_employee
