# pay_employer_b.mcfunction

execute if score earnb wishlist.vars matches 1.. \
        run function hired_traders:pay_employer_b_1
execute if score earnb wishlist.vars matches 1.. \
        run function hired_traders:pay_employer_b
execute if score earnb wishlist.vars matches ..-1 \
        run tellraw @a[tag=wishlist.employer] { \
          "translate":"wishlist.hired_traders.negative_earnb", \
          "with":[{"selector":"@s"}], \
          "color":"dark_gray" \
        }
