# copy_recipe.mcfunction - copy recipe from barrel

data remove storage ht_stor new_recipe.buy.Slot
data remove storage ht_stor new_recipe.buyB.Slot
data remove storage ht_stor new_recipe.sell.Slot

data modify storage ht_stor old set value {}
data modify storage ht_stor old.buy set from storage ht_stor recipe.buy
data modify storage ht_stor old.buyB set from storage ht_stor recipe.buyB
data modify storage ht_stor old.sell set from storage ht_stor recipe.sell

data modify storage ht_stor new set value {}
data modify storage ht_stor new.buy set from storage ht_stor new_recipe.buy
data modify storage ht_stor new.buyB set from storage ht_stor new_recipe.buyB
data modify storage ht_stor new.sell set from storage ht_stor new_recipe.sell

##############################################################################
# Slots containing air seem to be unstable, their Count and tag tags can change
# in response to players trading with the villager, so we remove them from the
# recipe because they should not trigger changes anyway

data modify storage ht_stor buf set value "minecraft:air"
execute store success score go wishlist.vars \
        run data modify storage ht_stor buf set from storage ht_stor old.buy.id
execute if score go wishlist.vars matches 0 \
        run data remove storage ht_stor old.buy

data modify storage ht_stor buf set value "minecraft:air"
execute store success score go wishlist.vars \
        run data modify storage ht_stor buf set from storage ht_stor old.buyB.id
execute if score go wishlist.vars matches 0 \
        run data remove storage ht_stor old.buyB

data modify storage ht_stor buf set value "minecraft:air"
execute store success score go wishlist.vars \
        run data modify storage ht_stor buf set from storage ht_stor old.sell.id
execute if score go wishlist.vars matches 0 \
        run data remove storage ht_stor old.sell

data modify storage ht_stor buf set value "minecraft:air"
execute store success score go wishlist.vars \
        run data modify storage ht_stor buf set from storage ht_stor new.buy.id
execute if score go wishlist.vars matches 0 \
        run data remove storage ht_stor new.buy

data modify storage ht_stor buf set value "minecraft:air"
execute store success score go wishlist.vars \
        run data modify storage ht_stor buf set from storage ht_stor new.buyB.id
execute if score go wishlist.vars matches 0 \
        run data remove storage ht_stor new.buyB

data modify storage ht_stor buf set value "minecraft:air"
execute store success score go wishlist.vars \
        run data modify storage ht_stor buf set from storage ht_stor new.sell.id
execute if score go wishlist.vars matches 0 \
        run data remove storage ht_stor new.sell

data modify storage ht_stor buf set from storage ht_stor old

scoreboard players set go wishlist.vars 0
execute store success score go wishlist.vars \
        run data modify storage ht_stor buf set from storage ht_stor new
execute if score go wishlist.vars matches 1.. \
        run function hired_traders:copy_recipe_1

