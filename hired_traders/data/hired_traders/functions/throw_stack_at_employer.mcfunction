# throw_stack_at_employer.mcfunction - Summon item towards entity

execute at @a[tag=wishlist.employer] \
        facing entity @s feet \
        rotated ~ 0 \
        run execute positioned ^ ^1.25 ^1 run function wishlist:throw_stack
