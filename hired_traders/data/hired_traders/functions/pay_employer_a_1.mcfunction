# pay_employer_a.mcfunction - give coins back to employer

data modify storage wishlist:args Item set from storage ht_stor recipe.buy
scoreboard players operation ssize wishlist.vars = earna wishlist.vars
function hired_traders:throw_stack_at_employer
scoreboard players operation earna wishlist.vars -= ssize wishlist.vars
advancement grant @a[tag=wishlist.employer] only wishlist:get_paid_by_employee
