# destock.mcfunction - empty trader's inventory

data modify storage wishlist:args Item set from storage ht_stor recipe.sell
scoreboard players operation ssize wishlist.vars = stock wishlist.vars
function hired_traders:throw_stack_at_employer
scoreboard players operation stock wishlist.vars -= ssize wishlist.vars
advancement grant @a[tag=wishlist.employer] only wishlist:destock_employee
