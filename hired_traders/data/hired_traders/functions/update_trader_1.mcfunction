# update_trader.mcfunction - update hired trader's trader

# Determine what the employer wants this tick
scoreboard players set request_destock wishlist.vars 0
execute store success score request_destock wishlist.vars \
        if entity @a[tag=wishlist.employer,limit=1,nbt={SelectedItem:{id:"minecraft:chest"}}]
scoreboard players set request_payment wishlist.vars 0
execute store success score request_payment wishlist.vars \
        if entity @a[tag=wishlist.employer,limit=1,nbt={SelectedItem:{id:"minecraft:paper"}}]

# Update recipe #0
data modify storage ht_stor recipe set from entity @s Offers.Recipes[0]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:0b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:9b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:18b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[0]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[0] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[0] set from storage ht_stor extra

# Update recipe #1
data modify storage ht_stor recipe set from entity @s Offers.Recipes[1]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:1b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:10b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:19b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[1]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[1] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[1] set from storage ht_stor extra

# Update recipe #2
data modify storage ht_stor recipe set from entity @s Offers.Recipes[2]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:2b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:11b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:20b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[2]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[2] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[2] set from storage ht_stor extra

# Update recipe #3
data modify storage ht_stor recipe set from entity @s Offers.Recipes[3]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:3b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:12b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:21b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[3]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[3] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[3] set from storage ht_stor extra

# Update recipe #4
data modify storage ht_stor recipe set from entity @s Offers.Recipes[4]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:4b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:13b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:22b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[4]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[4] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[4] set from storage ht_stor extra

# Update recipe #5
data modify storage ht_stor recipe set from entity @s Offers.Recipes[5]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:5b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:14b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:23b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[5]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[5] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[5] set from storage ht_stor extra

# Update recipe #6
data modify storage ht_stor recipe set from entity @s Offers.Recipes[6]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:6b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:15b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:24b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[6]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[6] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[6] set from storage ht_stor extra

# Update recipe #7
data modify storage ht_stor recipe set from entity @s Offers.Recipes[7]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:7b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:16b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:25b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[7]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[7] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[7] set from storage ht_stor extra

# Update recipe #8
data modify storage ht_stor recipe set from entity @s Offers.Recipes[8]
data modify storage ht_stor new_recipe set from storage ht_stor empty_recipe
data modify storage ht_stor new_recipe.buy set from block ~ ~-1 ~ Items[{Slot:8b}]
data modify storage ht_stor new_recipe.buyB set from block ~ ~-1 ~ Items[{Slot:17b}]
data modify storage ht_stor new_recipe.sell set from block ~ ~-1 ~ Items[{Slot:26b}]
data modify storage ht_stor extra set from entity @s HandItems[1].tag.extra[8]
function hired_traders:update_recipe
data modify entity @s Offers.Recipes[8] set from storage ht_stor recipe
data modify entity @s HandItems[1].tag.extra[8] set from storage ht_stor extra

# Print updated inventory
tellraw @a[tag=wishlist.employer] { \
        "translate":"wishlist.hired_traders.inventory_header", \
        "with":[{"selector":"@s"}], \
        "color":"dark_gray" \
        }
tellraw @a[tag=wishlist.employer] { \
        "translate":"wishlist.hired_traders.inventory_stock", \
        "with":[{"entity":"@s","nbt":"HandItems[1].tag.extra[{}].stock"}], \
        "color":"dark_gray" \
        }
tellraw @a[tag=wishlist.employer] { \
        "translate":"wishlist.hired_traders.inventory_earna", \
        "with":[{"entity":"@s","nbt":"HandItems[1].tag.extra[{}].earna"}], \
        "color":"dark_gray" \
        }
tellraw @a[tag=wishlist.employer] { \
        "translate":"wishlist.hired_traders.inventory_earnb", \
        "with":[{"entity":"@s","nbt":"HandItems[1].tag.extra[{}].earnb"}], \
        "color":"dark_gray" \
        }

# Grant employer advancement to employer
advancement grant @a[tag=wishlist.employer] only wishlist:hire_employee
