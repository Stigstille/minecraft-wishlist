
# report_player.mcfunction - report traders in range of player

# Tag employer
tag @s add wishlist.employer

# Check every hired trader around the employer
execute as @e[tag=wishlist.hired_trader,distance=..4] \
        at @s \
        run function hired_traders:report_trader

# Untag employer
tag @s remove wishlist.employer


