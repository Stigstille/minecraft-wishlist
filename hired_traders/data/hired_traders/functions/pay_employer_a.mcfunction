# pay_employer_a.mcfunction

execute if score earna wishlist.vars matches 1.. \
        run function hired_traders:pay_employer_a_1
execute if score earna wishlist.vars matches 1.. \
        run function hired_traders:pay_employer_a
execute if score earna wishlist.vars matches ..-1 \
        run tellraw @a[tag=wishlist.employer] { \
          "translate":"wishlist.hired_traders.negative_earna", \
          "with":[{"selector":"@s"}], \
          "color":"dark_gray" \
        }
