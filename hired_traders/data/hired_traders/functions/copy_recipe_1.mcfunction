# copy_recipe_1.mcfunction

##############################################################################
# Empty payment and stock to ensure nothing is lost

function hired_traders:pay_employer_a
function hired_traders:pay_employer_b
function hired_traders:destock

data modify storage ht_stor recipe set from storage ht_stor new_recipe

particle minecraft:happy_villager ~ ~1 ~ 0.2 1 0.2 0.5 10

execute store result score buy_count wishlist.vars \
        run data get storage ht_stor new_recipe.buy.Count
execute store result score buyb_count wishlist.vars \
        run data get storage ht_stor new_recipe.buyB.Count
execute store result score sell_count wishlist.vars \
        run data get storage ht_stor new_recipe.sell.Count

