
# update_player.mcfunction - update traders in range of player

# Tag employer
tag @s add wishlist.employer

# Check every hired trader around the employer
execute as @e[tag=wishlist.hired_trader,distance=..5] \
        at @s \
        run function hired_traders:update_trader

# Untag employer
tag @s remove wishlist.employer

