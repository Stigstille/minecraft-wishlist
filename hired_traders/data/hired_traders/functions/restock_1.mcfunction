
# restock.mcfunction - consume items on the floor to restock

particle minecraft:heart ~ ~1.5 ~ 0.5 0.5 0.5 0.1 1
execute store result score item_count wishlist.vars \
        run data get entity @s Item.Count
scoreboard players operation stock wishlist.vars += item_count wishlist.vars
kill @s
advancement grant @a[tag=wishlist.employer] only wishlist:restock_employee
