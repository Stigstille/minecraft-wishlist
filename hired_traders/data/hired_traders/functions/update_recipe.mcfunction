# update_recipe.mcfunction - update single trade in hired trader's shop

##############################################################################
# Update stock, earna, and earnb variables from villager's trade list

execute store result score buy_count wishlist.vars \
        run data get storage ht_stor recipe.buy.Count
execute store result score buyb_count wishlist.vars \
        run data get storage ht_stor recipe.buyB.Count
execute store result score sell_count wishlist.vars \
        run data get storage ht_stor recipe.sell.Count
execute store result score uses wishlist.vars \
        run data get storage ht_stor recipe.uses
execute store result score stock wishlist.vars \
        run data get storage ht_stor extra.stock
execute store result score earna wishlist.vars \
        run data get storage ht_stor extra.earna
execute store result score earnb wishlist.vars \
        run data get storage ht_stor extra.earnb

scoreboard players operation diff wishlist.vars = uses wishlist.vars
scoreboard players operation diff wishlist.vars *= sell_count wishlist.vars
scoreboard players operation stock wishlist.vars -= diff wishlist.vars

scoreboard players operation diff wishlist.vars = uses wishlist.vars
scoreboard players operation diff wishlist.vars *= buy_count wishlist.vars
scoreboard players operation earna wishlist.vars += diff wishlist.vars

scoreboard players operation diff wishlist.vars = uses wishlist.vars
scoreboard players operation diff wishlist.vars *= buyb_count wishlist.vars
scoreboard players operation earnb wishlist.vars += diff wishlist.vars

##############################################################################
# Attempt to update the recipe from the configuration on the barrel; this will
# force paymend and restocking if successful.

function hired_traders:copy_recipe

##############################################################################
# Find items in the floor whose id matches the villager trade, and add them to
# the villager's stock

execute if score request_destock wishlist.vars matches 0 \
        as @e[type=minecraft:item,distance=..2] \
        run function hired_traders:restock

##############################################################################
# If the trader is holding money for their employer, they give it to them

execute if score request_payment wishlist.vars matches 1 \
        run function hired_traders:pay_employer_a
execute if score request_payment wishlist.vars matches 1 \
        run function hired_traders:pay_employer_b

##############################################################################
# If the employer holds a chest, the trader will spill their inventory

execute if score request_destock wishlist.vars matches 1 \
        run function hired_traders:destock

##############################################################################
# Reset the maxUses variable from villager stock divided by the amount of stock
# that should be sold each transaction.

scoreboard players operation max_uses wishlist.vars = stock wishlist.vars
scoreboard players operation max_uses wishlist.vars /= sell_count wishlist.vars
execute store result storage ht_stor recipe.maxUses int 1 \
        run scoreboard players get max_uses wishlist.vars
data modify storage ht_stor recipe.uses set value 0

execute store result storage ht_stor extra.stock int 1 \
        run scoreboard players get stock wishlist.vars
execute store result storage ht_stor extra.earna int 1 \
        run scoreboard players get earna wishlist.vars
execute store result storage ht_stor extra.earnb int 1 \
        run scoreboard players get earnb wishlist.vars
