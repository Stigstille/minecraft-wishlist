
# hire.mcfunction - bind trader to nearest player


data merge entity @s {VillagerData:{level:5},Offers:{Recipes:[{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b},{maxUses:4,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b}]}}

execute store result score @s ht_uuid_0 run data get entity @p UUID[0]
execute store result score @s ht_uuid_1 run data get entity @p UUID[1]
execute store result score @s ht_uuid_2 run data get entity @p UUID[2]
execute store result score @s ht_uuid_3 run data get entity @p UUID[3]

tag @s remove trader_for_hire
tag @s add hired_trader
