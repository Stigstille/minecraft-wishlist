# report_trader.mcfunction - report hired trader's shop

scoreboard players set go wishlist.vars 0

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[0].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[0].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[1].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[1].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[2].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[2].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[3].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[3].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[4].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[4].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[5].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[5].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[6].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[6].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[7].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[7].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute store result score count wishlist.vars \
        run data get entity @s Offers.Recipes[8].sell.Count
execute store result score stock wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[8].stock
execute unless score count wishlist.vars matches 0 \
        if score stock wishlist.vars < count wishlist.vars \
        run scoreboard players set go wishlist.vars 1

execute if score go wishlist.vars matches 1 \
        run tellraw @a[tag=wishlist.employer] { \
                "translate":"wishlist.hired_traders.out_of_stock", \
                "with":[{"selector":"@s"}], \
                "color":"dark_gray" \
                }

scoreboard players set go wishlist.vars 0

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[0].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[1].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[2].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[3].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[4].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[5].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[6].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[7].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute store result score earna wishlist.vars \
        run data get entity @s HandItems[1].tag.extra[8].earna
execute unless score earna wishlist.vars matches 0 \
        run scoreboard players set go wishlist.vars 1

execute if score go wishlist.vars matches 1 \
        run tellraw @a[tag=wishlist.employer] { \
                "translate":"wishlist.hired_traders.has_money", \
                "with":[{"selector":"@s"}], \
                "color":"dark_gray" \
                }

