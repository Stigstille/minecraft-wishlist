# restock.mcfunction

data remove storage ht_stor tmp
data modify storage ht_stor tmp.id set from entity @s Item.id
data modify storage ht_stor tmp.tag set from entity @s Item.tag
data remove storage ht_stor tmp2
data modify storage ht_stor tmp2.id set from storage ht_stor recipe.sell.id
data modify storage ht_stor tmp2.tag set from storage ht_stor recipe.sell.tag
execute store success score same_item wishlist.vars \
        run data modify storage ht_stor tmp set from storage ht_stor tmp2
execute if score same_item wishlist.vars matches 0 \
        run function hired_traders:restock_1

