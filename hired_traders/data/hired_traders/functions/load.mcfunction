# load.mcfunction - initialize

data modify storage ht_stor empty_recipe set value { \
        demand:0, \
        maxUses:0, \
        priceMultiplier:0.0f, \
        rewardExp:0b, \
        specialPrice:0, \
        uses:0, \
        xp:0, \
        buy:{id:"minecraft:air",Count:1b,tag:{Charged:0b}}, \
        buyB:{id:"minecraft:air",Count:1b,tag:{Charged:0b}}, \
        sell:{id:"minecraft:air",Count:1b,tag:{Charged:0b}}, \
        "Paper.IgnoreDiscounts":1b \
        }

data modify storage wishlist:args all_chatter append value \
        "So, are you one of those job-creators we heard about all the time?"
data modify storage wishlist:args all_chatter append value \
        "You have your own shop? where's it?"

#function hired_traders:tick_1m
