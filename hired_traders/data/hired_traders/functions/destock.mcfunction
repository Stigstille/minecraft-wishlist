# destock.mcfunction - try to pay employer

execute if score stock wishlist.vars matches 1.. \
        run function hired_traders:destock_1
execute if score stock wishlist.vars matches 1.. \
        run function hired_traders:destock
execute if score stock wishlist.vars matches ..-1 \
        run tellraw @a[tag=wishlist.employer] { \
          "translate":"wishlist.hired_traders.negative_stock", \
          "with":[{"selector":"@s"}], \
          "color":"dark_gray" \
        }

