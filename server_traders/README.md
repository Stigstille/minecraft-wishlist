Server Traders
==============

Provides villagers that will sell up to nine kinds of items at the price
configured by a server administrator, with no stock limit.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Summon](../summon) for summon contract
* Requires [Wishlist Resources](../resources) for media and text translations.
* Recommends [Look Alive](../look_alive) to make the traders look more lively.

Usage
-----

You can get a ![contract] contract to summon a server trader with
the `server_traders:contract` loot table.

The server trader should be summoned on top of a barrel, whose contents are used
to configure their trades.

By default the hired trader looks like a vanilla villager wearing a green
eyeshade.

### Configuring Trades

Server traders are configured with the same UI as VanillaTweaks Custom Traders.

Each column of the barrel's inventory is used to configure a different trade,
first and second row are the price for the items in the third row.

A server trader will only update if a player in creative mode is at a distance
of at most four blocks from them.

### Differences With Vanilla Tweaks' Custom Traders

I coded this datapack in particular because the Vanilla Tweaks version had
issues that irked me a lot, including:

* Vanilla Tweaks resets the villager skin to a nitwit when the trades are
  reset, preventing you from having custom-skinned custom traders or forcing
  you to reset the skin every time you reset the trades.

* Vanilla Tweaks uses a chest to hold the trade configuration. Not only does
  this increment the number of chest entities in the world, but the traders are
  slightly buried on the floor.

* Wishlist adds the `Paper.IgnoreDiscounts` flag to the custom trades, so
  players have to pay the same amount for items (they are a public service,
  after all).

* If an item traded by the server trader has a tag of the form
  `wishlist.currency` equal to a number N, each one traded will modify the
  `vault` entry of the `wishlist.` scoreboard; items sold will come out of the
  vault, and items bought will add to the vault. If there isn't enough currency
  in the vault for the items to be sold, the trade will fail.

Overhead
--------

Updates when a creative player crouches via `#wishlist:crouch`:

* searches every entity in a 4-block radius around the player for server
  traders to update.

[contract]: resources/assets/wishlist/textures/item/contract.png ""
