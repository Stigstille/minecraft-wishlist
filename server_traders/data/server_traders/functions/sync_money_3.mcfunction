# sync_money_3.mcfunction

execute store result score amount wishlist.vars \
        run data get storage wishlist:args recipe.buy.Count
execute store result score worth wishlist.vars \
        run data get storage wishlist:args recipe.buy.tag.wishlist.currency
scoreboard players operation amount wishlist.vars *= uses wishlist.vars
scoreboard players operation amount wishlist.vars *= worth wishlist.vars
scoreboard players operation vault wishlist.vars += amount wishlist.vars
