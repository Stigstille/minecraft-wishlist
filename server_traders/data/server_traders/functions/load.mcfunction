# load.mcfunction

data modify storage wishlist:args all_chatter append value \
        "All those traders selling from the Server's Vault got it made"
data modify storage wishlist:args all_chatter append value \
        "Nah, I think that every single thing should be on a limited supply"

data modify storage wishlist:args empty_recipe set value {maxUses:2147483647,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b}
