# sync_money_1.mcfunction

scoreboard players set maxUses wishlist.vars 2000000000

execute store result score uses wishlist.vars \
        run data get storage wishlist:args recipe.uses
execute unless score uses wishlist.vars matches 0 \
        run function server_traders:sync_money_2
execute if data storage wishlist:args recipe.sell.tag.wishlist.currency \
        run function server_traders:sync_money_6

