# sync_money_6.mcfunction

execute store result score count wishlist.vars \
        run data get storage wishlist:args recipe.sell.Count
execute store result score worth wishlist.vars \
        run data get storage wishlist:args recipe.sell.tag.wishlist.currency
scoreboard players operation maxUses wishlist.vars = vault wishlist.vars
scoreboard players operation maxUses wishlist.vars /= count wishlist.vars
scoreboard players operation maxUses wishlist.vars /= worth wishlist.vars

