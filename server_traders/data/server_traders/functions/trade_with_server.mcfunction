# trade_with_server.mcfunction

execute as @e[type=#wishlist:trader,tag=wishlist.server_trader,distance=..6] \
        run function server_traders:sync_money

advancement revoke @s only server_traders:trade_with_server
