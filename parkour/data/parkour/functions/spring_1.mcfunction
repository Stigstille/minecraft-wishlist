
# parkour:spring_1
execute if entity @s[tag=wishlist.spring.vertical] \
        run function parkour:vertical
execute if entity @s[tag=wishlist.spring.diagonal] \
        run function parkour:diagonal
execute if entity @s[tag=wishlist.spring.yellow] \
        store result storage wishlist:args args.motion[0] double 0.0007 \
        run data get storage wishlist:args args.motion[0] 1000
execute if entity @s[tag=wishlist.spring.yellow] \
        store result storage wishlist:args args.motion[1] double 0.0007 \
        run data get storage wishlist:args args.motion[1] 1000
execute if entity @s[tag=wishlist.spring.yellow] \
        store result storage wishlist:args args.motion[2] double 0.0007 \
        run data get storage wishlist:args args.motion[2] 1000


