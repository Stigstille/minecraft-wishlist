
data modify storage wishlist:parkour diagonal_spring_motion set value [ \
        {motion:[0.0,0.5657,0.5657]}, \
        {motion:[-0.2165,0.5657,0.5226]}, \
        {motion:[-0.4,0.5657,0.4]}, \
        {motion:[-0.5226,0.5657,0.2165]}, \
        {motion:[-0.5657,0.5657,0.0]}, \
        {motion:[-0.5226,0.5657,-0.2165]}, \
        {motion:[-0.4,0.5657,-0.4]}, \
        {motion:[-0.2165,0.5657,-0.5226]}, \
        {motion:[0.0,0.5657,-0.5657]}, \
        {motion:[0.2165,0.5657,-0.5226]}, \
        {motion:[0.4,0.5657,-0.4]}, \
        {motion:[0.5226,0.5657,-0.2165]}, \
        {motion:[0.5657,0.5657,0.0]}, \
        {motion:[0.5226,0.5657,0.2165]}, \
        {motion:[0.4,0.5657,0.4]}, \
        {motion:[0.2165,0.5657,0.5226]}, \
]

data modify storage wishlist:custom_blocks yellow_vertical_spring set value { \
        interaction: { \
                Tags: [wishlist.spring,wishlist.spring.vertical,wishlist.spring.yellow], \
                width: 1.0, \
                height: 1.0 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271860, \
                      display:{Name:'{"text":"Yellow Vertical Spring","italic":false}'} \
              } \
        }, \
        block: "minecraft:air", \
}

data modify storage wishlist:custom_blocks yellow_diagonal_spring set value { \
        interaction: { \
                Tags: [wishlist.spring,wishlist.spring.diagonal,wishlist.spring.yellow], \
                width: 1.0, \
                height: 1.0 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271861, \
                      display:{Name:'{"text":"Yellow Diagonal Spring","italic":false}'} \
              } \
        }, \
        block: "minecraft:air", \
}

data modify storage wishlist:custom_blocks red_vertical_spring set value { \
        interaction: { \
                Tags: [wishlist.spring,wishlist.spring.vertical,wishlist.spring.red], \
                width: 1.0, \
                height: 1.0 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271862, \
                      display:{Name:'{"text":"Red Vertical Spring","italic":false}'} \
              } \
        }, \
        block: "minecraft:air", \
}

data modify storage wishlist:custom_blocks red_diagonal_spring set value { \
        interaction: { \
                Tags: [wishlist.spring,wishlist.spring.diagonal,wishlist.spring.red], \
                width: 1.0, \
                height: 1.0 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271863, \
                      display:{Name:'{"text":"Red Diagonal Spring","italic":false}'} \
              } \
        }, \
        block: "minecraft:air", \
}


function parkour:tick_1t
