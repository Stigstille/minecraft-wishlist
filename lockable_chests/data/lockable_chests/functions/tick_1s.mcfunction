# tick_1s.mcfunction

execute at @a \
        as @e[type=minecraft:item,distance=..4] \
        at @s \
        if block ~ ~-1 ~ #minecraft:anvil \
        if data entity @s Item.tag.wishlist.key_blank \
        run function lockable_chests:cut_key

schedule function lockable_chests:tick_1s 1s
