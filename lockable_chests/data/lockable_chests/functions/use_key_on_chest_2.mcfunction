# use_key_on_chest_2.mcfunction

scoreboard players set go wishlist.vars 0
execute store success score go wishlist.vars if data block ~ ~ ~ Lock

execute if score go wishlist.vars matches 0 \
        run function lockable_chests:lock

execute unless score go wishlist.vars matches 0 \
        run function lockable_chests:unlock

scoreboard players set steps wishlist.vars 0

