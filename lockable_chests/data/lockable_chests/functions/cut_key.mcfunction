# cut_key.mcfunction

function wishlist:decrement_item_entity_count
execute unless entity @e[type=item,distance=..1,nbt={Item:{tag:{wishlist:{chest_key:1b}}}}] \
        run function lockable_chests:generate_combination
execute as @e[type=item,distance=..1,nbt={Item:{tag:{wishlist:{chest_key:1b}}}}] \
        run data modify storage wishlist:args combination set from entity @s Item.tag.wishlist.combination
loot spawn ~ ~ ~ loot lockable_chests:chest_key
particle minecraft:instant_effect ~ ~ ~ 0.1 0.1 0.1 0.005 10
playsound minecraft:block.anvil.use player @a ~ ~ ~
advancement grant @p only wishlist:make_chest_key
