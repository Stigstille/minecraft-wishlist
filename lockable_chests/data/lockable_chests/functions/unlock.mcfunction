# unlock.mcfunction

data modify storage wishlist:args combination set from entity @s SelectedItem.tag.wishlist.combination
scoreboard players set go wishlist.vars 0
execute store success score go wishlist.vars \
        run data modify storage wishlist:args combination set from block ~ ~ ~ Lock
execute if score go wishlist.vars matches 0 \
        run function lockable_chests:unlock_1
execute unless score go wishlist.vars matches 0 \
        run function lockable_chests:unlock_2

