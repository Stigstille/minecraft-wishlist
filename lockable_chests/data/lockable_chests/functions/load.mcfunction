# load.mcfunction

scoreboard objectives add wishlist.lockable_chests.used_carrot_on_a_stick minecraft.used:minecraft.carrot_on_a_stick

data modify storage wishlist:args all_chatter append value \
        "I locked all my stuff up, but someone took the chest!"
data modify storage wishlist:args all_chatter append value \
        "So, you have to cut a key blank on an anvil; is that right?"

function lockable_chests:tick
function lockable_chests:tick_1s
