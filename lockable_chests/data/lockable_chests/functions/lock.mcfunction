# lock.mcfunction

data modify block ~ ~ ~ Lock set from entity @s SelectedItem.tag.wishlist.combination
playsound minecraft:block.wooden_trapdoor.close block @a ~ ~ ~
title @s actionbar "Locked"
advancement grant @s only wishlist:lock_container
