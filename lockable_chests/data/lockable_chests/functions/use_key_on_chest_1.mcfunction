# use_key_on_chest_1.mcfunction

execute if block ~ ~ ~ #wishlist:lockable \
        run function lockable_chests:use_key_on_chest_2
execute unless block ~ ~ ~ #wishlist:non_full \
        run scoreboard players set steps wishlist.vars 0
scoreboard players remove steps wishlist.vars 1
execute if score steps wishlist.vars matches 1.. \
        positioned ^ ^ ^0.1 \
        run function lockable_chests:use_key_on_chest_1

