# tick.mcfunction

# Carrots on a stick are triggered when used
execute as @a[scores={wishlist.lockable_chests.used_carrot_on_a_stick=1..}] \
        at @s \
        if data entity @s SelectedItem.tag.wishlist.chest_key \
        run function lockable_chests:use_key_on_chest

scoreboard players reset * wishlist.lockable_chests.used_carrot_on_a_stick

schedule function lockable_chests:tick 1t
