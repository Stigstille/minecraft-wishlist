Lockable Chests
===============

Provides a key item that can be used to lock and unlock chests and other
lockable blocks.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The following containers are lockable:

* chest
* trapped chest
* barrel
* dropper
* dispenser
* hopper
* furnace
* blast furnace
* smoker
* shulker box
* beacon

Use the `lockable_chests:key_blank` loot table to generate a key blank; these
are generic items stackable up to 64.

Key blanks can be dropped on anvils to make them into unique keys; these are
unstackable and can be used to lock and unlock chests by crouching and using
them on the chest.

You can also copy an existing key by first dropping the original on the anvil,
and then a blank. The newly created key will behave like the original, even
though they won't stack together.

Any unlocked chest will become locked by the key, and will require the key or
one of its copies to be opened again.

Any locked chest can only be opened by the key that closed it or one of its
copies.

Overhead
--------

Updates every tick:

* checks every player to see if they have used a key.

Known Issues
------------

Items can still be taken from a locked container by placing a hopper under it.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Your Very Own: drop a key blank on an anvil and obtain an unique key
    * The Illusion of Safety: use a key to lock a container
        * Master of Unlocking: use a key to unlock a container
