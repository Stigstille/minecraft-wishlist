# start_spectating.mcfunction

scoreboard players reset @s wishlist.dungeon.death
gamemode spectator @s
data modify storage wishlist:args Pos set value [0.0d,0.0d,0.0d]
execute store result storage wishlist:args Pos[0] double 1 \
        run scoreboard players get @s wishlist.dungeon.death_x
execute store result storage wishlist:args Pos[1] double 1 \
        run scoreboard players get @s wishlist.dungeon.death_y
execute store result storage wishlist:args Pos[2] double 1 \
        run scoreboard players get @s wishlist.dungeon.death_z
tag @s add wishlist.dungeon.dead
summon marker ~ ~ ~ {Tags:[wishlist.dungeon.marker]}
execute as @e[type=marker,tag=wishlist.dungeon.marker,distance=0,limit=1] \
        run function dungeon_tools:start_spectating_1
tag @s remove wishlist.dungeon.dead

