# tick_1s.mcfunction

execute as @a[gamemode=!creative,gamemode=!spectator] \
        at @s \
        positioned ~-16 ~-8 ~-16 \
        unless entity @e[type=marker,tag=wishlist.dungeon_room,dx=16,dy=16,dz=16] \
        run clear @s minecraft:iron_nugget{wishlist:{small_key:1b}}

execute as @a[gamemode=!creative,gamemode=!spectator] \
        at @s \
        positioned ~-16 ~-8 ~-16 \
        unless entity @e[type=marker,tag=wishlist.dungeon_room,dx=16,dy=16,dz=16] \
        run clear @s minecraft:iron_nugget{wishlist:{big_key:1b}}

kill @e[type=text_display,tag=wishlist.marker_data]

execute as @a \
        if predicate dungeon_tools:show_markers \
        at @s \
        run function dungeon_tools:debug

execute at @a \
        at @e[type=item,tag=wishlist.dungeon_loot,distance=..16] \
        run particle minecraft:wax_off ~ ~0.5 ~ 0.1 0.1 0.1 0.1 1 normal

execute as @a[gamemode=!creative,gamemode=!spectator,scores={wishlist.dungeon.rot=0..}] \
        at @s \
        run function dungeon_tools:tick_rot

schedule function dungeon_tools:tick_1s 1s

