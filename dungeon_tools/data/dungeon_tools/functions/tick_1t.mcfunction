# tick_1t.mcfunction

#execute as @e[type=player,scores={wishlist.dungeon.death=1..}] run function dungeon_tools:start_spectating
execute as @a[gamemode=spectator] \
        at @s \
        positioned ~-16 ~-8 ~-16 \
        if entity @e[type=marker,tag=wishlist.dungeon_room,dx=16,dy=16,dz=16] \
        run function dungeon_tools:tick_spectator

schedule function dungeon_tools:tick_1t 1t

