# tick_open.mcfunction
#
# Angry entities may change target for other player on the same room
#
# When the room is open, it remains so until no more non-passive (hostile or
# neutral) mobs remain inside.

execute positioned ~ ~-8 ~ \
        as @e[tag=wishlist.dungeon_angry,dx=16,dy=16,dz=16] \
        at @s \
        run data modify entity @s AngryAt set from entity @a[distance=..20,sort=nearest,limit=1] UUID

execute positioned ~ ~-8 ~ \
        unless entity @e[tag=wishlist.dungeon_enemy,dx=16,dy=16,dz=16] \
        positioned ~ ~8 ~ \
        run function dungeon_tools:clear_room

execute positioned ~ ~-8 ~ \
        as @e[type=area_effect_cloud,dx=16,dy=16,dz=16,nbt={Effects:[{Id:26}]}] \
        at @s \
        run function dungeon_tools:explosion
