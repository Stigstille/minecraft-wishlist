# eat_enchanted_apple.mcfunction

execute if score @s wishlist.dungeon.rot matches 0.. \
        run tellraw @s {"text":"You feel healed","color":"green","bold":true}
scoreboard players reset @s wishlist.dungeon.rot
advancement revoke @s only dungeon_tools:eat_enchanted_apple

