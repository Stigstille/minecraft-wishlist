# tick_room.mcfunction
#
# The kind of tick depends on the state the room is in

execute if entity @s[tag=wishlist.dungeon_ready] \
        run function dungeon_tools:tick_ready
execute if entity @s[tag=wishlist.dungeon_open] \
        run function dungeon_tools:tick_open
execute if entity @s[tag=wishlist.dungeon_clear] \
        run function dungeon_tools:tick_clear
execute if entity @s[tag=wishlist.dungeon_burn] \
        run function dungeon_tools:tick_burn
execute if entity @s[tag=wishlist.dungeon_freeze] \
        run function dungeon_tools:tick_freeze

scoreboard players add #ticked wishlist.vars 1
