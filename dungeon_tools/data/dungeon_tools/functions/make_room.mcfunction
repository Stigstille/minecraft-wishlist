# make_room.mcfunction

execute store result score x wishlist.vars run data get entity @p Pos[0] 0.0625
execute store result score y wishlist.vars run data get entity @p Pos[1]
execute store result score z wishlist.vars run data get entity @p Pos[2] 0.0625

data modify storage wishlist:args Pos set value [0.0,0.0,0.0]
execute store result storage wishlist:args Pos[0] double 16 \
        run scoreboard players get x wishlist.vars
execute store result storage wishlist:args Pos[1] double 1 \
        run scoreboard players get y wishlist.vars
execute store result storage wishlist:args Pos[2] double 16 \
        run scoreboard players get z wishlist.vars

summon minecraft:marker ~ ~ ~ {Tags:[wishlist.dungeon_room]}
scoreboard players operation @e[type=minecraft:marker,tag=wishlist.dungeon_room,sort=nearest,limit=1] wishlist.dungeon_room = #next wishlist.dungeon_room
scoreboard players add #next wishlist.dungeon_room 1
data modify entity @e[type=minecraft:marker,tag=wishlist.dungeon_room,sort=nearest,limit=1] Pos \
        set from storage wishlist:args Pos
