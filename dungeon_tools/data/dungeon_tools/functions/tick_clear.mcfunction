# tick_clear.mcfunction
#
# When the room is done, players can interact with the doors, using keys to
# open the regular ones and spectral arrows to reveal secret ones.

execute as @a[dx=16,dy=8,dz=16] \
        at @s \
        if predicate dungeon_tools:hold_small_key \
        if block ~ ~ ~ minecraft:oak_trapdoor \
        run function dungeon_tools:open_wood_door

execute as @a[dx=16,dy=8,dz=16] \
        at @s \
        if predicate dungeon_tools:hold_big_key \
        if block ~ ~ ~ minecraft:iron_trapdoor \
        run function dungeon_tools:open_iron_door

execute if entity @s[nbt={data:{door_north:3b}}] \
        positioned ~8 ~ ~ \
        unless block ~ ~ ~ minecraft:air \
        if entity @e[type=spectral_arrow,distance=..3] \
        run function dungeon_tools:open_secret_z

execute if entity @s[nbt={data:{door_south:3b}}] \
        positioned ~8 ~ ~16 \
        unless block ~ ~ ~ minecraft:air \
        if entity @e[type=spectral_arrow,distance=..3] \
        run function dungeon_tools:open_secret_z

execute if entity @s[nbt={data:{door_west:3b}}] \
        positioned ~ ~ ~8 \
        unless block ~ ~ ~ minecraft:air \
        if entity @e[type=spectral_arrow,distance=..3] \
        run function dungeon_tools:open_secret_x

execute if entity @s[nbt={data:{door_east:3b}}] \
        positioned ~16 ~ ~8 \
        unless block ~ ~ ~ minecraft:air \
        if entity @e[type=spectral_arrow,distance=..3] \
        run function dungeon_tools:open_secret_x

