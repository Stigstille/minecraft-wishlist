# tick_rot.mcfunction

scoreboard players remove @s wishlist.dungeon.rot 1

execute if score @s wishlist.dungeon.rot matches 20 \
        run tellraw @s {"text":"You feel queasy","color":"red","bold":true}
execute if score @s wishlist.dungeon.rot matches 10 \
        run tellraw @s {"text":"You feel ill!","color":"red","bold":true}
execute if score @s wishlist.dungeon.rot matches 5 \
        run tellraw @s {"text":"You feel deathly sick!","color":"red","bold":true}
execute if score @s wishlist.dungeon.rot matches 0 \
        run function dungeon_tools:rot
