# tick_freeze.mcfunction
#
# Rooms marked as freezing have special properties

execute as @a[gamemode=!creative,gamemode=!spectator,dx=16,dy=8,dz=16,predicate=dungeon_tools:wear_metal] \
        at @s \
        run function dungeon_tools:cold_damage

