# tick_burn.mcfunction
#
# Rooms marked as burning have special properties

execute as @a[gamemode=!creative,gamemode=!spectator,dx=16,dy=8,dz=16,nbt=!{ActiveEffects:[{Id:12}]}] \
        at @s \
        run function dungeon_tools:heat_damage

