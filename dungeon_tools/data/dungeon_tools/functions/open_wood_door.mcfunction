# open_wood_door.mcfunction

playsound minecraft:block.oak_door.open block @a ~ ~ ~
item modify entity @s weapon.mainhand wishlist:consume_one
fill ~-1 ~-4 ~-1 ~1 ~4 ~1 air replace oak_trapdoor[waterlogged=false]
fill ~-1 ~-4 ~-1 ~1 ~4 ~1 water replace oak_trapdoor[waterlogged=true]

