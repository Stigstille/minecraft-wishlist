# reset_room.mcfunction
#
# Reset a room

tag @s remove wishlist.dungeon_open
tag @s remove wishlist.dungeon_clear
tag @s add wishlist.dungeon_ready

tp @e[type=#wishlist:non_passive,tag=wishlist.dungeon_enemy,dx=16,dy=8,dz=16] 0 -128 0
kill @e[type=!marker,tag=wishlist.dungeon_loot,dx=16,dy=8,dz=16]

fill ~ ~ ~ ~15 ~7 ~15 air replace spruce_trapdoor[waterlogged=false]
fill ~ ~ ~ ~15 ~7 ~15 water replace spruce_trapdoor[waterlogged=true]
fill ~ ~ ~ ~15 ~7 ~15 air replace oak_trapdoor[waterlogged=false]
fill ~ ~ ~ ~15 ~7 ~15 water replace oak_trapdoor[waterlogged=true]
fill ~ ~ ~ ~15 ~7 ~15 air replace iron_trapdoor[waterlogged=false]
fill ~ ~ ~ ~15 ~7 ~15 water replace iron_trapdoor[waterlogged=true]

execute if entity @s[nbt={data:{door_north:1b}}] \
        run function dungeon_tools:close_wood_n
execute if entity @s[nbt={data:{door_south:1b}}] \
        run function dungeon_tools:close_wood_s
execute if entity @s[nbt={data:{door_west:1b}}] \
        run function dungeon_tools:close_wood_w
execute if entity @s[nbt={data:{door_east:1b}}] \
        run function dungeon_tools:close_wood_e

execute if entity @s[nbt={data:{door_north:2b}}] \
        run function dungeon_tools:close_iron_n
execute if entity @s[nbt={data:{door_south:2b}}] \
        run function dungeon_tools:close_iron_s
execute if entity @s[nbt={data:{door_west:2b}}] \
        run function dungeon_tools:close_iron_w
execute if entity @s[nbt={data:{door_east:2b}}] \
        run function dungeon_tools:close_iron_e

execute if entity @s[nbt={data:{door_north:3b}}] \
        run clone ~7 ~-3 ~-2 ~8 ~-2 ~1 ~7 ~ ~-2
execute if entity @s[nbt={data:{door_south:3b}}] \
        run clone ~7 ~-3 ~14 ~8 ~-2 ~17 ~7 ~ ~14
execute if entity @s[nbt={data:{door_west:3b}}] \
        run clone ~-2 ~-3 ~7 ~1 ~-2 ~8 ~-2 ~ ~7
execute if entity @s[nbt={data:{door_east:3b}}] \
        run clone ~14 ~-3 ~7 ~17 ~-2 ~8 ~14 ~ ~7

execute unless data entity @s data.door_north \
        run function dungeon_tools:close_n
execute unless data entity @s data.door_south \
        run function dungeon_tools:close_s
execute unless data entity @s data.door_west run function dungeon_tools:close_w
execute unless data entity @s data.door_east run function dungeon_tools:close_e

fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=north]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=north]{Lock:"nudiafgukhidofguidofughidtiarnoeudia"} replace minecraft:piston_head
fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=south]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=south]{Lock:"nudiafgukhidofguidofughidtiarnoeudia"} replace minecraft:piston_head
fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=east]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=east]{Lock:"nudiafgukhidofguidofughidtiarnoeudia"} replace minecraft:piston_head
fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=west]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=west]{Lock:"nudiafgukhidofguidofughidtiarnoeudia"} replace minecraft:piston_head

fill ~7 ~ ~7 ~8 ~8 ~8 cave_air replace #wishlist:air

execute if entity @s[nbt={data:{door_north:2b}}] \
        run fill ~7 ~ ~ ~8 ~3 ~ iron_trapdoor[facing=south,open=true] keep
execute if entity @s[nbt={data:{door_south:2b}}] \
        run fill ~7 ~ ~15 ~8 ~3 ~15 iron_trapdoor[facing=north,open=true] keep
execute if entity @s[nbt={data:{door_west:2b}}] \
        run fill ~ ~ ~7 ~ ~3 ~8 iron_trapdoor[facing=east,open=true] keep
execute if entity @s[nbt={data:{door_east:2b}}] \
        run fill ~15 ~ ~7 ~15 ~3 ~8 iron_trapdoor[facing=west,open=true] keep

execute if entity @s[nbt={data:{door_north:3b}}] \
        run clone ~7 ~-3 ~-2 ~8 ~-2 ~1 ~7 ~ ~-2
execute if entity @s[nbt={data:{door_south:3b}}] \
        run clone ~7 ~-3 ~14 ~8 ~-2 ~17 ~7 ~ ~14
execute if entity @s[nbt={data:{door_west:3b}}] \
        run clone ~-2 ~-3 ~7 ~1 ~-2 ~8 ~-2 ~ ~7
execute if entity @s[nbt={data:{door_east:3b}}] \
        run clone ~14 ~-3 ~7 ~17 ~-2 ~8 ~14 ~ ~7

execute unless data entity @s data.door_north \
        run fill ~7 ~ ~ ~8 ~3 ~ spruce_trapdoor[facing=south,open=true] keep
execute unless data entity @s data.door_south \
        run fill ~7 ~ ~15 ~8 ~3 ~15 spruce_trapdoor[facing=north,open=true] keep
execute unless data entity @s data.door_west \
        run fill ~ ~ ~7 ~ ~3 ~8 spruce_trapdoor[facing=east,open=true] keep
execute unless data entity @s data.door_east \
        run fill ~15 ~ ~7 ~15 ~3 ~8 spruce_trapdoor[facing=west,open=true] keep

execute positioned ~ ~-8 ~ as @a[gamemode=spectator,dx=16,dy=16,dz=16] \
        run function dungeon_tools:stop_spectating
