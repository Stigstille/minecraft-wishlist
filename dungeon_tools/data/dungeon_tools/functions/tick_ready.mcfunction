# tick_ready.mcfunction
#
# When the room is ready, if all players at a distance from the center are
# actually inside the actual room, it will open.

execute store result score #in wishlist.vars \
        positioned ~ ~-8 ~ \
        if entity @a[gamemode=!creative,gamemode=!spectator,dx=15,dy=15,dz=15]
execute store result score #out wishlist.vars \
        positioned ~8 ~ ~8 \
        if entity @a[gamemode=!creative,gamemode=!spectator,distance=..24]
execute if score #in wishlist.vars = #out wishlist.vars \
        run function dungeon_tools:open_room

