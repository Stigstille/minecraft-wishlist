# tick_spectator.mcfunction

tag @s add wishlist.dungeon_spectator
execute at @a[gamemode=!creative,gamemode=!spectator,sort=nearest,limit=1] \
        positioned ~-16 ~-8 ~-16 \
        at @e[type=marker,tag=wishlist.dungeon_room,dx=16,dy=16,dz=16] \
        positioned ~8 ~6 ~13 \
        facing ~ ~-6 ~-5 \
        run tp @a[tag=wishlist.dungeon_spectator] ~ ~ ~ ~ ~
tag @s remove wishlist.dungeon_spectator



