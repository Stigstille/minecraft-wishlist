# edit_kit.mcfunction

execute rotated ~ 0 \
        run summon minecraft:armor_stand ^ ^ ^1.5 {ShowArms:1b,CustomNameVisible:1b,Tags:[wishlist.dungeon_kit]}
execute positioned ~-16 ~-8 ~-16 \
        run data modify entity @e[type=minecraft:armor_stand,tag=wishlist.dungeon_kit,dx=16,dy=8,dz=16,limit=1] {} \
                merge from entity @e[type=minecraft:marker,tag=wishlist.dungeon_room,dx=16,dy=8,dz=16,limit=1] data.kit

