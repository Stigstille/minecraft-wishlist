# open_room.mcfunction
#
# Open a ready room by closing all doors (yea) and spawn all mobs

tag @s remove wishlist.dungeon_ready
tag @s add wishlist.dungeon_open

playsound minecraft:block.wooden_door.close block @a ~8 ~ ~
playsound minecraft:block.wooden_door.close block @a ~8 ~ ~16
playsound minecraft:block.wooden_door.close block @a ~ ~ ~8
playsound minecraft:block.wooden_door.close block @a ~16 ~ ~8

function dungeon_tools:close_n
function dungeon_tools:close_s
function dungeon_tools:close_w
function dungeon_tools:close_e

fill ~7 ~ ~7 ~8 ~8 ~8 light[level=8] replace #wishlist:air

scoreboard players set type wishlist.vars 0
execute store result score type wishlist.vars run data get entity @s data.mob_type

data modify storage wishlist:args data set from entity @s data

execute positioned ~ ~-8 ~ \
        at @e[type=minecraft:marker,tag=wishlist.dungeon_mob,dx=16,dy=16,dz=16] \
        run function dungeon_tools:summon_mob

execute if data storage wishlist:args data.boss_room \
        run playsound wishlist:dungeon.spawn_boss ambient @a ~8 ~ ~8

execute positioned ~ ~-8 ~ \
        as @a[gamemode=!creative,gamemode=!spectator,dx=16,dy=8,dz=16] \
        run advancement grant @s only wishlist:trigger_dungeon

