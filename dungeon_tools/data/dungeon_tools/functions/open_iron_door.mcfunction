# open_iron_door.mcfunction

playsound minecraft:block.iron_door.open block @a ~ ~ ~
item modify entity @s weapon.mainhand wishlist:consume_one
fill ~-1 ~-4 ~-1 ~1 ~4 ~1 air replace iron_trapdoor[waterlogged=false]
fill ~-1 ~-4 ~-1 ~1 ~4 ~1 water replace iron_trapdoor[waterlogged=true]
