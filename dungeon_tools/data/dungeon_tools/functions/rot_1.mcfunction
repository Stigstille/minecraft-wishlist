# rot_1.mcfunction 

tag @s add wishlist.dungeon.rotten
summon wither_skeleton ~ ~ ~ { \
        Tags:[wishlist.dungeon.rotten], \
        HandDropChances:[100.f,100.f], \
        ArmorDropChances:[100.f,100.f,100.f,100.f], \
        DeathLootTable:"minecraft:empty" \
        }
execute as @e[type=wither_skeleton,tag=wishlist.dungeon.rotten,distance=0,limit=1] \
        run function dungeon_tools:rot_2
tag @s remove wishlist.dungeon.rotten

