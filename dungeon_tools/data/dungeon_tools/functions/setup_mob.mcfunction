# setup_mob.mcfunction

tag @s add wishlist.dungeon_enemy
data modify entity @s {} merge from storage wishlist:args data.kit
data modify entity @s DeathLootTable set value "dungeon_tools:mob_drop"

execute if data storage wishlist:args data.boss_room \
        run effect give @s resistance 999999 3 false
