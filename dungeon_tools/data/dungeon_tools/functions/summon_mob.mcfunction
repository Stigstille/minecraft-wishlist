# summon_mob.mcfunction

execute if score type wishlist.vars matches 0 \
        run summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:stick",Count:1b}}

execute if score type wishlist.vars matches 1 \
        summon minecraft:blaze \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 2 \
        summon minecraft:creeper \
        run function dungeon_tools:setup_creeper

execute if score type wishlist.vars matches 3 \
        summon minecraft:drowned \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 4 \
        summon minecraft:elder_guardian \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 5 \
        summon minecraft:ghast \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 6 \
        summon minecraft:guardian \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 7 \
        summon minecraft:hoglin \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 8 \
        summon minecraft:husk \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 9 \
        summon minecraft:magma_cube \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 10 \
        summon minecraft:skeleton \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 11 \
        summon minecraft:slime \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 12 \
        summon minecraft:stray \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 13 \
        summon minecraft:wither_skeleton \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 14 \
        summon minecraft:zoglin \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 15 \
        summon minecraft:zombie \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 16 \
        summon minecraft:zombie_villager \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 17 \
        summon minecraft:spider \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 18 \
        summon minecraft:cave_spider \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 19 \
        summon minecraft:slime \
        run function dungeon_tools:setup_slime_1

execute if score type wishlist.vars matches 20 \
        summon minecraft:slime \
        run function dungeon_tools:setup_slime_2

execute if score type wishlist.vars matches 21 \
        summon minecraft:slime \
        run function dungeon_tools:setup_slime_3

execute if score type wishlist.vars matches 22 \
        summon minecraft:magma_cube \
        run function dungeon_tools:setup_slime_1

execute if score type wishlist.vars matches 23 \
        summon minecraft:magma_cube \
        run function dungeon_tools:setup_slime_2

execute if score type wishlist.vars matches 24 \
        summon minecraft:magma_cube \
        run function dungeon_tools:setup_slime_3

execute if score type wishlist.vars matches 25 \
        summon minecraft:wither \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 26 \
        summon minecraft:warden \
        run function dungeon_tools:setup_warden

execute if score type wishlist.vars matches 27 \
        summon minecraft:ender_dragon \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 28 \
        summon minecraft:ravager \
        run function dungeon_tools:setup_mob

execute if score type wishlist.vars matches 29 \
        summon minecraft:wolf \
        run function dungeon_tools:setup_angry

execute if score type wishlist.vars matches 30 \
        summon minecraft:iron_golem \
        run function dungeon_tools:setup_angry

