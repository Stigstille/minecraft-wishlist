# rot_2.mcfunction

item replace entity @s weapon.mainhand from entity @a[distance=0,limit=1] weapon.mainhand
item replace entity @s weapon.offhand from entity @a[distance=0,limit=1] weapon.offhand
item replace entity @s armor.chest from entity @a[distance=0,limit=1] armor.chest
item replace entity @s armor.head from entity @a[distance=0,limit=1] armor.head
item replace entity @s armor.legs from entity @a[distance=0,limit=1] armor.legs
item replace entity @s armor.feet from entity @a[distance=0,limit=1] armor.feet
setblock 0 0 0 oak_sign
data modify block 0 0 0 front_text.messages[0] set value \
        '{"nbt":"bukkit.lastKnownName","entity":"@a[tag=wishlist.dungeon.rotten]"}'
data modify entity @s CustomName set from block 0 0 0 front_text.messages[0]
setblock 0 0 0 air

item replace entity @a[distance=0,limit=1] weapon.mainhand with minecraft:air
item replace entity @a[distance=0,limit=1] weapon.offhand with minecraft:air
item replace entity @a[distance=0,limit=1] armor.chest with minecraft:air
item replace entity @a[distance=0,limit=1] armor.feet with minecraft:air
item replace entity @a[distance=0,limit=1] armor.head with minecraft:air
item replace entity @a[distance=0,limit=1] armor.legs with minecraft:air


