# tick_core.mcfunction
#
# Tick every room in reach of this core that contains at least one player.

scoreboard players set #ticked wishlist.vars 0

execute as @e[type=marker,tag=wishlist.dungeon_room,distance=..128] \
        at @s \
        positioned ~ ~-8 ~ \
        if entity @a[gamemode=!creative,gamemode=!spectator,dx=16,dy=16,dz=16] \
        positioned ~ ~8 ~ \
        run function dungeon_tools:tick_room

execute if score #ticked wishlist.vars matches 0 \
        as @e[type=marker,tag=wishlist.dungeon_room,tag=!wishlist.dungeon_ready,distance=..128] \
        at @s \
        run function dungeon_tools:reset_room
