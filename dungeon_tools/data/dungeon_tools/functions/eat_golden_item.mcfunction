# eat_golden_item.mcfunction

execute if score @s wishlist.dungeon.rot matches 0.. \
        run tellraw @s {"text":"You feel better","color":"green","bold":true}
execute if score @s wishlist.dungeon.rot matches 0.. \
        run scoreboard players add @s wishlist.dungeon.rot 30
advancement revoke @s only dungeon_tools:eat_golden_item
