# clear_room.mcfunction
#
# Once all mobs are gone, change status to clear, open regular doors, spawn room
# drops, and unlock chests.

tag @s remove wishlist.dungeon_open
tag @s add wishlist.dungeon_clear

playsound minecraft:block.wooden_door.open block @a ~8 ~ ~
playsound minecraft:block.wooden_door.open block @a ~8 ~ ~16
playsound minecraft:block.wooden_door.open block @a ~ ~ ~8
playsound minecraft:block.wooden_door.open block @a ~16 ~ ~8

fill ~-1 ~ ~-1 ~16 ~8 ~16 air replace spruce_trapdoor[waterlogged=false]
fill ~-1 ~ ~-1 ~16 ~8 ~16 water replace spruce_trapdoor[waterlogged=true]

fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=north]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=north]{LootTable:"minecraft:chests/simple_dungeon"} replace minecraft:piston_head
fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=south]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=south]{LootTable:"minecraft:chests/simple_dungeon"} replace minecraft:piston_head
fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=east]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=east]{LootTable:"minecraft:chests/simple_dungeon"} replace minecraft:piston_head
fill ~ ~ ~ ~15 ~8 ~15 minecraft:piston_head replace minecraft:chest[facing=west]
fill ~ ~ ~ ~15 ~8 ~15 minecraft:chest[facing=west]{LootTable:"minecraft:chests/simple_dungeon"} replace minecraft:piston_head


execute positioned ~ ~-8 ~ \
        as @e[type=marker,tag=wishlist.dungeon_loot,dx=16,dy=16,dz=16] \
        at @s \
        run function dungeon_tools:spawn_loot

execute if data entity @s data.boss_room \
        run playsound wishlist:dungeon.kill_boss ambient @a ~8 ~ ~8
execute if data entity @s data.boss_room \
        run advancement grant @a[gamemode=!creative,gamemode=!spectator,dx=16,dy=8,dz=16] only wishlist:kill_dungeon_boss
