# save_kit.mcfunction

data modify storage wishlist:args kit set value {}
data modify storage wishlist:args kit.CustomName \
        set from entity @e[type=minecraft:armor_stand,tag=wishlist.dungeon_kit,distance=..8,limit=1] CustomName
data modify storage wishlist:args kit.ArmorItems \
        set from entity @e[type=minecraft:armor_stand,tag=wishlist.dungeon_kit,distance=..8,limit=1] ArmorItems
data modify storage wishlist:args kit.HandItems \
        set from entity @e[type=minecraft:armor_stand,tag=wishlist.dungeon_kit,distance=..8,limit=1] HandItems
execute positioned ~-16 ~-8 ~-16 \
        run data modify entity @e[type=minecraft:marker,tag=wishlist.dungeon_room,dx=16,dy=8,dz=16,limit=1] data.kit \
                set from storage wishlist:args kit
kill @e[type=minecraft:armor_stand,tag=wishlist.dungeon_kit,distance=..8]


