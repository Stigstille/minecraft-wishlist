# load.mcfunction

scoreboard objectives add wishlist.dungeon_room dummy
scoreboard objectives add wishlist.dungeon.death minecraft.custom:minecraft.deaths
scoreboard objectives add wishlist.dungeon.death_x dummy
scoreboard objectives add wishlist.dungeon.death_y dummy
scoreboard objectives add wishlist.dungeon.death_z dummy
scoreboard objectives add wishlist.dungeon.rot dummy

data modify storage wishlist:args all_chatter append value \
        "It's mildly inconvenient to go alone"
data modify storage wishlist:args all_chatter append value \
        "I've heard of strange keys that you can't carry on you outside of the place the came from"

function dungeon_tools:tick_1s
function dungeon_tools:tick_1t
