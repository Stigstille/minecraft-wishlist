Dungeon Tools
=============

Provides tools to construct Zelda-like (as in the original Legend of Zelda,
from the NES) dungeons.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Playing Dungeons
----------------

Dungeons are made out of 16x16 rooms (with 4-block-wide walls, leaving a 12x12
usable space in the inside). The objective can be to reach an specific room
from another, to retrieve a specific item and exit again through the same
place, to kill one or more specific mobs inside, or any other challenge that
the dungeon builder may think of.

Rooms await until all players at a 24-block distance of their center are inside
then, then they trigger: their doors close, all chests inside are locked, and a
number of preset mobs are summoned.

The room is considered to be cleared when all summoned mobs are gone: at this
point, doors and chests are unlocked, and additional preset loot may spawn at
specific points in the room.

Regular doors are opened when the room is cleared, but there are three kinds of
doors that behave differently:

* locked wooden doors require a small key to open; a player must hold the key
  on their hand and touch the door.
* locked metal doors require a big key to open; a player must hold the key on
  their hand and touch the door.
* secret doors must be shot with a spectral arrow to be revealed. until that
  point they will look just like regular walls.

A room can also be declared a boss room: Mobs in these rooms are stronger, and
a musical cue plays both when the room is triggered and when it is cleared.

Building Dungeons
-----------------

The process of building the actual dungeon structure is left to their builder.
No tools are provided for this.

Dungeon rooms must be built perfectly aligned to the chunk grid. The usual
dungeon room looks like this from the top:

```
#######++#######
#######++#######
##............##
##............##
##............##
##............##
##............##
++............++
++............++
##............##
##............##
##............##
##............##
##............##
#######++#######
#######++#######
```

Where `#` is a solid block, `.` is open space, and `+` is the space where a
door may be created: make it solid for a solid wall or secret door, and leave
it open for a regular or locked door.

The walls may be thicker or thinner (but should always be at least
one-block wide on each side) and the internal space may contain additional
obstructions, but the general shape is always like this.

The dungeon room has a total area of 16x16x16 blocks, vertically, eight of
those blocks are counted starting from the ground level, and eight are below.
Doors are always at the same height, and in the middle of the walls.

### Features

Then you'll want to create a number of markers to store the dungeon data. These
are managed with the dungeon tools book. Each widget has the following form:

```
[+ -] widget name
```

Click on the `+` to create a widget, and on the `-` to delete one (usually the
nearest one on a 8-block radius if there is one)

While holding the dungeon tools book, all markers close to you will be visible.

The **core** defines the center of the dungeon; any room whose distance
to the core is no more than 128 blocks is considered to be part of the dungeon;
you'll want to place it close to the geometric center of the structure.

The core must also be ticked for the dungeon to work. You can do this in a
function that you write yourself or with a command block. It should be ticked
every second (20 server ticks), and it's preferable to target it by its UUID:

```
execute as <core uuid> at @s run function dungeon_tools:tick_core
```

Each one of the **rooms** must have a marker too; this one can be created from
anywhere in the room, but it must be done while standing on ground level;
otherwise, doors will not appear to be in the right place. The room marker is
placed on the northwest corner of the room/chunk, not the center

A **loot** marker defines a place where treasure drops after the room has been
cleared. Any item can be treasure. Hold it on your off hand while creating the
marker and a copy of it will be stored.

A **mob** marker defines a place where mobs appear when the room is triggered.
All mobs in the room will have the same type and equipment, that is defined
elsewhere.

If the room is declared **boss**, all mobs spawned will be stronger, and a
musical cue will play both when the room triggers and clears.

If the room is declared **burn**, it is considered to be on fire/very hot and
the following special properties apply:

* lava will instakill players,
* toxic fumes cause both poison and nausea,
* these effects can be prevented with the fire resistance status.

If the room is declared **cold**, it is considered to be freezing/very cold and
the following special properties apply:

* wearing metal or diamond armor causes slowness and weakness
* wearing leather armor instead negates the effect

Any regular chest placed into a dungeon room will be locked during the open
stage, and when the room is cleared, reset with the tag
`LootTable:"minecraft:chests/simple_dungeon"`; hence, items will be generated
the moment a player opens the chest after clearing the room.

### Doors

Each room has four doors (north, south, west, east); by default these are
wooden doors that are automatically opened when the door is cleared. The book
provides a widget to change these:

```
[w k b s x] direction
```

* `w` is for "wooden" and means the door has the default behavior,
* `k` is for "key" and means the door requires a small key to open,
* `b` is for "big key" and means the door requires a big key to open,
* `s` is for "secret" and means a secret door can be revealed by shooting the
  wall with a spectral arrow
* `x` means there is no door on that side, not even a regular one; this is
  usually used only to mark the starting door of the dungeon.

### Mobs

Each room can have a different type of mob with custom weapons/armor that can
be configured; these can be chosen in the second page of the dungeon tools
book.

To configure the equipment for the mobs, click on `edit` under "Mob Kit"; this
will summon an armor stand that can be equipped with what the mobs should have;
click again on `save` under "Mob Kit" to save the changes.

### Bosses

Bosses are just mobs in a room marked **boss**; they have musical cues when
spawned and when all of them die, and are buffed with `minecraft:resistance`
level 4 (i.e.: `3` in the `effect give` command) when summoned.

The dungeons datapack does not provide a way to add a bossbar to these mobs,
since each bossbar in a Minecraft world has to be hard coded, but this can be
implemented easily in a per-boss-basis

First create a bossbar of name `<name>` and set it max value to the maximum
health of the mob that will spawn in the room.

Then add three command blocks below the dungeon room marker: the bottom one
should be marked `repeat`, `unconditional`, `always active`, pointing up and
run the command:

```
bossbar set <name> players
```

Immediately above it, another should be marked `chain`, `unconditional`,
`always active`, pointing up and run the command:

```
execute store result bossbar <name> value run data get entity @e[type=#wishlist:non_passive,tag=wishlist.dungeon_enemy,dx=16,dy=16,dz=16,limit=1] Health
```

Finally, above all of them, a last command block marked `chain`, `conditional`,
`always active`, pointing up, should run:

```
bossbar set <name> players @a[dx=16,dy=16,dz=16]
```

Miscellany
-----------

There's a fully-coded additional status effect that is not triggered by any
dungeon code. If a players is given a score in `wishlist.dungeon.rot`, they
will start rotting: every second, the score will go down by one, and if it
reaches zero the player will be killed and a wither skeleton wearing their
armor and wielding their weapons will stand in their place. Some time can be
earned by eating golden carrots or apples (adds 30 seconds to the time until
death), and the effect can be cured by consuming an enchanted golden apple.

Players spectating a dungeon being cleared will be teleported to the nearest
active room and they will be able to watch the players doing the clearing. This
may be used to occupy the players recently killed in the dungeon.

The dungeon intentionally interacts with the skip tools in the `magic_tools`
datapack: players will be able to teleport away from the dungeon, but not back
in, and they will not be able to teleport to a player that is inside the
dungeon.

If the type of a loot, or mobs in a room is not set, sticks will be spawned in
their place.

Known Issues
------------

Secret doors in underwater rooms will be replaced by air providing a breathing
pocket for players; this is not the intended behaviour.

Similarly, chests in underwater rooms will be replaced by non-waterlogged
chests when the room is reset. This is not intended, but has been left to be
fixed at a later date.

There seems to be the possibility of a room triggering even if the player is
not entirely inside it; if the player steps back at the right moment, they
could be locked outside the room that should be cleared.

The door opening check is performed once per 20 server ticks: this means that
locked doors may take a second to actually open. It also means that secret
doors may not open because a player removed the arrow on the wall after
shooting, but before the ckeck happened.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* It's Dangerous To Go Alone!: trigger a custom dungeon
    * Take This!: kill the boss of a custom dungeon
