# src/custom_trader.mcfunction

execute if entity @s[tag=traaa.map_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args map_trader
execute if entity @s[tag=traaa.ferry_driver] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args ferry_driver
execute if entity @s[tag=traaa.train_driver] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args train_driver
execute if entity @s[tag=traaa.bartender] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args bartender
execute if entity @s[tag=traaa.baker] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args baker
execute if entity @s[tag=traaa.sushi_chef] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args sushi_chef
execute if entity @s[tag=traaa.noodle_chef] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args noodle_chef
execute if entity @s[tag=traaa.pizza_chef] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args pizza_chef
execute if entity @s[tag=traaa.box_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args box_trader
execute if entity @s[tag=traaa.pharmacist] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args pharmacist
execute if entity @s[tag=traaa.tea_trader] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args tea_trader
execute if entity @s[tag=traaa.grocer] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args grocer
execute if entity @s[tag=traaa.bank_teller] \
        run data modify entity @s Offers.Recipes \
                set from storage traaa:args bank_teller
execute if entity @s[type=minecraft:wandering_trader] \
        run data modify entity @s DespawnDelay set value 2000000000

