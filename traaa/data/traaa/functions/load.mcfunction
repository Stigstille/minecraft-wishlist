# load.mcfunction

# General chat lines
data modify storage wishlist:args all_chatter append value \
        "Welcome to Trans Station!"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of lifetime stuff? It will last your entire life. One of them, at least"
data modify storage wishlist:args all_chatter append value \
        "Ever heard of Vonturbola and Rallyim? They have been everywhere"
data modify storage wishlist:args all_chatter append value \
        "You're awesome!"
data modify storage wishlist:args all_chatter append value \
        "You're valid!"
data modify storage wishlist:args all_chatter append value \
        "You deserve hugs!"

# Chatter lines about Figaro
data modify storage wishlist:args all_chatter append value \
        "Today I'm gonna spend most of the day at the shops in Figaro... and it might not be enough!"
data modify storage wishlist:args all_chatter append value \
        "Did you know that you can get a free house and warehouse access in the Figaro Community?"
data modify storage wishlist:args all_chatter append value \
        "I'm flabbergasted at the amount of people who cooperated to build the city of Figaro"

# Chatter lines about Black Bastion
data modify storage wishlist:args all_chatter append value \
        "Ever wondered how the mail gets everywhere? It's Black Bastion's Work"
data modify storage wishlist:args all_chatter append value \
        "High into the sky~ Among the swirling mists~"
data modify storage wishlist:args all_chatter append value \
        "I didn't know that the only way to reach Black Bastion was to ride a ship... on the air"


function traaa:tick_3s
function traaa:tick_1t
