
# tick_1t.mcfunction

# Warp points for the figaro skyscraper
execute in minecraft:overworld as @a[scores={wishlist.x=2985,wishlist.y=99..106,wishlist.z=-943}] at @s run tp @s ~-29 ~ ~
execute in minecraft:overworld as @a[scores={wishlist.x=2957,wishlist.y=99..106,wishlist.z=-943}] at @s run tp @s ~29 ~ ~
execute in minecraft:overworld as @a[scores={wishlist.x=2985,wishlist.y=99..106,wishlist.z=-959}] at @s run tp @s ~-29 ~ ~
execute in minecraft:overworld as @a[scores={wishlist.x=2957,wishlist.y=99..106,wishlist.z=-959}] at @s run tp @s ~29 ~ ~

schedule function traaa:tick_1t 1t
