Multitudes
==========

Provides random NPCs that populate city areas with a low hit to server
performance.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Random Tick](../random_tick) for update optimizations.
* Requires [Magic](../magic) for player UI
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The skip datapack provides five new items for the magic datapack:

### ![wand] Place Bait

* Blaces an NPC bait where NPCs may spawn,
* Can be obtained with the loot table `multitudes:place_bait`.

### ![wand] Remove Bait

* Removes all baits placed by the same player in a 2-block radius.
* Can be obtained with the loot table `multitudes:remove_bait`.

Details
-------

An NPC location is a marker tagged `wishlist.multitudes.bait`; if a player gets
at a range of 32 blocks from it, a random NPC will be generated at that
position, with the following chances:

* 15:64 chance of it being a villager
    * The villager type is chosen thus:
        * 2:64 chance of it being a desert villager
        * 2:64 chance of it being a jungle villager
        * 2:64 chance of it being a savanna villager
        * 2:64 chance of it being a snow villager
        * 2:64 chance of it being a swamp villager
        * 2:64 chance of it being a taiga villager
        * 2:64 chance of it being a plains villager
        * 50:64 chance of it being local to the current biome
    * The villager profession is chosen thus:
        * 2:64 chance of it being an armorer
        * 2:64 chance of it being a butcher
        * 2:64 chance of it being a cartographer
        * 2:64 chance of it being a cleric
        * 2:64 chance of it being a farmer
        * 2:64 chance of it being a fisherperson
        * 2:64 chance of it being a fletcher
        * 2:64 chance of it being a leatherworker
        * 2:64 chance of it being a librarian
        * 2:64 chance of it being a nitwit
        * 2:64 chance of it being a mason
        * 2:64 chance of it being a shepherd
        * 2:64 chance of it being a toolsmith
        * 2:64 chance of it being a weaponsmith
        * 16:64 chance of it being a jobless child
        * 34:64 chance of it being a jobless adult
* 1:64 chance of it being a wandering trader
* 16:64 chance of it being an illager
    * 4:64 chance of it being a pillager
    * 4:64 chance of it being a vindicator
    * 4:64 chance of it being an evoker
    * 4:64 chance of it being a witch
* 8:64 chance of it being a piglin
    * 4:64 chance of it being a regular piglin
    * 2:64 chance of it being a piglin baby
    * 2:64 chance of it being a piglin brute
* 2:64 chance of it being an enderfolk
* 2:64 chance of it being a dog or a chicken
* 20:64 chance of it being left empty

The NPC will remain loaded as-is until no players remain around it in a
48-block radius, then the NPC will be removed and the marker placed again.

Dependencies
------------

The [Magic Tools](../magic_tools) datapack provides a magic egg (Bait NPC) that
automates the creation of NPC location points.

Known Issues
------------

Some minecraft code makes entities "unable" to spawn by removing them *after*
they have spawned, but the spawn function still succeeds; this includes a
world being *peaceful* and it will cause NPCs to fail spawning *and* the bait
marker being removed.

Overhead
--------

Updates every three seconds:

* checks every entity in a 32-block radius around each player in search of npc
  locations to be populated.

Randomly updates one chunk per tick:

* checks every entity in the chunk in search of npcs outside the range of an
  existing player to remove them.

[wand]: resources/assets/wishlist/textures/item/wand.png ""
[ticket]: resources/assets/grabeltone/textures/item/ticket.png ""
