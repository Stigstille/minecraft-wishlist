# dismiss.mcfunction

scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
execute as @e[tag=wishlist.multitudes.npc,distance=..2] \
        at @s \
        run function multitudes:dismiss_1

# dismiss_1.mcfunction

execute store result score employer wishlist.vars int 1 \
        run data get entity @s ArmorItems[3].tag.employer
execute if score employer wishlist.vars = playerId wishlist.vars \
        run function multitudes:dismiss_2

# dismiss_2.mcfunction

function wishlist:dismiss
loot spawn ~ ~ ~ loot magic_tools:bait_npc
