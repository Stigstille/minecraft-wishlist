# add_2.mcfunction

data modify storage wishlist:args employer set from entity @s data.employer

function wishlist:rand
scoreboard players operation type wishlist.vars = rand wishlist.vars
scoreboard players operation type wishlist.vars %= 64 wishlist.vars

execute if score type wishlist.vars matches 0..14 \
        summon minecraft:villager \
        run function multitudes:add_villager
execute if score type wishlist.vars matches 15 \
        summon minecraft:wandering_trader \
        run function multitudes:add_wandering_trader
execute if score type wishlist.vars matches 16..19 \
        summon minecraft:pillager \
        run function multitudes:add_pillager
execute if score type wishlist.vars matches 20..23 \
        summon minecraft:vindicator \
        run function multitudes:add_vindicator
execute if score type wishlist.vars matches 24..27 \
        summon minecraft:evoker \
        run function multitudes:add_evoker
execute if score type wishlist.vars matches 28..31 \
        summon minecraft:witch \
        run function multitudes:add_witch
execute if score type wishlist.vars matches 32..35 \
        summon minecraft:piglin \
        run function multitudes:add_piglin
execute if score type wishlist.vars matches 36..37 \
        summon minecraft:piglin \
        run function multitudes:add_piglin_baby
execute if score type wishlist.vars matches 38..39 \
        summon minecraft:piglin_brute \
        run function multitudes:add_piglin_brute
execute if score type wishlist.vars matches 40..41 \
        summon minecraft:enderman \
        run function multitudes:add_enderfolk
execute if score type wishlist.vars matches 42 \
        summon minecraft:wolf \
        run function multitudes:add_wolf
execute if score type wishlist.vars matches 43 \
        summon minecraft:chicken \
        run function multitudes:add_chicken
execute if score type wishlist.vars matches 44.. \
        run function multitudes:add_marker

# Only remove if it has not become an npc marker
kill @s[tag=wishlist.multitudes.bait]
