# add_3.mcfunction

tag @s add wishlist.multitudes.npc
tag @s add wishlist.look_alive

data merge entity @s {NoAI:1b,Invulnerable:1b}

item replace entity @s weapon.mainhand with minecraft:air
item replace entity @s weapon.offhand with minecraft:air
item replace entity @s armor.chest with minecraft:air
item replace entity @s armor.feet with minecraft:air
item replace entity @s armor.legs with minecraft:air
item replace entity @s armor.head with minecraft:air

function wishlist:rand
scoreboard players operation rot wishlist.vars = rand wishlist.vars
scoreboard players operation rot wishlist.vars %= 360 wishlist.vars
execute store result entity @s Rotation[0] float 1 \
        run scoreboard players get rot wishlist.vars

function wishlist:recall
data modify storage wishlist:args memories.employer set from storage wishlist:args employer
function wishlist:memorize
