# add_villager.mcfunction

function wishlist:rand
scoreboard players operation m wishlist.vars = rand wishlist.vars
scoreboard players operation m wishlist.vars %= 64 wishlist.vars

execute if score m wishlist.vars matches 0..1 \
        run data merge entity @s {VillagerData:{type:"minecraft:desert"}}
execute if score m wishlist.vars matches 2..3 \
        run data merge entity @s {VillagerData:{type:"minecraft:jungle"}}
execute if score m wishlist.vars matches 4..5 \
        run data merge entity @s {VillagerData:{type:"minecraft:savanna"}}
execute if score m wishlist.vars matches 6..7 \
        run data merge entity @s {VillagerData:{type:"minecraft:snow"}}
execute if score m wishlist.vars matches 8..9 \
        run data merge entity @s {VillagerData:{type:"minecraft:swamp"}}
execute if score m wishlist.vars matches 10..11 \
        run data merge entity @s {VillagerData:{type:"minecraft:taiga"}}
execute if score m wishlist.vars matches 12..13 \
        run data merge entity @s {VillagerData:{type:"minecraft:plains"}}
execute if score m wishlist.vars matches 14.. \
        run function wishlist:skin_villager_by_biome

function wishlist:rand
scoreboard players operation m wishlist.vars = rand wishlist.vars
scoreboard players operation m wishlist.vars %= 64 wishlist.vars

execute if score m wishlist.vars matches 0..1 \
        run data merge entity @s {VillagerData:{profession:"minecraft:armorer"}}
execute if score m wishlist.vars matches 2..3 \
        run data merge entity @s {VillagerData:{profession:"minecraft:butcher"}}
execute if score m wishlist.vars matches 4..5 \
        run data merge entity @s {VillagerData:{profession:"minecraft:cartographer"}}
execute if score m wishlist.vars matches 6..7 \
        run data merge entity @s {VillagerData:{profession:"minecraft:cleric"}}
execute if score m wishlist.vars matches 8..9 \
        run data merge entity @s {VillagerData:{profession:"minecraft:farmer"}}
execute if score m wishlist.vars matches 10..11 \
        run data merge entity @s {VillagerData:{profession:"minecraft:fisherman"}}
execute if score m wishlist.vars matches 12..13 \
        run data merge entity @s {VillagerData:{profession:"minecraft:fletcher"}}
execute if score m wishlist.vars matches 14..15 \
        run data merge entity @s {VillagerData:{profession:"minecraft:leatherworker"}}
execute if score m wishlist.vars matches 16..17 \
        run data merge entity @s {VillagerData:{profession:"minecraft:librarian"}}
execute if score m wishlist.vars matches 18..19 \
        run data merge entity @s {VillagerData:{profession:"minecraft:nitwit"}}
execute if score m wishlist.vars matches 20..21 \
        run data merge entity @s {VillagerData:{profession:"minecraft:mason"}}
execute if score m wishlist.vars matches 22..23 \
        run data merge entity @s {VillagerData:{profession:"minecraft:shepherd"}}
execute if score m wishlist.vars matches 24..25 \
        run data merge entity @s {VillagerData:{profession:"minecraft:toolsmith"}}
execute if score m wishlist.vars matches 26..27 \
        run data merge entity @s {VillagerData:{profession:"minecraft:weaponsmith"}}
execute if score m wishlist.vars matches 28..43 \
        run data merge entity @s {Age:-999999999}
execute if score m wishlist.vars matches 44.. \
        run data merge entity @s {VillagerData:{profession:"minecraft:none"}}

function multitudes:add_3
function multitudes:add_4

