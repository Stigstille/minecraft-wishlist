# dismiss_bait/cast.mcfunction

scoreboard players set count wishlist.vars 0
scoreboard players operation playerId wishlist.vars = @s wishlist.playerId
execute as @e[tag=wishlist.multitudes.bait,distance=..2] \
        at @s \
        run function multitudes:remove_bait_1
execute as @e[tag=wishlist.multitudes.npc,distance=..2] \
        at @s \
        run function multitudes:remove_bait_2

title @s actionbar { \
        "translate": "%s baits removed", \
        "with": [{"score":{"name":"count","objective":"wishlist.vars"}}] \
}
