# remove.mcfunction

# This function is tagged at #random_ticks:tick

execute as @e[tag=wishlist.multitudes.npc,dx=16,dy=256,dz=16] \
        at @s \
        unless entity @a[distance=..48] \
        run function multitudes:remove_1
