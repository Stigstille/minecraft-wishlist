# load.mcfunction - initialize multitudes

scoreboard players set 64 wishlist.vars 64
scoreboard players set 360 wishlist.vars 360

data modify storage wishlist:args all_chatter append value \
        "The problem with big cities is that when you get too many people in the same place, they can't all move around like hopped up slimes"
data modify storage wishlist:args all_chatter append value \
        "Time! The problem is time! the World always needs more of it"

function multitudes:remove
function multitudes:add
