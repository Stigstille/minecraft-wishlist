
execute store result score employer wishlist.vars \
        run data get entity @s data.employer
execute unless score employer wishlist.vars = playerId wishlist.vars run return fail
function wishlist:dismiss
scoreboard players add count wishlist.vars 1

