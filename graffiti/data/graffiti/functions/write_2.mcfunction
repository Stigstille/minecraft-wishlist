

data modify storage wishlist:args args set from entity @s SelectedItem.tag.wishlist_marker
data modify storage wishlist:args args.text set from entity @s SelectedItem.tag.pages[0]
execute store result score yaw wishlist.vars run data get entity @s Rotation[0] 1000
execute store result storage wishlist:args args.yaw float 0.001 \
        run scoreboard players add yaw wishlist.vars 180000
execute if score steps wishlist.vars matches 50.. \
        run data modify storage wishlist:args args.scale set value 0.5f
execute if score steps wishlist.vars matches 40..49 \
        run data modify storage wishlist:args args.scale set value 1.0f
execute if score steps wishlist.vars matches 30..39 \
        run data modify storage wishlist:args args.scale set value 2.0f
execute if score steps wishlist.vars matches 20..29 \
        run data modify storage wishlist:args args.scale set value 3.0f
execute if score steps wishlist.vars matches ..19 \
        run data modify storage wishlist:args args.scale set value 4.0f

function graffiti:write_3 with storage wishlist:args args
advancement grant @s only wishlist:use_marker
