
advancement revoke @s only graffiti:write
execute unless data entity @s SelectedItem.tag.pages run return fail
execute store result score len wishlist.vars \
        run data get entity @s SelectedItem.tag.pages[0]
execute if score len wishlist.vars matches 0 run return fail
scoreboard players set steps wishlist.vars 60
execute anchored eyes positioned ^ ^ ^ run function graffiti:write_1
item modify entity @s weapon.mainhand graffiti:reset
