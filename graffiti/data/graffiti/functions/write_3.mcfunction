
$execute positioned ~0.1 ~ ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[-90.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~-0.1 ~ ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[90.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~0.1 ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[$(yaw)f,-90.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~-0.1 ~ \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[$(yaw)f,90.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~ ~0.1 \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[0.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
$execute positioned ~ ~ ~-0.1 \
        if block ~ ~ ~ minecraft:air \
        run return run summon minecraft:text_display ~ ~ ~ { \
                text:"{\"text\":\"$(text)\",\"color\":\"$(color)\",\"bold\":$(bold)}", \
                Tags:[wishlist.graffiti], \
                Rotation:[180.0f,0.0f], \
                background:0, \
                transformation:[$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,$(scale)f,0f,0f,0f,0f,1f] \
        }
