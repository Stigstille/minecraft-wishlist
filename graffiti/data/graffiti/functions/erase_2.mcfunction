
execute if score steps wishlist.vars matches 50.. \
        run return run kill @e[tag=wishlist.graffiti,distance=..0.5]
execute if score steps wishlist.vars matches 40..49 \
        run return run kill @e[tag=wishlist.graffiti,distance=..1]
execute if score steps wishlist.vars matches 30..39 \
        run return run kill @e[tag=wishlist.graffiti,distance=..2]
execute if score steps wishlist.vars matches 20..29 \
        run return run kill @e[tag=wishlist.graffiti,distance=..3]
execute if score steps wishlist.vars matches ..19 \
        run return run kill @e[tag=wishlist.graffiti,distance=..4]
return fail
