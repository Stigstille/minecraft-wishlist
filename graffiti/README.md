Graffiti
========

Provides a marker item that allows writing on walls, floors and ceilings.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for player UI
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

### ![white_marker] ![light_gray_marker] ![gray_marker] ![black_marker] ![brown_marker] ![red_marker] ![orange_marker] ![yellow_marker] ![lime_marker] ![green_marker] ![cyan_marker] ![light_blue_marker] ![blue_marker] ![purple_marker] ![magenta_marker] ![pink_marker] Marker

When a player clicks with a magic marker on any writable surface, they will be
prompted with a writable book interface; when the player clicks "done" the
marker will be spent and the text placed on the surface the player is looking
at.

The size of the text being written depends on the distance from the player's
eyes to the point where it's being written:

* 0 blocks: text will be half-size,
* 1 block: text will be full-size,
* 2 blocks: text will be double-size,
* 3 blocks: text will be triple-size,
* 4 blocks: text will be quadruple-size.

Markers come in 32 variants with 16 colors and two thicknesses; each one has
its own loot table:

* `graffiti:bold_black_marker`
* `graffiti:bold_blue_marker`
* `graffiti:bold_brown_marker`
* `graffiti:bold_cyan_marker`
* `graffiti:bold_gray_marker`
* `graffiti:bold_green_marker`
* `graffiti:bold_light_blue_marker`
* `graffiti:bold_light_gray_marker`
* `graffiti:bold_lime_marker`
* `graffiti:bold_magenta_marker`
* `graffiti:bold_orange_marker`
* `graffiti:bold_pink_marker`
* `graffiti:bold_purple_marker`
* `graffiti:bold_red_marker`
* `graffiti:bold_white_marker`
* `graffiti:bold_yellow_marker`
* `graffiti:thin_black_marker`
* `graffiti:thin_blue_marker`
* `graffiti:thin_brown_marker`
* `graffiti:thin_cyan_marker`
* `graffiti:thin_gray_marker`
* `graffiti:thin_green_marker`
* `graffiti:thin_light_blue_marker`
* `graffiti:thin_light_gray_marker`
* `graffiti:thin_lime_marker`
* `graffiti:thin_magenta_marker`
* `graffiti:thin_orange_marker`
* `graffiti:thin_pink_marker`
* `graffiti:thin_purple_marker`
* `graffiti:thin_red_marker`
* `graffiti:thin_white_marker`
* `graffiti:thin_yellow_marker`

### ![eraser] Eraser

Using the eraser on a surface will remove neighbouring graffiti; the graffiti
removed depends on the distance of the player to the surface they clicked on:

* 0 blocks: 0.5-block radius
* 1 block: 1-block radius
* 2 blocks: 2-block radius
* 3 blocks: 3-block radius
* 4 blocks: 4-block radius

The eraser can be obtained with the `graffiti:eraser` loot table.

Known Issues
------------

Graffiti text can't include double quote characters ("); it would break the
macro that builds the text display

Signing the book interface that appears when using the marker is not handled by
the code, and can have any effect.

Overhead
--------

Uses an advancement to check if a  player modified a book with the tag
identifying a marker in their inventory; if so the rest of the algorithm runs.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Wordsmith: use a marker to create text
  * Words in the Wind: use a eraser to destroy text

[eraser]: resources/assets/wishlist/textures/item/eraser.png ""
[white_marker]: resources/assets/wishlist/textures/item/white_marker.png ""
[light_gray_marker]: resources/assets/wishlist/textures/item/light_gray_marker.png ""
[gray_marker]: resources/assets/wishlist/textures/item/gray_marker.png ""
[black_marker]: resources/assets/wishlist/textures/item/black_marker.png ""
[brown_marker]: resources/assets/wishlist/textures/item/brown_marker.png ""
[red_marker]: resources/assets/wishlist/textures/item/red_marker.png ""
[orange_marker]: resources/assets/wishlist/textures/item/orange_marker.png ""
[yellow_marker]: resources/assets/wishlist/textures/item/yellow_marker.png ""
[lime_marker]: resources/assets/wishlist/textures/item/lime_marker.png ""
[green_marker]: resources/assets/wishlist/textures/item/green_marker.png ""
[cyan_marker]: resources/assets/wishlist/textures/item/cyan_marker.png ""
[light_blue_marker]: resources/assets/wishlist/textures/item/light_blue_marker.png ""
[blue_marker]: resources/assets/wishlist/textures/item/blue_marker.png ""
[purple_marker]: resources/assets/wishlist/textures/item/purple_marker.png ""
[magenta_marker]: resources/assets/wishlist/textures/item/magenta_marker.png ""
[pink_marker]: resources/assets/wishlist/textures/item/pink_marker.png ""
