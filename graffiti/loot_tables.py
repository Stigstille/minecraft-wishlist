
import json

colors = {
        "white":["#f9fffe", 271828],
        "light_gray":["#9d9d97", 271829],
        "gray":["#474f52", 271830],
        "black":["#1d1d21", 271831],
        "brown":["#835432", 271832],
        "red":["#b02e26", 271833],
        "orange":["#f9801d", 271834],
        "yellow":["#fed83d", 271835],
        "lime":["#80c71f", 271836],
        "green":["#5e7c16", 271837],
        "cyan":["#169c9c", 271838],
        "light_blue":["#3ab3da", 271839],
        "blue":["#3c44aa", 271840],
        "purple":["#8932b8", 271841],
        "magenta":["#c74ebd", 271842],
        "pink":["#f38baa", 271843],
        }

boldness = {
        "thin":"false",
        "bold":"true",
        }

for c in colors:
    for b in boldness:
        with open("data/graffiti/loot_tables/{}_{}_marker.json".format(b, c), 'w') as f:
            nbt = json.dumps({
                "CustomModelData":colors[c][1],
                "wishlist_marker":{
                    "color":colors[c][0],
                    "bold":boldness[b]
                }
            });
            f.write(json.dumps({
              "pools": [
                {
                  "rolls": 1,
                  "entries": [
                    {
                      "type": "minecraft:item",
                      "name": "minecraft:writable_book",
                      "functions": [
                        {
                          "function": "minecraft:set_name",
                          "name": {
                            "translate":"wishlist.graffiti.{}_{}_marker.name".format(b,c),
                            "color": "white",
                            "italic": False
                          }
                        },
                        {
                          "function": "minecraft:set_nbt",
                          "tag": nbt
                        }
                      ]
                    }
                  ]
                }
              ]
            }))
