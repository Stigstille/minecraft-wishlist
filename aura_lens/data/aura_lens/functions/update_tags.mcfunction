# true_sight/update_tags.mcfunction

tag @s remove wishlist.hurt
execute store result score cur wishlist.vars run data get entity @s Health
execute store result score max wishlist.vars run attribute @s minecraft:generic.max_health get
execute if score cur wishlist.vars < max wishlist.vars run tag @s add wishlist.hurt
execute if entity @s[type=villager,nbt=!{NoAI:1b}] run function aura_lens:update_villager_tags

