
effect give @s minecraft:glowing 2 0 true

execute if entity @s[tag=wishlist.hurt] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:dust 1 0.4 0.4 1 ~ ~ ~ 0.2 0.2 0.2 0 5

execute if entity @s[tag=wishlist.homeless] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:dust 0.4 1 0.4 1 ~ ~ ~ 0.2 0.2 0.2 0 5

execute if entity @s[tag=wishlist.jobless] \
        at @s \
        anchored eyes \
        positioned ^ ^ ^ \
        run particle minecraft:dust 0.4 0.4 1 1 ~ ~ ~ 0.2 0.2 0.2 0 5
