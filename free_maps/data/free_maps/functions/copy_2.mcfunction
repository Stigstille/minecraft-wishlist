
# free_maps:copy_2
data modify storage wishlist:args item set from entity @s Item
execute store result score rot wishlist.vars run data get entity @s ItemRotation
scoreboard players remove rot wishlist.vars 1
execute store result entity @s ItemRotation byte 1 \
        run scoreboard players operation rot wishlist.vars %= 8 wishlist.vars
return 1
