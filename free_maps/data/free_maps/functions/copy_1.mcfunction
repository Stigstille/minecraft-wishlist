
# free_maps:copy_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ minecraft:air run return fail
execute as @e[type=#wishlist:item_frames,distance=..0.5] run return run function free_maps:copy_2
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run return run function free_maps:copy_1
