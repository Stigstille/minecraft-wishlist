
# free_maps:copy
advancement revoke @s only free_maps:copy
scoreboard players set steps wishlist.vars 60
execute anchored eyes \
        positioned ^ ^ ^ \
        if function free_maps:copy_1 \
        run function free_maps:copy_3 with storage wishlist:args item


