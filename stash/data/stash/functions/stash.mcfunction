# stash.mcfunction

data modify storage wishlist:args stash set from entity @s {}
summon minecraft:armor_stand ~ ~ ~ {Tags:[wishlist.stash]}
ride @s mount @e[tag=wishlist.stash,distance=0,limit=1]
data modify storage wishlist:args stash.id \
        set from entity @e[tag=wishlist.stash,distance=0,limit=1] Passengers[0].id
kill @e[tag=wishlist.stash,distance=0,limit=1]

