# pop.mcfunction

scoreboard players set depth wishlist.vars 0
data modify storage wishlist:args stack set value []
data modify storage wishlist:args stack append from storage wishlist:args stash
function stash:pop_1 with storage wishlist:args stack[-1]
tag @e[distance=0] remove wishlist.stash.child

