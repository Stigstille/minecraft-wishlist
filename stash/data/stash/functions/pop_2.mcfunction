# pop_2.mcfunction

data remove storage wishlist:args stack[-1].Pos
data remove storage wishlist:args stack[-1].UUID
data modify entity @s {} merge from storage wishlist:args stack[-1]
execute if score depth wishlist.vars matches ..2 \
        if data storage wishlist:args stack[-1].Passengers[0] \
        run function stash:pop_3
tag @s add wishlist.stash.child

