Stash
=====

Provide functions to serialize and deserialize any non-player entity to and
from NBT data, including recursively handling passengers.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.

Usage
-----

To stash an entity at the `stash` tag of the storage `wishlist:args` run:

```
execute as <entity> run function stash:stash
```

To summon a copy of the entity stashed at `stash` of the storage
`wishlist:args` run:

```
function stash:pop
```

The summoned entity (or entities, if there were passengers) will retain all
data except for their position and UUID.

Overhead
--------

During the popping process, spawns markers in order to perform string
comparison and determine what kind of entity is stashed and should be summoned.

Known Issues
------------

In order to generate the `id` tag for the stashed entity, it will be made to
mount a temporary armor stand and then inmediately dismount it; this means that
stashing an entity that is riding something else will dismount it.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:


