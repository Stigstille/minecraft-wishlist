Looping Music
=============

Allows playing looping music for players

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

This is a technical datapack used as dependency for others; it provides two
functions:

`looping_music:play` will play a sound for a player for a number of seconds,
then loop it:

```
looping_music:play {track:<name>,ttl:<seconds>,volume:<float>,pitch:<float>,minVolume:<float>}
```

`looping_music:stop` will stop the sound being played for the targeted player:

```
looping_music:stop
```

Overhead
--------

Checks a score for every player every second to see if a music should loop.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

