
# looping_music:play_1
$data remove storage wishlist:looping_music players[{id:$(id)}]
data modify storage wishlist:looping_music players append from storage wishlist:args args
scoreboard players set @s wishlist.ttlMusic 0

$tellraw @a[tag=wishlist.debug] { \
        "translate":"[looping_music] Play %s for %s", \
        "with": ["$(track)","$(id)"], \
        "color": "yellow" \
}
