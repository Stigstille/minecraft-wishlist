
# looping_music:play
function looping_music:stop
$data modify storage wishlist:args args set value \
        {track:"$(track)",ttl:$(ttl),volume:$(volume),pitch:$(pitch),minVolume:$(minVolume)}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function looping_music:play_1 with storage wishlist:args args
