
# looping_music:update
execute if score @s wishlist.ttlMusic matches -1 \
        run return fail
stopsound @s music
execute unless score @s wishlist.ttlMusic matches 0 \
        run scoreboard players remove @s wishlist.ttlMusic 1
execute unless score @s wishlist.ttlMusic matches 0 \
        run return 0
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function looping_music:update_1 with storage wishlist:args args
