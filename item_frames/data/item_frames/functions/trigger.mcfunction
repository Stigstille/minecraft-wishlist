# trigger.mcfunction

execute if score @s if_trigger matches 1 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[type=item_frame,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 2 \
        as @e[type=item_frame,distance=..1] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 3 \
        as @e[type=item_frame,distance=..2] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 4 \
        as @e[type=item_frame,distance=..4] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 5 \
        as @e[type=item_frame,distance=..8] \
        run data merge entity @s {Invisible:1b}

execute if score @s if_trigger matches 11 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[type=item_frame,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 12 \
        as @e[type=item_frame,distance=..1] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 13 \
        as @e[type=item_frame,distance=..2] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 14 \
        as @e[type=item_frame,distance=..4] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 15 \
        as @e[type=item_frame,distance=..8] \
        run data merge entity @s {Invisible:0b}

execute if score @s if_trigger matches 21 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[type=glow_item_frame,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 22 \
        as @e[type=glow_item_frame,distance=..1] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 23 \
        as @e[type=glow_item_frame,distance=..2] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 24 \
        as @e[type=glow_item_frame,distance=..4] \
        run data merge entity @s {Invisible:1b}
execute if score @s if_trigger matches 25 \
        as @e[type=glow_item_frame,distance=..8] \
        run data merge entity @s {Invisible:1b}

execute if score @s if_trigger matches 31 \
        anchored eyes \
        positioned ^ ^ ^1 \
        as @e[type=glow_item_frame,distance=..8,sort=nearest,limit=1] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 32 \
        as @e[type=glow_item_frame,distance=..1] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 33 \
        as @e[type=glow_item_frame,distance=..2] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 34 \
        as @e[type=glow_item_frame,distance=..4] \
        run data merge entity @s {Invisible:0b}
execute if score @s if_trigger matches 35 \
        as @e[type=glow_item_frame,distance=..8] \
        run data merge entity @s {Invisible:0b}

execute if score @s if_trigger matches 41 \
        anchored eyes \
        positioned ^ ^ ^1 \
        at @e[type=glow_item_frame,distance=..8,sort=nearest,limit=1] \
        if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=15]
execute if score @s if_trigger matches 42 \
        run execute at @e[type=glow_item_frame,distance=..1] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=15]
execute if score @s if_trigger matches 43 \
        run execute at @e[type=glow_item_frame,distance=..2] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=15]
execute if score @s if_trigger matches 44 \
        run execute at @e[type=glow_item_frame,distance=..4] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=15]
execute if score @s if_trigger matches 45 \
        run execute at @e[type=glow_item_frame,distance=..8] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=15]

execute if score @s if_trigger matches 51 \
        anchored eyes \
        positioned ^ ^ ^1 \
        at @e[type=glow_item_frame,distance=..8,sort=nearest,limit=1] \
        if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=11]
execute if score @s if_trigger matches 52 \
        run execute at @e[type=glow_item_frame,distance=..1] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=11]
execute if score @s if_trigger matches 53 \
        run execute at @e[type=glow_item_frame,distance=..2] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=11]
execute if score @s if_trigger matches 54 \
        run execute at @e[type=glow_item_frame,distance=..4] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=11]
execute if score @s if_trigger matches 55 \
        run execute at @e[type=glow_item_frame,distance=..8] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=11]

execute if score @s if_trigger matches 61 \
        anchored eyes \
        positioned ^ ^ ^1 \
        at @e[type=glow_item_frame,distance=..8,sort=nearest,limit=1] \
        if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=7]
execute if score @s if_trigger matches 62 \
        run execute at @e[type=glow_item_frame,distance=..1] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=7]
execute if score @s if_trigger matches 63 \
        run execute at @e[type=glow_item_frame,distance=..2] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=7]
execute if score @s if_trigger matches 64 \
        run execute at @e[type=glow_item_frame,distance=..4] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=7]
execute if score @s if_trigger matches 65 \
        run execute at @e[type=glow_item_frame,distance=..8] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ light[level=7]

execute if score @s if_trigger matches 71 \
        anchored eyes \
        positioned ^ ^ ^1 \
        at @e[type=glow_item_frame,distance=..8,sort=nearest,limit=1] \
        if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ air
execute if score @s if_trigger matches 72 \
        run execute at @e[type=glow_item_frame,distance=..1] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ air
execute if score @s if_trigger matches 73 \
        run execute at @e[type=glow_item_frame,distance=..2] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ air
execute if score @s if_trigger matches 74 \
        run execute at @e[type=glow_item_frame,distance=..4] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ air
execute if score @s if_trigger matches 75 \
        run execute at @e[type=glow_item_frame,distance=..8] \
        run execute if block ~ ~ ~ #item_frames:empty \
        run setblock ~ ~ ~ air

advancement grant @s only wishlist:use_frame_book
