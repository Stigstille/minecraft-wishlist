# tick.mcfunction - execute command for players with if_trigger score

schedule function item_frames:tick 1s

execute as @a[scores={if_trigger=1..}] at @s run function item_frames:trigger
scoreboard players enable @a if_trigger
scoreboard players set @a if_trigger 0

execute at @a run function item_frames:pop_empty_invisible_item_frames
execute at @a run function item_frames:pop_empty_invisible_glow_item_frames

