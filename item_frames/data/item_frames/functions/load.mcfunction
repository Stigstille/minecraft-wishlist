# load.mcfunction - initialize datapack

scoreboard objectives add if_trigger trigger

data modify storage wishlist:args all_chatter append value \
        "You can have enough stone, or iron, or diamond, but you never have enough time"
data modify storage wishlist:args all_chatter append value \
        "Ever seen one of those Tool Books? those things are spooky"

function item_frames:tick
