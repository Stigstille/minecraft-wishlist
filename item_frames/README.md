Item Frames
===========

Provide utilities to trigger invisibility in both item frames and glow item
frames. A low-level API to the datapack is provided through the `if_trigger`
scoreboard, and a high-level API is provided through a book that can be
acquired with the `item_frames:book` loot table.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

Each line in the book is on the form:

```
[1 2 4 8] action
```

Where `action` refers to the action to be performed and the numbers are
clickable elements that perform that action on a number of item frames:

* `1` targets the item frame nearest to the player's face but no further than
  8 blocks,
* `2` targets every item frame up to a 2-block distance from the player's feet,
* `4` targets every item frame up to a 4-block distance from the player's feet,
* `8` targets every item frame up to a 8-block distance from the player's feet.

Note that actions targeting regular and glow item frames are separate.

If you hover the cursor over the `action` a short explanation will pop up.

### Actions on regular item frames

* `visible` makes item frames visible,
* `invisible` makes item frames invisible.

### Actions on glow item frames

* `visible` makes item frames visible,
* `invisible` makes item frames invisible,
* `bright` replaces empty blocks occupied by item frames with `minecraft:light[level=15]`,
* `soft` replaces empty blocks occupied by item frames with `minecraft:light[level=11]`,
* `dim` replaces empty blocks occupied by item frames with `minecraft:light[level=7]`,
* `dark` replaces empty blocks occupied by item frames with `minecraft:air`.

In order to prevent lag in servers, empty invisible frames are removed by the
datapack.

Overhead
--------

Updates every second:

* checks every player to see if they have used the Item Frames book,
* checks every entity in an 8-block radius around each player to see if it is
  an empty, invisible item frame; to remove it.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Custom Framework: trigger the `if_trigger` score
