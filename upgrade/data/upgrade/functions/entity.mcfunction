
execute as @s[tag=wishlist.globe] \
        on passengers \
        unless data entity @s item.tag.wishlist_globe \
        run data modify entity @s item.tag.wishlist_globe set value {radius:16,zoom:2}

execute as @s[tag=wishlist.better_gossip.radio,tag=!wishlist.custom_block] \
        run function upgrade:radio

execute as @s[tag=wishlist.cooking_stations] at @s \
        run function upgrade:cooking_stations

execute as @s[tag=wishlist.magic_markers.dry] \
        run data modify entity @s Tags set value [wishlist.graffiti]

execute as @s[tag=wishlist.hired_trader] \
        unless data entity @s ArmorItems[3].tag.CustomModelData \
        run data modify entity @s ArmorItems[3].tag.CustomModelData set value 271828

