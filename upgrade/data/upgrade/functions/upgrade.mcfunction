
execute if predicate upgrade:aura_lens \
        run loot replace entity @s weapon.mainhand loot aura_lens:aura_lens

execute if predicate upgrade:astrolabe \
        run loot replace entity @s weapon.mainhand loot astrolabe:astrolabe

execute if predicate upgrade:noise_box \
        run loot replace entity @s weapon.mainhand loot noise_box:noise_box

execute if predicate upgrade:wand run function upgrade:magic
execute if predicate upgrade:ticket run function upgrade:magic
execute if predicate upgrade:bauble run function upgrade:magic
execute if predicate upgrade:grimoire run function upgrade:grimoire

execute if predicate upgrade:white_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_white_marker
execute if predicate upgrade:light_gray_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_light_gray_marker
execute if predicate upgrade:gray_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_gray_marker
execute if predicate upgrade:black_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_black_marker
execute if predicate upgrade:brown_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_brown_marker
execute if predicate upgrade:red_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_red_marker
execute if predicate upgrade:orange_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_orange_marker
execute if predicate upgrade:yellow_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_yellow_marker
execute if predicate upgrade:lime_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_lime_marker
execute if predicate upgrade:green_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_green_marker
execute if predicate upgrade:cyan_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_cyan_marker
execute if predicate upgrade:light_blue_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_light_blue_marker
execute if predicate upgrade:blue_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_blue_marker
execute if predicate upgrade:purple_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_purple_marker
execute if predicate upgrade:magenta_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_magenta_marker
execute if predicate upgrade:pink_marker \
        run loot replace entity @s weapon.mainhand loot graffiti:thin_pink_marker

execute if predicate upgrade:chest_key run function upgrade:chest_key

