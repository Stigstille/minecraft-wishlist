

data modify storage wishlist:args item set from entity @s SelectedItem
execute unless data storage wishlist:args item.tag.wishlist run return fail
data modify storage wishlist:args item.tag.wishlist_magic \
        set from storage wishlist:args item.tag.wishlist.magic
data modify storage wishlist:args item.tag.wishlist_one_use \
        set from storage wishlist:args item.tag.wishlist.one_use
data modify storage wishlist:args item.tag.wishlist_bauble \
        set from storage wishlist:args item.tag.wishlist.bauble
data remove storage wishlist:args item.tag.wishlist
function upgrade:item with storage wishlist:args item
title @s actionbar "Upgraded magic tool"
