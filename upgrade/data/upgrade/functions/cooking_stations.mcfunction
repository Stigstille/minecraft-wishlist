

execute on passengers if entity @s[tag=wishlist.cooking_stations.chop] \
        run function custom_blocks:place_1 {id:sink}
execute on passengers if entity @s[tag=wishlist.cooking_stations.oven] \
        run function custom_blocks:place_1 {id:oven}
execute on passengers if entity @s[tag=wishlist.cooking_stations.plate] \
        run function custom_blocks:place_1 {id:cooktop}
execute on passengers if entity @s[tag=wishlist.cooking_stations.press] \
        run function custom_blocks:place_1 {id:oven}
execute on passengers if entity @s[tag=wishlist.cooking_stations.rest] \
        run function custom_blocks:place_1 {id:rack}
execute on passengers if entity @s[tag=wishlist.cooking_stations.ripen] \
        run function custom_blocks:place_1 {id:cask}

function wishlist:dismiss
