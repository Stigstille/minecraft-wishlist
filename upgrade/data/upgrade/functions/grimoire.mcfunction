

data modify storage wishlist:args item set from entity @s SelectedItem
execute unless data storage wishlist:args item.tag.wishlist run return fail
data modify storage wishlist:args item.tag.wishlist_grimoire set value []
function upgrade:grimoire_1
data remove storage wishlist:args item.tag.wishlist
function upgrade:item with storage wishlist:args item
title @s actionbar "Upgraded magic book"



