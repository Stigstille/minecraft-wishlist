
execute unless data storage wishlist:args item.tag.wishlist.grimoire[0] run return 0
function upgrade:grimoire_2
data modify storage wishlist:args item.tag.wishlist_grimoire \
        append from storage wishlist:args item.tag.wishlist.grimoire[0]
data remove storage wishlist:args item.tag.wishlist.grimoire[0]
function upgrade:grimoire_1
