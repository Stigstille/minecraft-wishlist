




execute unless data storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist run return fail
data modify storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist_magic \
        set from storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist.magic
data modify storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist_one_use \
        set from storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist.one_use
data modify storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist_bauble \
        set from storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist.bauble
data remove storage wishlist:args item.tag.wishlist.grimoire[0].tag.wishlist

