
function wishlist:rotate_random
data modify storage wishlist:args name set from storage wishlist:args elems[0]

data modify storage wishlist:args elems set from storage wishlist:args npc_titles
function wishlist:rotate_random
data modify storage wishlist:args title set from storage wishlist:args elems[0]

setblock 0 0 0 minecraft:oak_sign
data modify block 0 0 0 front_text.messages[0] set value '[{"storage":"wishlist:args","nbt":"title"},{"storage":"wishlist:args","nbt":"name"}]'
data modify entity @s CustomName set from block 0 0 0 front_text.messages[0]
setblock 0 0 0 minecraft:air

