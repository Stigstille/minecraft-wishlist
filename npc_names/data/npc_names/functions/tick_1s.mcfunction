
schedule function npc_names:tick_1s 1s

execute at @a \
        as @e[type=#wishlist:villagers,distance=..8] \
        unless data entity @s CustomName \
        run function npc_names:name_villager

execute at @a \
        as @e[type=#wishlist:illagers,distance=..8] \
        unless data entity @s CustomName \
        run function npc_names:name_illager

execute at @a \
        as @e[type=#wishlist:piglins,distance=..8] \
        unless data entity @s CustomName \
        run function npc_names:name_piglin
