Skip
====

Utilities to "skip" (teleport) across the world.

Unlike other datapacks and mods that achieve this through commands, Skip adds
actual useable items to the game that do the teleporting in different ways.

Skipping players produce particles and sound just like enderfolk.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for magic tool framework
* Requires [Custom Blocks](../custom_blocks) for the map core custom block
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

The skip datapack provides five new items for the magic datapack and one custom
block:

### ![wand] Skip Home

* Teleports the player to their current respawn point.
* Can be obtained with the loot table `skip:skip_home`.

### ![wand] Skip Back

* Teleports the player to the last place they teleported from or the last place
  they died on, whichever was most recent.
* Can be obtained with the loot table `skip:skip_home`.

### ![wand] Skip to Coords

* Generates a ![ticket] ticket that will teleport any player to the place where it was generated.
* Can be obtained with the loot table `skip:skip_coords`.

### ![wand] Skip to Player

* Generates a ![ticket] ticket that will teleport any player to the player who generated it.
* Can be obtained with the loot table `skip:skip_player`.
* Skip will fail if the player is offline or [hidden](../noise_box).

### ![wand] Skip to Map

* When used in a map hall, teleports the player to the corresponding place on a floor map
* Can be obtained with the loot table `skip:skip_map`.

### Map Core

* Custom block obtainable with `function custom_blocks:give {id:map_core}`
* When placed, it makes the surrounding area into a map hall.
  * Default radius of the map hall is 16 blocks from the core
    * This is not configurable (yet).
  * Default zoom of the maps expected by the hall is level 2
    * You can configure the actual zoom by using the map core block
* The map corresponding to the region where the core is should be in the same
  xz coordinates as the map core.
* Using ![wand] Skip to Map around the core skips to the corresponding place
  the map shows
* Or you can just crouch and jump on the place of the map you want to skip to.

Overhead
--------

Uses the hooks created by [Magic](../magic) and causes no further overhead.

Known Issues
------------

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Yippee Skippy!: skip through the world for the first time
  * A Place To Call Home: print a ticket to skip to a place
  * Where The Heart Is: print a ticket to skip to a player
  * Citizen Of The World: create a map hall

[wand]: resources/assets/wishlist/textures/item/wand.png ""
[ticket]: resources/assets/wishlist/textures/item/ticket.png ""
