
advancement revoke @s only skip:configure_map_core

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function skip:configure_map_core_1

execute if score done wishlist.vars matches 0 \
        run return run title @s actionbar {"text":"[skip] Unknown Error"}

title @s actionbar { \
        "translate": "Zoom level set to %s", \
        "with": [{"score":{"name":"zoom","objective":"wishlist.vars"}}] \
}


