
data modify storage wishlist:args args set value {}
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:back_1 with storage wishlist:args args
execute if score done wishlist.vars matches 0 \
        run return run title @s actionbar "Nowhere to skip back to"
function skip:skip with storage wishlist:args skip

