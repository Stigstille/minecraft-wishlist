
$data modify storage wishlist:args ticket set value { \
        name: '{"translate":"wishlist.skip.skip_to.name","italic":false}', \
        lore: ['{"text":"$(dimension) $(x) $(y) $(z)","italic":false,"color":"gray"}'], \
        magic: { id:"skip:coords", args:{dimension:"$(dimension)",x:$(x),y:$(y),z:$(z)} } \
}

loot spawn ~ ~ ~ loot magic:ticket
advancement grant @s only wishlist:use_skip_coords
