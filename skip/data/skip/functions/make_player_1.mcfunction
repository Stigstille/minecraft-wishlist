
$data modify storage wishlist:args ticket set value { \
        name: '{"translate":"wishlist.skip.skip_to.name","italic":false}', \
        lore: ['{"text":"player #$(id)","italic":false,"color":"gray"}'], \
        magic: { id:"skip:player", args:{id:$(id)} } \
}

loot spawn ~ ~ ~ loot magic:ticket
advancement grant @s only wishlist:use_skip_player

