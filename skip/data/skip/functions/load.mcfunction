
scoreboard objectives add wishlist.dead deathCount

execute unless data storage wishlist:skip back \
        run data modify storage wishlist:skip back set value []

data modify storage wishlist:skip maps set value [ \
        { scale:128, off1:64, off2:0, }, \
        { scale:256, off1:64, off2:64, }, \
        { scale:512, off1:64, off2:192, }, \
        { scale:1024, off1:64, off2:448, }, \
        { scale:2048, off1:64, off2:960, }, \
]

data modify storage wishlist:custom_blocks map_core set value { \
        interaction: { \
                Tags: [wishlist.globe], \
                width: 0.8, \
                height: 0.8 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271845, \
                      display:{Name:'{"text":"Map Core","italic":false}'} \
              } \
        }, \
        block: "minecraft:air" \
}

function skip:tick_1t
