
$tellraw @a[tag=wishlist.debug] { \
        "text": "[skip] $(id) to $(dimension) $(x) $(y) $(z)", \
        "color": "yellow" \
}
function skip:record
$data modify storage wishlist:skip back[{id:$(id)}] merge from storage wishlist:args skip
execute at @s run function skip:effect
$execute in $(dimension) run tp @s $(x) $(y) $(z)
tag @s add wishlist.skip
schedule function skip:fix 1t
execute at @s run function skip:effect
advancement grant @s only wishlist:use_skip
