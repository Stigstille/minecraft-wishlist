
# skip:map_3
function skip:record
execute at @s run function skip:effect
$execute store success score done wishlist.vars run spreadplayers $(x) $(z) 1 1 false @s
execute at @s run function skip:effect
execute if score done wishlist.vars matches 0 \
        run return run title @p actionbar {"text":"You can't skip to water, lava, fire, or void"}
$data modify storage wishlist:skip back[{id:$(id)}] merge from storage wishlist:args skip
advancement grant @s only wishlist:use_skip
