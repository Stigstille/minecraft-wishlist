
# skip:map_2
# calculate the coordinates of the northwest corner of the map
$scoreboard players set scale wishlist.vars $(scale)

execute store result score dx wishlist.vars run data get storage wishlist:args args.pos[0] 1
$scoreboard players add dx wishlist.vars $(off1)
scoreboard players operation dx wishlist.vars /= scale wishlist.vars
scoreboard players operation dx wishlist.vars *= scale wishlist.vars
$scoreboard players add dx wishlist.vars $(off2)

execute store result score dz wishlist.vars run data get storage wishlist:args args.pos[2] 1
$scoreboard players add dz wishlist.vars $(off1)
scoreboard players operation dz wishlist.vars /= scale wishlist.vars
scoreboard players operation dz wishlist.vars *= scale wishlist.vars
$scoreboard players add dz wishlist.vars $(off2)

$execute store result score x0 wishlist.vars run data get storage wishlist:args args.pos[0] $(scale)
$execute store result score z0 wishlist.vars run data get storage wishlist:args args.pos[2] $(scale)

$execute store result score x1 wishlist.vars run data get entity @s Pos[0] $(scale)
$execute store result score z1 wishlist.vars run data get entity @s Pos[2] $(scale)

scoreboard players operation x1 wishlist.vars -= x0 wishlist.vars
scoreboard players operation z1 wishlist.vars -= z0 wishlist.vars
execute store result storage wishlist:args args.x int 1 \
        run scoreboard players operation x1 wishlist.vars += dx wishlist.vars
execute store result storage wishlist:args args.z int 1 \
        run scoreboard players operation z1 wishlist.vars += dz wishlist.vars
execute store result storage wishlist:args args.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:map_3 with storage wishlist:args args

