

execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers \
        store result score zoom wishlist.vars \
        run data get entity @s item.tag.wishlist_globe.zoom
scoreboard players add zoom wishlist.vars 1
scoreboard players operation zoom wishlist.vars %= 6 wishlist.vars
execute on passengers \
        store result entity @s item.tag.wishlist_globe.zoom int 1 \
        run scoreboard players get zoom wishlist.vars
scoreboard players set done wishlist.vars 1
