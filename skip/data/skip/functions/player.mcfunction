
$execute unless entity @a[scores={wishlist.playerId=$(id)}] \
        run return run title @s actionbar "No such player online"
$execute unless entity @a[scores={wishlist.playerId=$(id)},tag=!wishlist.hidden] \
        run return run title @s actionbar "That player is hidden"
function skip:record
execute at @s run function skip:effect
$tp @s @a[scores={wishlist.playerId=$(id)},limit=1]
execute at @s run function skip:effect
advancement grant @s only wishlist:use_skip
