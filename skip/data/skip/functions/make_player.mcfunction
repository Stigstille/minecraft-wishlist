
execute unless function wishlist:consume_level \
        run return run title @s actionbar "You don't have enough levels"
data modify storage wishlist:args skip set value {}
execute store result storage wishlist:args skip.id int 1 \
        run scoreboard players get @s wishlist.playerId
function skip:make_player_1 with storage wishlist:args skip


