
data modify storage wishlist:args skip set value {}
execute store result storage wishlist:args skip.id int 1 \
        run scoreboard players get @s wishlist.playerId
data modify storage wishlist:args skip.x set from entity @s SpawnX
data modify storage wishlist:args skip.y set from entity @s SpawnY
data modify storage wishlist:args skip.z set from entity @s SpawnZ
data modify storage wishlist:args skip.dimension set from entity @s SpawnDimension
function skip:skip with storage wishlist:args skip
