
data modify storage wishlist:args skip set value {}
execute store result storage wishlist:args skip.x int 1 run data get entity @s Pos[0]
execute store result storage wishlist:args skip.y int 1 run data get entity @s Pos[1]
execute store result storage wishlist:args skip.z int 1 run data get entity @s Pos[2]
data modify storage wishlist:args skip.dimension set from entity @p Dimension
