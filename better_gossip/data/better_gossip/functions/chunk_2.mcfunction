# chunk_2.mcfunction

execute store result score player wishlist.vars \
        run data get storage wishlist:args gossip.player

execute as @a \
        if score @s wishlist.playerId = player wishlist.vars \
        at @s \
        anchored eyes \
        if entity @e[tag=wishlist.better_gossip.radio,distance=..4] \
        run tag @s add wishlist.better_gossip.target

execute if entity @a[tag=wishlist.better_gossip.target] \
        run function better_gossip:chunk_3
