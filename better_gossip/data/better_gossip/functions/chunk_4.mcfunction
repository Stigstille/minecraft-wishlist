# chunk_4.mcfunction

function wishlist:recall

function wishlist:rand
scoreboard players operation rand wishlist.vars %= 8 wishlist.vars

data modify storage wishlist:args gossip set value {}
execute if score rand wishlist.vars matches 0 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args gossip_pool.good[0]
execute if score rand wishlist.vars matches 1 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args gossip_pool.good[1]
execute if score rand wishlist.vars matches 2 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args gossip_pool.good[2]
execute if score rand wishlist.vars matches 4 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args gossip_pool.bad[0]
execute if score rand wishlist.vars matches 5 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args gossip_pool.bad[1]
execute if score rand wishlist.vars matches 6 \
        run data modify storage wishlist:args gossip \
        set from storage wishlist:args gossip_pool.bad[2]

execute if score rand wishlist.vars matches 0..3 \
        run data modify storage wishlist:args memories_gossip.good \
        append from storage wishlist:args gossip
execute if score rand wishlist.vars matches 4..7 \
        run data modify storage wishlist:args memories_gossip.bad \
        append from storage wishlist:args gossip

execute if data storage wishlist:args memories_gossip.good[3] \
        run data remove storage wishlist:args memories_gossip.good[0]
execute if data storage wishlist:args memories_gossip.bad[3] \
        run data remove storage wishlist:args memories_gossip.bad[0]

function wishlist:memorize

