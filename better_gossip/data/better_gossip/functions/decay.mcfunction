# decay

execute if score @s vg_positive matches 1.. run scoreboard players remove @s vg_positive 1
execute if score @s vg_negative matches 1.. run scoreboard players remove @s vg_negative 1
execute if score @s ig_positive matches 1.. run scoreboard players remove @s ig_positive 1
execute if score @s ig_negative matches 1.. run scoreboard players remove @s ig_negative 1

execute store result score @s wishlist.better_gossip.last_gossiped run time query gametime
