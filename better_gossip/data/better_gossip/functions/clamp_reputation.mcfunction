# clamp_reputation.mcfunction

execute as @s[scores={vg_positive=1..}] \
        run function better_gossip:villager/positive
execute as @s[scores={vg_negative=1..}] \
        run function better_gossip:villager/negative

execute store result score gossip_delta wishlist.vars run time query gametime
scoreboard players operation gossip_delta wishlist.vars -= @s \
        wishlist.better_gossip.last_gossiped
execute if score gossip_delta wishlist.vars matches 1200.. \
        run function better_gossip:decay

scoreboard players set @s[scores={vg_positive=200..}] vg_positive 200
scoreboard players set @s[scores={vg_negative=200..}] vg_negative 200
scoreboard players set @s[scores={ig_positive=200..}] ig_positive 200
scoreboard players set @s[scores={ig_negative=200..}] ig_negative 200

scoreboard players operation @s vg_negative > @s vg_negative_min
scoreboard players operation @s vg_positive > @s vg_positive_min
scoreboard players operation @s ig_negative > @s ig_negative_min
scoreboard players operation @s ig_positive > @s ig_positive_min

scoreboard players reset @s[scores={vg_positive=0}] vg_positive
scoreboard players reset @s[scores={vg_negative=0}] vg_negative
scoreboard players reset @s[scores={ig_positive=0}] ig_positive
scoreboard players reset @s[scores={ig_negative=0}] ig_negative

# Trigger luck effects
scoreboard players operation rep wishlist.vars = @s vg_positive
scoreboard players operation rep wishlist.vars -= @s vg_negative

execute if score rep wishlist.vars matches 180.. \
        run scoreboard players add @s vg_good_luck 1
execute unless score rep wishlist.vars matches 180.. \
        run scoreboard players set @s vg_good_luck 0

execute if score rep wishlist.vars matches ..-180 \
        run scoreboard players add @s vg_bad_luck 1
execute unless score rep wishlist.vars matches ..-180 \
        run scoreboard players set @s vg_bad_luck 0

execute if score @s vg_good_luck matches 10.. \
        run effect give @s minecraft:luck 200 0 true
execute if score @s vg_good_luck matches 20.. \
        run effect give @s minecraft:luck 200 1 true
execute if score @s vg_good_luck matches 40.. \
        run effect give @s minecraft:luck 200 2 true
execute if score @s vg_good_luck matches 80.. \
        run effect give @s minecraft:luck 200 3 true
execute if score @s vg_good_luck matches 80.. \
        run advancement grant @s only wishlist:karma-riffic

execute if score @s vg_bad_luck matches 10.. \
        run effect give @s minecraft:unluck 200 0 true
execute if score @s vg_bad_luck matches 20.. \
        run effect give @s minecraft:unluck 200 1 true
execute if score @s vg_bad_luck matches 40.. \
        run effect give @s minecraft:unluck 200 2 true
execute if score @s vg_bad_luck matches 80.. \
        run effect give @s minecraft:unluck 200 3 true
execute if score @s vg_bad_luck matches 80.. \
        run advancement grant @s only wishlist:karma-riffic

# Grant advancements for maxed reputation
advancement grant @s[scores={vg_positive=200..}] only wishlist:the_price_of_fame
advancement grant @s[scores={vg_negative=200..}] only wishlist:the_price_of_fame
