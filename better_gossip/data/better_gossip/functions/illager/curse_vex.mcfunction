# retribution_1.mcfunction - exact retribution on player (level 1)

execute if predicate better_gossip:propagate \
        at @s \
        run summon minecraft:vex ~ ~ ~
scoreboard players remove @s ig_negative 1
