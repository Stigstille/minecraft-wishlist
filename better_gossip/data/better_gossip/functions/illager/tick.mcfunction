# tick.mcfunction - Update gossip

# Increment negative rep by number of illagers killed
execute as @a \
        run scoreboard players operation @s ig_negative += @s ig_pillagers_killed
scoreboard players reset @a ig_pillagers_killed
execute as @a \
        run scoreboard players operation @s ig_negative += @s ig_vindicators_killed
scoreboard players reset @a ig_vindicators_killed
execute as @a \
        run scoreboard players operation @s ig_negative += @s ig_evokers_killed
scoreboard players reset @a ig_evokers_killed
execute as @a \
        run scoreboard players operation @s ig_negative += @s ig_illusioners_killed
scoreboard players reset @a ig_illusioners_killed
execute as @a \
        run scoreboard players operation @s ig_negative += @s ig_ravagers_killed
scoreboard players reset @a ig_ravagers_killed
execute as @a \
        run scoreboard players operation @s ig_negative += @s ig_witches_killed
scoreboard players reset @a ig_witches_killed
execute as @a \
        run scoreboard players operation @s ig_killed *= death_rep_hit wishlist.vars
execute as @a run scoreboard players operation @s ig_negative -= @s ig_killed
scoreboard players reset @a ig_killed

# Transform reputation scores into illager gossip.
execute as @a[scores={ig_positive=1..}] \
        run function better_gossip:illager/positive
execute as @a[scores={ig_negative=1..}] \
        run function better_gossip:illager/negative

# Clamp reputation scores to 200
scoreboard players set @a[scores={ig_positive=200..}] ig_positive 200
scoreboard players set @a[scores={ig_negative=200..}] ig_negative 200

# Remove players with no reputation from scoreboard
scoreboard players reset @a[scores={ig_positive=..0}] ig_positive
scoreboard players reset @a[scores={ig_negative=..0}] ig_negative

execute as @a[scores={ig_negative=200..}] \
        run function better_gossip:illager/curse_death
execute as @a[scores={ig_negative=175..}] \
        run function better_gossip:illager/curse_vex
execute as @a[scores={ig_negative=150..}] \
        run function better_gossip:illager/curse_wither
execute as @a[scores={ig_negative=125..}] \
        run function better_gossip:illager/curse_blindness
execute as @a[scores={ig_negative=100..}] \
        run function better_gossip:illager/curse_poison
execute as @a[scores={ig_negative=75..}] \
        run function better_gossip:illager/curse_weakness
execute as @a[scores={ig_negative=50..}] \
        run function better_gossip:illager/curse_slowness
