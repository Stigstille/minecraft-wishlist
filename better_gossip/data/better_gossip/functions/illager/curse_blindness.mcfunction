# curse_blindness.mcfunction - make player blind for a little while

execute if predicate better_gossip:propagate \
        run effect give @s minecraft:blindness 60
scoreboard players remove @s ig_negative 1

