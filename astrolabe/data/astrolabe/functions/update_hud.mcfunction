
execute if score @s wishlist.facing matches 0 \
run data modify storage wishlist:args direction set value "E"
execute if score @s wishlist.facing matches 1 \
run data modify storage wishlist:args direction set value "W"
execute if score @s wishlist.facing matches 2 \
run data modify storage wishlist:args direction set value "U"
execute if score @s wishlist.facing matches 3 \
run data modify storage wishlist:args direction set value "D"
execute if score @s wishlist.facing matches 4 \
run data modify storage wishlist:args direction set value "S"
execute if score @s wishlist.facing matches 5 \
run data modify storage wishlist:args direction set value "N"

title @s actionbar ["", \
{"color":"gold","entity":"@s","nbt":"Dimension"}, \
{"text":"  ","color":"white"}, \
{"color":"white","score":{"name":"@s","objective":"wishlist.x"}}, \
{"text":" ","color":"white"}, \
{"color":"white","score":{"name":"@s","objective":"wishlist.y"}}, \
{"text":" ","color":"white"}, \
{"color":"white","score":{"name":"@s","objective":"wishlist.z"}}, \
{"text":"  ","color":"white"}, \
{"storage":"wishlist:args","nbt":"direction","color":"gold"}, \
{"text":"  ","color":"white"}, \
{"color":"white","score":{"name":"@s","objective":"wishlist.cx"}}, \
{"text":".","color":"white"}, \
{"color":"white","score":{"name":"@s","objective":"wishlist.cz"}}, \
{"text":"  ","color":"white"}, \
{"color":"white","score":{"name":"@s","objective":"wishlist.rx"}}, \
{"text":".","color":"white"}, \
{"color":"white","score":{"name":"@s","objective":"wishlist.rz"}} \
]

advancement grant @s only wishlist:use_astrolabe
