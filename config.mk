
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# To go down a level $(call descend,directory[,target[,flags]])

descend = make -C $(1) $(2) $(3)

# If the -s option was passed, use these to give a hint of what we are doing

ifneq ($(findstring s,$(filter-out --%,$(MAKEFLAGS))),)
    QUIET_GEN = @echo GEN $@;
    QUIET_ZIP = @echo ZIP $@;
    QUIET_UNZIP = @echo UNZIP $@;
    QUIET_INSTALL = @echo INSTALL $@;
    QUIET_TEST = @echo TEST $@;
    descend = @echo ENTER `pwd`/$(1) && make -C $(1) $(2) $(3) && echo EXIT `pwd`/$(1)
endif


