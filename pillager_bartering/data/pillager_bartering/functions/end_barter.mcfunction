# end_barter.mcfunction - remove emerald from hand and give item to player

item replace entity @s weapon.mainhand with minecraft:air
tag @s add pb_trader

execute at @p facing entity @e[tag=pb_trader] feet \
        rotated ~ 0 \
        run loot spawn ^ ^1.25 ^1 loot pillager_bartering:pillager_bartering

tag @s remove pb_trader
scoreboard players reset @s pb_appraise
advancement grant @p only wishlist:barter_with_pillager

