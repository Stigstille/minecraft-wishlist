# load.mcfunction - Initialize pillager bartering

scoreboard objectives add pb_ dummy
scoreboard objectives add pb_appraise dummy

data modify storage wishlist:args all_chatter append value \
        "Did you know that some pillagers manage a black market?"
data modify storage wishlist:args all_chatter append value \
        "There's some stuff that you'll only get from a friendly pillager"

# Call the tick functions
function pillager_bartering:tick
