# tick_2.mcfunction

# if the pillager is appraising stuff, let them do that
execute if score @s pb_appraise matches 0.. \
        run function pillager_bartering:tick_appraisal

# empty-handed villagers try to start bartering
# Empty-handed is defined as "not having a crossbow on their right hand"
# this may be not very accurate, but I can't find a way to check for an actual
# empty hand in an entity using nbt.
execute unless score @s pb_appraise matches 0.. \
        if entity @s[nbt=!{HandItems:[{id:"minecraft:crossbow"},{}]}] \
        run function pillager_bartering:try_begin_barter



