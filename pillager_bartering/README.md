Pillager Bartering
==================

Provides a bartering interface for unarmed pillagers. They will take emeralds
from the floor similarly to how piglins take gold bars, then give back an item
from the provided loot table.

Unlike piglins, pillagers will not move towards the emeralds.
Not yet, at least.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Overhead
--------

Updates every second:

* checks all entities in an 8-block radius around each player in search of
  pillagers.
    * checks all entities in an 2-block radius around empty-handed,
      non-appraising pillagers in search of emeralds.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Black Market: receive items from a bartering pillager
