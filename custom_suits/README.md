Custom Suits
============

Provides some special leather suit kits that give permanent enchants but
require being worn at all times to get the bonus:

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Suited for Success: wear a full custom kit
