# give_all.mcfunction - give one of all suit pieces to a player

give @s leather_boots{display:{Name:'[{"text":"Firefighter Boots","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Firefighter Kit","italic":false,"color":"gray"}]'],color:16701501},suit:"firefighter"}
give @s leather_chestplate{display:{Name:'[{"text":"Firefighter Jacket","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Firefighter Kit","italic":false,"color":"gray"}]'],color:16701501},suit:"firefighter"}
give @s leather_leggings{display:{Name:'[{"text":"Firefighter Pants","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Firefighter Kit","italic":false,"color":"gray"}]'],color:16701501},suit:"firefighter"}
give @s minecraft:player_head{display:{Name:'[{"text":"Firefighter Helmet","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Firefighter Kit","italic":false,"color":"gray"}]']},suit:"firefighter",SkullOwner:{Id:[I;-1578370733,-1642705875,-1234073372,-1189250242],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGJhOTA2YTkxOWQ5NTE5NTg5MjhmNWNhYjIyNDBlZjY4MDNjZTgyMjFkMDMxMWM3OGM3NDcyMjhkZjI1ODc0YSJ9fX0="}]}}}

give @s leather_boots{display:{Name:'[{"text":"Diving Boots","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Diving Kit","italic":false,"color":"gray"}]'],color:16351261},suit:"diving"}
give @s leather_chestplate{display:{Name:'[{"text":"Diving Jacket","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Diving Kit","italic":false,"color":"gray"}]'],color:16351261},suit:"diving"}
give @s leather_leggings{display:{Name:'[{"text":"Diving Pants","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Diving Kit","italic":false,"color":"gray"}]'],color:16351261},suit:"diving"}
give @s minecraft:player_head{display:{Name:'[{"text":"Diving Helmet","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Diving Kit","italic":false,"color":"gray"}]']},suit:"diving",SkullOwner:{Id:[I;-692441358,-1705162172,-2006523088,1179447155],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTIxMmVlYWU4MDcyZTdlNDkwMzM0YjA5MzBkOWI2NTM0NmMwMjA4OGRmMTAzZTc5MzA4Y2FlMmM3ZTRmMzY2NSJ9fX0="}]}}}

give @s leather_boots{display:{Name:'[{"text":"Running Boots","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Running Kit","italic":false,"color":"gray"}]'],color:4960605},suit:"running"}
give @s leather_chestplate{display:{Name:'[{"text":"Running Jacket","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Running Kit","italic":false,"color":"gray"}]'],color:4960605},suit:"running"}
give @s leather_leggings{display:{Name:'[{"text":"Running Pants","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Running Kit","italic":false,"color":"gray"}]'],color:4960605},suit:"running"}
give @s turtle_helmet{display:{Name:'[{"text":"Running Helmet","italic":false,"color":"yellow"}]',Lore:['[{"text":"Part of the Running Kit","italic":false,"color":"gray"}]']},suit:"running"}
