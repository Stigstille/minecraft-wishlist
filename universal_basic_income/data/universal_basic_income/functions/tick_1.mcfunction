# tick_1.mcfunction

execute at @s \
        rotated ~ 0 \
        run summon minecraft:enderman ^ ^ ^3 {Tags:[wishlist.mail_giver,wishlist.look_alive],CustomName:'"Post Carrier"',NoAI:1b}
playsound minecraft:entity.enderman.teleport neutral @a[distance=..8] ~ ~ ~
tellraw @s {"translate":"wishlist.player_mail.receive_mail","with":["Welfare Office"],"color":"dark_gray"}
execute at @s \
        rotated ~ 0 \
        run loot spawn ^ ^1.25 ^1 loot universal_basic_income:check
advancement grant @s only wishlist:universal_basic_income
scoreboard players operation @s wishlist.pay_day = pay_day wishlist.vars
scoreboard players remove vault wishlist.vars 16

