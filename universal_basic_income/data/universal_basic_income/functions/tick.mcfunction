# tick.mcfunction - animate some AI-less entities

execute store result score pay_day wishlist.vars run time query gametime
scoreboard players operation pay_day wishlist.vars /= 1728000 wishlist.vars

execute as @a \
        unless score @s wishlist.pay_day = pay_day wishlist.vars \
        if score vault wishlist.vars matches 16.. \
        if entity @s[nbt={OnGround:1b}] \
        run function universal_basic_income:tick_1

schedule function universal_basic_income:tick 60s

