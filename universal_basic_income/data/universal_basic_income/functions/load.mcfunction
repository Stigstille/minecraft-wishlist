# load.mcfunction - initialize datapack

scoreboard objectives add wishlist.pay_day dummy

scoreboard players set 1728000 wishlist.vars 1728000

data modify storage wishlist:args all_chatter append value \
        "So I heard that the rulers of the upworld would rather die than UBI? That's sad"
data modify storage wishlist:args all_chatter append value \
        "Give me a minute; I'm waiting for my daily money"

function universal_basic_income:tick

