Custom Discs
============

Provides a way to have *additional* music discs in the game, as opposed to
merely changing existing music discs to have different tracks.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Custom Blocks](../custom_blocks) for custom blocks
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

There are two specific item types added by the datapack.

The gramophone is a custom block similar to the vanilla jukebox:

* it does not interact with the surrounding redstone,
* but it can play custom music discs,
* in addition to vanilla music discs.

All custom discs are retextured instances of a carrot on a stick; when you
click one of these on a gramophone, it will be moved from the player's hand to
the gramophone slot, and the custom track associated with it will play.

Overhead
--------

Uses advancements to check if the player interacted with a gramophone and
trigger the appropriate functions.

Once per second per player, checks around for gramophones currently playing to
add a note particle effect.

Known Issues
------------

If more than one gramophone is around, playing or stopping a custom disc may
result in the rest of the gramophones going silent.

Gramophones don't make parrots dance (Unforgivable, i know)

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

