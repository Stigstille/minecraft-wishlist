
$execute on target run title @s actionbar { \
        "translate":"Now Playing: %s", \
        "with": [{"translate":"item.minecraft.music_disc_$(id).desc"}] \
        }
$playsound minecraft:music_disc.$(id) record @a ~ ~ ~
