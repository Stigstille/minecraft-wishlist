
#stop_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
data modify entity @s Tags set value [wishlist.custom_block,wishlist.gramophone.empty]
execute on passengers run data modify entity @s item.tag.CustomModelData set value 271864
execute on passengers run data modify storage wishlist:args Item set from entity @s item.tag.item
execute on passengers run data remove entity @s item.tag.item
function wishlist:throw_item
stopsound @a record
scoreboard players set done wishlist.vars 1
