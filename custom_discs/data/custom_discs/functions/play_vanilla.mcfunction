
advancement revoke @s only custom_discs:play_vanilla
data modify storage wishlist:args item set from entity @s SelectedItem

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s interaction \
        run function custom_discs:play_vanilla_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"text":"Unknown Error"}
execute unless score done wishlist.vars matches 0 \
        run item modify entity @s weapon.mainhand wishlist:consume_one



