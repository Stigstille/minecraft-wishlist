
data modify storage wishlist:custom_blocks gramophone set value { \
        interaction: { \
                Tags: [wishlist.gramophone.empty], \
                width: 1.01, \
                height: 1.01 \
        }, \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f] \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271864, \
                      display:{Name:'{"text":"Gramophone","italic":false}'} \
              } \
        }, \
        block: "minecraft:barrier" \
}

function custom_discs:tick_1s
