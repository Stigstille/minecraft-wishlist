
#play_custom_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
data modify entity @s Tags set value [wishlist.gramophone.full]
execute on passengers run data modify entity @s item.tag.CustomModelData set value 271865
execute on passengers run data modify entity @s item.tag.item set from storage wishlist:args item
data modify storage wishlist:args args set value {namespace:wishlist}
data modify storage wishlist:args args.id set string storage wishlist:args item.tag.wishlist.music_disc.id 20
function custom_discs:play_custom_2 with storage wishlist:args args
scoreboard players set done wishlist.vars 1

