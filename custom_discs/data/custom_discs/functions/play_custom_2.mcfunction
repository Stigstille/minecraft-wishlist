
$execute on target run title @s actionbar { \
        "translate":"Now Playing: %s", \
        "with": [{"translate":"wishlist.custom_discs.$(id).lore"}] \
        }
$playsound wishlist:music_disc.$(id) record @a ~ ~ ~
