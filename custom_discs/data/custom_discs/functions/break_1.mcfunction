
#break_1
execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run data modify storage wishlist:args Item set from entity @s item.tag.item
execute on passengers run data remove entity @s item.tag.item
function wishlist:throw_item
execute at @s run function custom_blocks:spawn {id:gramophone}
execute at @s run setblock ~ ~ ~ minecraft:air
execute on passengers run kill @s
kill @s
stopsound @a record
scoreboard players set done wishlist.vars 1
