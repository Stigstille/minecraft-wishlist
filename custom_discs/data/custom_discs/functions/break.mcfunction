
advancement revoke @s only custom_discs:break
data modify storage wishlist:args item set from entity @s SelectedItem

scoreboard players set done wishlist.vars 0
execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        if data entity @s attack \
        run function custom_discs:break_1

execute if score done wishlist.vars matches 0 \
        run title @s actionbar {"text":"Unknown Error"}

