Stock Traders
=============

Provides villagers with two different trades: from an item to a currency, and
from a currency to the same item, with the exchange rate being dependant on the
amount of that item they have available (higher when there's no stock, and
lower and lower as they acquire more and more stock).

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Wishlist Resources](../resources) for media and text translations.

Setting up a Stock Trader
-------------------------

The Stock Traders package does not provide a player-friendly way to set up a
stock trader.

This explanation is for stock admins and players in creative mode.

First, place a barrel on the ground for the trader to stand on, and configure
its inventory as follows:

```
B . . . . . . . .
. . . . . . . . .
S . . . . . . . .
```

Where `B` is the items the trader will buy, `S` is the maximum amount of
currency the trader will give for it, and `.` are empty spaces.

Then stand on the barrel and run the following command:

```
execute summon minecraft:villager run function stock_traders:reset_trader
```

Price Fluctuation
-----------------

Stock trader's prices will change in response to how much stock they
accumulate; the more stock they have, the less they will pay for more, and the
cheaper they'll let their stock go; the actual price depends on the `base`
price defined in the barrel, the `stock` and a factor of `N` hardcoded in the
datapack (by default `100`) in the following way:

```
price = base / (1 + stock / N)
```

Roughly this equation behaves as follows:

* at `stock = 0`, the price is equal to `base`;
* at `stock = N`, the price is `base / 2`;

If the price of an item reaches zero, the trader will stop buying that item.

Overhead
--------

Adds some advancements:

* checks if a player trades with a stock trader; when this happens, the stock
  trader is tagged for update and the update function is scheduled to run next
  tick.
    * checks every entity in a 6-block radius around each player each second to
      find stock traders that should be updated.

And no, the update can't be performed from the function called by the
advancement; that fails to correctly update the trader's recipes and somehow
allows for item duping.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

* Regulated Marketplace: trade materials with a stock trader
