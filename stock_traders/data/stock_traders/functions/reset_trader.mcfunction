# reset_trader.mcfunction

# The data stored is as follows:
#
# * memories.buy has the items that are exchanged for money
# * memories.sell has the money exchanged for the items
# * memories.stock has the number of items that the trader has
#
# * recipes[0] is the recipe that buys items, sells money
# * recipes[1] is the recipe that buys money, sells items

data modify entity @s NoAI set value 1b
tag @s add wishlist.stock_trader

function wishlist:recall

data modify storage wishlist:args memories.buy set from block ~ ~-1 ~ Items[{Slot:0b}]
data remove storage wishlist:args memories.buy.Slot
data modify storage wishlist:args memories.sell set from block ~ ~-1 ~ Items[{Slot:18b}]
data remove storage wishlist:args memories.sell.Slot
data modify storage wishlist:args memories.stock set value 0

data modify storage wishlist:args recipes set value []

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy merge from storage wishlist:args memories.buy
data modify storage wishlist:args recipe.sell merge from storage wishlist:args memories.sell
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify storage wishlist:args recipe set from storage wishlist:args empty_recipe
data modify storage wishlist:args recipe.buy merge from storage wishlist:args memories.sell
data modify storage wishlist:args recipe.sell merge from storage wishlist:args memories.buy
data modify storage wishlist:args recipes append from storage wishlist:args recipe

data modify entity @s Offers.Recipes set from storage wishlist:args recipes

function wishlist:memorize

particle minecraft:happy_villager ~ ~1 ~ 0.2 1 0.2 0.5 10

function stock_traders:update_trader
