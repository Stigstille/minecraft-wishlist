# update_trader.mcfunction

# The data stored is as follows:
#
# * memories.buy has the items that are exchanged for money
# * memories.sell has the money exchanged for the items
# * memories.stock has the number of items that the trader has
#
# * recipes[0] is the recipe that buys items, sells money
# * recipes[1] is the recipe that buys money, sells items
#
# The amount of uses in the second recipe depends directly on the vault
# The amount of uses in the first recipe depends directly on the stock

function wishlist:recall
data modify storage wishlist:args recipes set from entity @s Offers.Recipes

execute store result score stock wishlist.vars \
        run data get storage wishlist:args memories.stock
execute store result score buyCount wishlist.vars \
        run data get storage wishlist:args memories.buy.Count
execute store result score sellCount wishlist.vars \
        run data get storage wishlist:args memories.sell.Count
execute store result score price wishlist.vars \
        run data get storage wishlist:args recipes[0].sell.Count

# Update stock from the amout of items that have been bought and sold
#
# stock += recipes[0].uses * buyCount
# vault -= recipes[0].uses * price * memories.sell.tag.wishlist.currency
# stock -= recipes[1].uses * buyCount
# vault += recipes[1].uses * price * memories.sell.tag.wishlist.currency

scoreboard players set priceCurrency wishlist.vars 0
execute store result score priceCurrency wishlist.vars \
        run data get storage wishlist:args memories.sell.tag.wishlist.currency
scoreboard players operation priceCurrency wishlist.vars *= price wishlist.vars

execute store result score uses wishlist.vars \
        run data get storage wishlist:args recipes[0].uses
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= buyCount wishlist.vars
scoreboard players operation stock wishlist.vars += tmp wishlist.vars
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= priceCurrency wishlist.vars
scoreboard players operation vault wishlist.vars -= tmp wishlist.vars

execute store result score uses wishlist.vars \
        run data get storage wishlist:args recipes[1].uses
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= buyCount wishlist.vars
scoreboard players operation stock wishlist.vars -= tmp wishlist.vars
scoreboard players operation tmp wishlist.vars = uses wishlist.vars
scoreboard players operation tmp wishlist.vars *= priceCurrency wishlist.vars
scoreboard players operation vault wishlist.vars += tmp wishlist.vars

# Recalculate the price according to stock
#
# The actual price used to trade between these depends on the original recipe
# price, modified by the amount of stock the trader holds (the higher the
# stock, the lower the price goes)
#
# price = sellCount / (1 + stock / 10)
#
# in order to provide a smoother curve, we multiply this equation by an
# arbitrary number
#
# price = (sellCount * N) / (N + (stock * N) / 10)

scoreboard players operation price wishlist.vars = sellCount wishlist.vars
scoreboard players operation price wishlist.vars *= price_mult wishlist.vars
execute store result score tmp wishlist.vars \
        run data get storage wishlist:args memories.stock
scoreboard players operation tmp wishlist.vars *= price_mult wishlist.vars
scoreboard players operation tmp wishlist.vars /= price_curve wishlist.vars
scoreboard players operation tmp wishlist.vars += price_mult wishlist.vars
scoreboard players operation price wishlist.vars /= tmp wishlist.vars

scoreboard players set clampedPrice wishlist.vars 1
scoreboard players operation clampedPrice wishlist.vars > price wishlist.vars

execute store result storage wishlist:args recipes[0].sell.Count int 1 \
        run scoreboard players get clampedPrice wishlist.vars
execute store result storage wishlist:args recipes[1].buy.Count int 1 \
        run scoreboard players get clampedPrice wishlist.vars

# Reset uses for both recipes

data modify storage wishlist:args recipes[0].uses set value 0
data modify storage wishlist:args recipes[1].uses set value 0

# Update maxUses for both recipes
#
# This number is 1 if there's money in the vault (for the first trade) or stock
# in the trader (for the second trade), and 0 otherwise.
#
# It has to be 1 to ensure that the price changes accordingly.

data modify storage wishlist:args recipes[0].maxUses set value 1
execute if score vault wishlist.vars < price wishlist.vars \
        run data modify storage wishlist:args recipes[0].maxUses set value 0
execute if score price wishlist.vars matches 0 \
        run data modify storage wishlist:args recipes[0].maxUses set value 0

data modify storage wishlist:args recipes[1].maxUses set value 1
execute if score stock wishlist.vars < buyCount wishlist.vars \
        run data modify storage wishlist:args recipes[1].maxUses set value 0

# Remember for next time

execute store result storage wishlist:args memories.stock int 1 \
        run scoreboard players get stock wishlist.vars

function wishlist:memorize
data modify entity @s Offers.Recipes set from storage wishlist:args recipes
tag @s remove wishlist.stock_trader.update

