# load.mcfunction

scoreboard players set price_curve wishlist.vars 100
scoreboard players set price_mult wishlist.vars 1000

data modify storage wishlist:args all_chatter append value \
        "A server-wide storage of stuff, with prices changing according to the stock…"
data modify storage wishlist:args all_chatter append value \
        "So, are you contributing to the Stock?"

data modify storage wishlist:args empty_recipe set value {maxUses:2147483647,xp:0,rewardExp:0b,buy:{id:"minecraft:air",Count:0b},buyB:{id:"minecraft:air",Count:0b},sell:{id:"minecraft:air",Count:0b},"Paper.IgnoreDiscounts":1b}

