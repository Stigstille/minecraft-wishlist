# trade_with_stock.mcfunction

tag @e[type=#wishlist:trader,tag=wishlist.stock_trader,distance=..6] \
        add wishlist.stock_trader.update
advancement revoke @s only stock_traders:trade_with_stock
schedule function stock_traders:tick 1t
