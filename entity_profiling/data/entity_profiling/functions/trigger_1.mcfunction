# trigger_1.mcfunction

scoreboard players set count wishlist.vars 0
scoreboard players set tmp wishlist.vars 10
scoreboard players operation distance wishlist.vars = @s wishlist.entity_profiling.trigger
scoreboard players operation distance wishlist.vars %= tmp wishlist.vars
scoreboard players operation filter wishlist.vars = @s wishlist.entity_profiling.filter

execute if score @s wishlist.entity_profiling.trigger matches 1..29 \
        run function entity_profiling:chunk
execute if score @s wishlist.entity_profiling.trigger matches 31..59 \
        run function entity_profiling:region

execute if score @s wishlist.entity_profiling.trigger matches 1..9 \
        positioned ~ -128 ~ \
        summon minecraft:marker run function entity_profiling:chunk_1
execute if score @s wishlist.entity_profiling.trigger matches 11..19 \
        positioned ~ ~ ~ \
        summon minecraft:marker run function entity_profiling:chunk_1
execute if score @s wishlist.entity_profiling.trigger matches 21..29 \
        positioned ~ ~-513 ~ \
        summon minecraft:marker run function entity_profiling:chunk_1

execute if score @s wishlist.entity_profiling.trigger matches 31..39 \
        positioned ~ -128 ~ \
        summon minecraft:marker run function entity_profiling:region_1
execute if score @s wishlist.entity_profiling.trigger matches 41..49 \
        positioned ~ ~ ~ \
        summon minecraft:marker run function entity_profiling:region_1
execute if score @s wishlist.entity_profiling.trigger matches 51..59 \
        positioned ~ ~-513 ~ \
        summon minecraft:marker run function entity_profiling:region_1

execute if score filter wishlist.vars matches 0 \
        run data modify storage wishlist:args type set value "entities"
execute if score filter wishlist.vars matches 1 \
        run data modify storage wishlist:args type set value "markers"
execute if score filter wishlist.vars matches 2 \
        run data modify storage wishlist:args type set value "text displays"
execute if score filter wishlist.vars matches 3 \
        run data modify storage wishlist:args type set value "item displays"
execute if score filter wishlist.vars matches 4 \
        run data modify storage wishlist:args type set value "block displays"
execute if score filter wishlist.vars matches 5 \
        run data modify storage wishlist:args type set value "items"
execute if score filter wishlist.vars matches 6 \
        run data modify storage wishlist:args type set value "item frames"
execute if score filter wishlist.vars matches 7 \
        run data modify storage wishlist:args type set value "glow item frames"
execute if score filter wishlist.vars matches 8 \
        run data modify storage wishlist:args type set value "armor stands"

execute if score distance wishlist.vars matches 1 \
        run scoreboard players set size wishlist.vars 1
execute if score distance wishlist.vars matches 2 \
        run scoreboard players set size wishlist.vars 4
execute if score distance wishlist.vars matches 3 \
        run scoreboard players set size wishlist.vars 16
execute if score distance wishlist.vars matches 4 \
        run scoreboard players set size wishlist.vars 64
execute if score distance wishlist.vars matches 5 \
        run scoreboard players set size wishlist.vars 256
execute if score distance wishlist.vars matches 6 \
        run scoreboard players set size wishlist.vars 1
execute if score distance wishlist.vars matches 7 \
        run scoreboard players set size wishlist.vars 4
execute if score distance wishlist.vars matches 8 \
        run scoreboard players set size wishlist.vars 16
execute if score distance wishlist.vars matches 9 \
        run scoreboard players set size wishlist.vars 64

execute if score @s wishlist.entity_profiling.trigger matches 1..29 \
        run tellraw @s { \
          "translate":"wishlist.entity_profiling.count", \
          "with":[ \
            {"score":{"name":"count","objective":"wishlist.vars"}}, \
            {"storage":"wishlist:args","nbt":"type"}, \
            {"score":{"name":"size","objective":"wishlist.vars"}}, \
            {"text":"chunks"}, \
            {"score":{"name":"x0","objective":"wishlist.vars"}}, \
            {"score":{"name":"z0","objective":"wishlist.vars"}} \
          ], \
          "color":"gray" \
        }

execute if score @s wishlist.entity_profiling.trigger matches 31..59 \
        run tellraw @s { \
          "translate":"wishlist.entity_profiling.count", \
          "with":[ \
            {"score":{"name":"count","objective":"wishlist.vars"}}, \
            {"storage":"wishlist:args","nbt":"type"}, \
            {"score":{"name":"size","objective":"wishlist.vars"}}, \
            {"text":"regions"}, \
            {"score":{"name":"x0","objective":"wishlist.vars"}}, \
            {"score":{"name":"z0","objective":"wishlist.vars"}} \
          ], \
          "color":"gray" \
        }

