# triggers.mcfunction - check for trigger activation

# Reschedule this function to be run later
schedule function entity_profiling:tick 1s

# Allow players to run /trigger prof
scoreboard players enable @a prof
execute as @a[scores={prof=1..}] at @s run function entity_profiling:local
scoreboard players set @a prof 0

# Allow players to run /trigger gprof
scoreboard players enable @a gprof
execute as @a[scores={gprof=1..}] at @s run function entity_profiling:global
scoreboard players set @a gprof 0

# Allow players to run /trigger cprof
scoreboard players enable @a cprof
execute as @a[scores={cprof=1..}] at @s run function entity_profiling:chunk
scoreboard players set @a cprof 0

# Allow players to run /trigger rprof
scoreboard players enable @a rprof
execute as @a[scores={rprof=1..}] at @s run function entity_profiling:region
scoreboard players set @a rprof 0

# Allow players to run /trigger pprof
scoreboard players enable @a pprof
execute as @a[scores={pprof=1..}] at @s run function entity_profiling:player
scoreboard players set @a pprof 0

# Allow players to run /trigger wishlist.entity_profiling.trigger
scoreboard players enable @a wishlist.entity_profiling.filter
scoreboard players enable @a wishlist.entity_profiling.trigger
execute as @a[scores={wishlist.entity_profiling.trigger=1..}] \
        at @s \
        run function entity_profiling:trigger
scoreboard players set @a wishlist.entity_profiling.trigger 0

