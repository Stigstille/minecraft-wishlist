
# cushion:block_3
execute if block ~ ~-0.25 ~ #cushion:ignore run return fail
execute unless block ~ ~0.75 ~ #cushion:ignore run return fail
execute unless block ~ ~1.75 ~ #cushion:ignore run return fail
tag @s add wishlist.match
execute positioned ~0.5 ~ ~0.5 summon minecraft:interaction run function cushion:sit
tag @s remove wishlist.match
