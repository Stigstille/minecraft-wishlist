
# cushion:sit
tellraw @a[tag=wishlist.debug] {"text":"[cushion] sit","color":"yellow"}
data merge entity @s {Tags:[wishlist.cushion],width:0.5,height:0.5}
ride @a[tag=wishlist.match,limit=1] mount @s
item modify entity @a[tag=wishlist.match,limit=1] weapon.mainhand wishlist:consume_one
return 1
