
# cushion:tick_1t

schedule function cushion:tick_1t 1t
scoreboard players operation sit_players_0 wishlist.vars = sit_players_1 wishlist.vars
scoreboard players set sit_players_1 wishlist.vars 0
execute as @a on vehicle run scoreboard players add sit_players_1 wishlist.vars 1
execute unless score sit_players_0 wishlist.vars = sit_players_1 wishlist.vars \
        at @a as @e[tag=wishlist.cushion,distance=..4] run function cushion:prune
