
# cushion:entity_1
execute store result score when wishlist.vars run data get entity @s interaction.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute unless block ~ ~1 ~ #cushion:ignore run return fail
execute unless block ~ ~2 ~ #cushion:ignore run return fail
data modify storage wishlist:args args set value {}
data modify storage wishlist:args args.height set from entity @s height
execute at @s run function cushion:entity_2 with storage wishlist:args args


