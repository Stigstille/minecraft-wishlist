
# cushion:block_2
tellraw @a[tag=wishlist.debug] {"text":"[cushion] block_2","color":"yellow"}
execute if block ~ ~ ~ #minecraft:stairs[half=bottom] \
        align xyz \
        positioned ~ ~0.5 ~ \
        run return run function cushion:block_3
execute if block ~ ~ ~ #minecraft:slabs[type=bottom] \
        align xyz \
        positioned ~ ~0.5 ~ \
        run return run function cushion:block_3
execute if block ~ ~ ~ #cushion:custom \
        align xyz \
        positioned ~ ~0.5 ~ \
        run return run function cushion:block_3
execute if block ~-0.1 ~ ~ #cushion:ignore \
        align xyz \
        positioned ~-1 ~ ~ \
        run return run function cushion:block_3
execute if block ~0.1 ~ ~ #cushion:ignore \
        align xyz \
        positioned ~1 ~ ~ \
        run return run function cushion:block_3
execute if block ~ ~ ~-0.1 #cushion:ignore \
        align xyz \
        positioned ~ ~ ~-1 \
        run return run function cushion:block_3
execute if block ~ ~ ~0.1 #cushion:ignore \
        align xyz \
        positioned ~ ~ ~1 \
        run return run function cushion:block_3
execute if block ~ ~0.1 ~ #cushion:ignore \
        align xyz \
        positioned ~ ~1 ~ \
        run return run function cushion:block_3
return fail


