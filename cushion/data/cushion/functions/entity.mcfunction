
# cushion:entity
advancement revoke @s only cushion:use_cushion_on_entity
tag @s add wishlist.match

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=interaction,distance=..4] \
        at @s \
        if data entity @s interaction \
        store success score done wishlist.vars \
        run function cushion:entity_1

execute if score done wishlist.vars matches 0 run title @s actionbar "No space to sit"
tag @s remove wishlist.match
