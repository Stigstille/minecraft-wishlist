
# cushion:block
tellraw @a[tag=wishlist.debug] {"text":"[cushion] block","color":"yellow"}
advancement revoke @s only cushion:use_cushion_on_block
scoreboard players set steps wishlist.vars 60
execute anchored eyes \
        positioned ^ ^ ^ \
        unless function cushion:block_1 \
        run return run title @s actionbar "No space to sit"
