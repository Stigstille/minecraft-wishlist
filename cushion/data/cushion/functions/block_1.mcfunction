
# cushion:block_1
execute if score steps wishlist.vars matches 0 run return fail
execute unless block ~ ~ ~ #cushion:ignore run return run function cushion:block_2
scoreboard players remove steps wishlist.vars 1
execute positioned ^ ^ ^0.1 run function cushion:block_1
