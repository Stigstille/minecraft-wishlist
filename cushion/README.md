Cushion
=======

Adds a ![cushion] cushion item that allows players to sit on blocks and
interaction entities

Compare with [Sittable Blocks](../sittable_blocks):

* Cushion allows to sit on interactions.
* Cushion triggers interactive blocks when sitting on them.
* Cushion will sit the player on the block they click.
* Cushion does require the use of an item taking one inventory slot.

Requirements
------------

* Requires [Wishlist Core](../wishlist) for common datapack functions.
* Requires [Magic](../magic) for UI
* Requires [Wishlist Resources](../resources) for media and text translations.

Usage
-----

For most blocks:

* clicking on top will sit you on top,
* clicking on the bottom will fail,
* clicking on the side will try to sit you on a block on that side.

For bottom stairs, bottom slabs, any block in the `cushion:custom` tag, and
interaction entities:

* clicking anywhere will sit you on it as if it was a chair

When sitting the cushion will be consumed.

Standing up will spawn a new cushion item for the player to pick up.

Blocks in the `cushion:ignore` tag are considered empty for the algorithm, and
include the following:

* `#minecraft:all_signs`
* `#minecraft:banners`
* `#minecraft:buttons`
* `#minecraft:candles`
* `#minecraft:cave_vines`
* `#minecraft:crops`
* `#minecraft:fire`
* `#minecraft:flowers`
* `#minecraft:portals`
* `#minecraft:pressure_plates`
* `#minecraft:rails`
* `#minecraft:saplings`
* `#minecraft:snow`
* `#minecraft:tall_flowers`
* `#minecraft:wool_carpets`
* `minecraft:air`
* `minecraft:cave_air`
* `minecraft:void_air`
* `minecraft:light`
* `minecraft:water`
* `minecraft:lava`

Blocks in the `cushion:custom` tag are considered "sittable" alongside bottom
stairs and bottom slabs; players will sit on them even when clicked on the
side:

* `minecraft:creeper_head`
* `minecraft:flower_pot`
* `minecraft:piglin_head`
* `minecraft:player_head`
* `minecraft:skeleton_skull`
* `minecraft:wither_skeleton_skull`
* `minecraft:zombie_head`
* `#minecraft:beds`

Overhead
--------

Uses [Magic](../magic) to trigger the cushion item.

Checks entities in a small radius around every player every second in search of
unoccupied cushions that can be respawned.

Known Issues
------------

Using the cushion on an empty pot will sit the player and add the cushion to
the pot's inventory effectively duplicating it.

Advancements
------------

Adding this datapack provides the following advancements under the root
wishlist advancement:

[cushion]: resources/assets/wishlist/textures/item/cushion.png ""
