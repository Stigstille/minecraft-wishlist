# custom_blocks/place_1.mcfunction

# Note: the rotation stuff is done to make the custom blocks align to the usual
# minecraft rotation scheme for blocks, that divides a full circumference in 16
# arcs but also to rotate the block 180 from the rotation of the player who
# placed it; the full equation is:
#
# r_block = floor((r_player + 180 + 11.25) / 22.5) * 22.5
#
# but we round 11.25 to 11 because we can't do floating-point math on scores

data modify storage wishlist:args block set value { \
        data: { \
                Passengers: [{ \
                        id:"minecraft:item_display", \
                        Rotation:[0f,0f], \
                        item: { \
                                id:"minecraft:item_frame", \
                                Count:1b, \
                        } \
                }] \
        } \
}
$data modify storage wishlist:args block.data \
        merge from storage wishlist:custom_blocks $(id).interaction
$data modify storage wishlist:args block.data.Passengers[0] \
        merge from storage wishlist:custom_blocks $(id).display
$data modify storage wishlist:args block.data.Passengers[0].item \
        merge from storage wishlist:custom_blocks $(id).item
$data modify storage wishlist:args \
        block.data.Passengers[0].item.tag.EntityTag.Item.tag.wishlist.custom_block.id \
        set value $(id)
execute store result storage wishlist:args block.data.Passengers[0].Rotation[0] float 0.044444 \
        run scoreboard players add yaw wishlist.vars 191
execute store result storage wishlist:args block.data.Passengers[0].Rotation[0] float 22.5 \
        run data get storage wishlist:args block.data.Passengers[0].Rotation[0]
data modify storage wishlist:args block.data.Tags append value wishlist.custom_block
$data modify storage wishlist:args block.block set from storage wishlist:custom_blocks $(id).block
function custom_blocks:place_2 with storage wishlist:args block
kill @s[type=item_frame]

