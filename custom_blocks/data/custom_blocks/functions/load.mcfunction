
data modify storage wishlist:custom_blocks black_pawn set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271833, \
                              display:{Name:'{"text":"Black Pawn","italic":false}'} \
              } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks black_knight set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
              tag: { \
                      CustomModelData:271834, \
                              display:{Name:'{"text":"Black Knight","italic":false}'} \
              } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks black_bishop set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271835, \
                        display:{Name:'{"text":"Black Bishop","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks black_rook set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271836, \
                        display:{Name:'{"text":"Black Rook","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks black_queen set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271837, \
                        display:{Name:'{"text":"Black Queen","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks black_king set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271838, \
                        display:{Name:'{"text":"Black King","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks white_pawn set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271839, \
                        display:{Name:'{"text":"White Pawn","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks white_knight set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271840, \
                        display:{Name:'{"text":"White Knight","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks white_bishop set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271841, \
                        display:{Name:'{"text":"White Bishop","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks white_rook set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271842, \
                        display:{Name:'{"text":"White Rook","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks white_queen set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271843, \
                        display:{Name:'{"text":"White Queen","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

data modify storage wishlist:custom_blocks white_king set value { \
        display: { \
                transformation: [1f,0f,0f,0f,0f,1f,0f,0.5f,0f,0f,1f,0f,0f,0f,0f,1f], \
                billboard: "vertical", \
        }, \
        item: { \
                tag: { \
                        CustomModelData:271844, \
                        display:{Name:'{"text":"White King","italic":false}'} \
                } \
        }, \
        block: "minecraft:air" \
}

