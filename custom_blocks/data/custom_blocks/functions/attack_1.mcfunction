# custom_blocks/attack_1.mcfunction

execute store result score when wishlist.vars run data get entity @s attack.timestamp
execute store result score now wishlist.vars run time query gametime
execute unless score when wishlist.vars = now wishlist.vars run return fail
execute on passengers run data modify storage wishlist:args item set from entity @s item
function custom_blocks:attack_2 with storage wishlist:args item.tag
function custom_blocks:attack_3 with storage wishlist:args item.tag.EntityTag.Item.tag.wishlist.custom_block
loot spawn ~ ~ ~ loot custom_blocks:block
execute on passengers run kill @s
kill @s
