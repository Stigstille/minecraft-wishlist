# attack.mcfunction

execute anchored eyes \
        positioned ^ ^ ^2 \
        as @e[type=minecraft:interaction,tag=wishlist.custom_block,distance=..4] \
        if data entity @s attack \
        at @s \
        run function custom_blocks:attack_1

advancement revoke @s only custom_blocks:attack

