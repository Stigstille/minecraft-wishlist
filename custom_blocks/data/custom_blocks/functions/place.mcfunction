# custom_blocks/place.mcfunction

scoreboard players operation yaw wishlist.vars = @s wishlist.yaw

execute as @e[type=item_frame,distance=..8] \
        if data entity @s Item.tag.wishlist.custom_block \
        at @s \
        align xyz \
        positioned ~0.5 ~ ~0.5 \
        run function custom_blocks:place_1 with entity @s Item.tag.wishlist.custom_block

advancement revoke @s only custom_blocks:place

